<?php 

require_once('lib/functions.php');
if(!validRequest()) {
	http_response_code(401); 
	die;
}

/*
session_start();
$headerToken = $_SERVER['HTTP_ACCESS_TOKEN'];
$sessionToken = $_SESSION['XSRF'];

if(!isset($sessionToken) && $headerToken != $sessionToken){
	$resposta["error"] = false;
	$resposta["status"] = 3;
	$resposta["message"] = "Favor realizar novamente o login.";

	echo json_encode($resposta);
	die;
}
*/

date_default_timezone_set('America/Sao_Paulo');
$resposta = array();

try{
	include_once('conexao.php');
	
	$checkout_id = filter_var($_POST["checkout"], FILTER_SANITIZE_NUMBER_INT);
	$type_card = $_POST["type_card"];
	$number_card = $_POST["number_card"];
	$month_expiration = $_POST["month_expiration"];
	$year_expiration = $_POST["year_expiration"];
	$security_card = $_POST["security_card"];
	$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
	
	if(isset($_POST["bdaytime"])){
		$schedule = $_POST["bdaytime"] . ":00";
	}
	
	include('class/Checkout.php');
	
	$query = $con->prepare('SELECT * FROM checkout WHERE checkout_id = ?');
	$query->execute(array($checkout_id));
	$num_rows = $query->rowCount();
	$query->setFetchMode(PDO::FETCH_CLASS, 'Checkout');
	
	if($num_rows > 0){
		
		$row = $query->fetch();
		$id_customer = $row->getCheckoutCustomerId();
		$order_tracking_number = $row->getCheckoutId() . date('ymdHi');
		$order_date = date('Y-m-d H:i:s');
		$id_sector = $row->getCheckoutSector();
		$id_floor = $row->getCheckoutFloor();
		$id_chair = $row->getCheckoutChair();
		$id_table = $row->getCheckoutTable();
		if($id_table == 0){
			$id_table = 'null';
		}
		$id_event = $row->getCheckoutEventId();
		
		include('class/Event.php');
		
		$query = $con->prepare('SELECT event_date, event_final_time, event_tax_service FROM event WHERE event_id = ?');
		$query->execute(array($id_event));
		$num_rows_event = $query->rowCount();
		$query->setFetchMode(PDO::FETCH_CLASS, 'Event');
		
		if($num_rows_event){
			$row2 = $query->fetch();

			$date_now = new DateTime(date('Y-m-d H:i:s'));
			$date = explode(" ", $row2->getEventDate());
			$date_event = new DateTime($date[0] . ' ' .$row2->getEventFinalTime());
			$per_tax_service = $row2->getEventTaxService();
			
			if($date_now > $date_event){
				$resposta["error"] = false;
				$resposta["status"] = 1;
				$resposta["message"] = "Data/Hora do evento é menor que data atual.";
				
				echo json_encode($resposta);
				die;
			}
		}
		
		$id_price = $row->getCheckoutPrice();
		$tax_service = $id_price * $per_tax_service;
		$id_price_discount = $row->getCheckoutPriceDiscount();
		$id_tip = $row->getCheckoutTip();
		$id_price_total = $row->getCheckoutPriceTotal() + $tax_service + $id_tip;
		$id_coupon = $row->getCheckoutCouponId();
		if($id_coupon == 0){
			$id_coupon = 'null';
		}
		
		include('class/CheckoutItem.php');
		
		$query2 = $con->prepare('SELECT * FROM checkout_item WHERE `checkout_item_checkout_id` = ?');
		$query2->execute(array($checkout_id));
		$num_rows2 = $query2->rowCount();
		$query2->setFetchMode(PDO::FETCH_CLASS, 'CheckoutItem');
		
		if($num_rows2 > 0){
			try {
				$con->beginTransaction();
				
				$sql3 = "INSERT INTO `order`  (`order_customer_id`,
											`order_tracking_number`,
											`order_date`,
											`order_chair`,
											`order_table`,
											`order_floor`,
											`order_sector`,
											`order_price`,
											`order_price_discount`,
											`order_tip`,
											`order_tax_service`,
											`order_price_total`,
											`order_coupon_id`,
											`order_event_id`,
											`order_customer_email`,
											`order_schedule_date`,
											`order_status_id`)
				VALUES(" . $id_customer . ", '" . $order_tracking_number . "', '" . $order_date . "', 
				'" . $id_chair . "', " . $id_table . ", " . $id_floor . ", " . $id_sector . ",
				" . $id_price . ", " . $id_price_discount . ", " . $id_tip . ", " . $tax_service . ", " . $id_price_total . ",
				" . $id_coupon . ", " . $id_event . ", '" . $email . "', '" . $schedule . "' , 1)";
			
				$query3 = $con->prepare($sql3);
				$query3->execute();
				$order_id = $con->lastInsertId();
				
				while($row2 = $query2->fetch()){
					
					$id_product = $row2->getCheckoutItemProductId();
					$price_unit = $row2->getCheckoutItemPriceUnit();
					$quantity = $row2->getCheckoutItemQuantity();
					$price_total = $row2->getCheckoutItemPriceTotal();
					
					// INSERE OS ITENS NO PEDIDO
					$sql4 = "INSERT INTO `item` (`item_order_id`,
												`item_product_id`,
												`item_price_unit`,
												`item_quantity`,
												`item_price_total`)
					VALUES(" . $order_id . ", " . $id_product . ", '" . $price_unit . "',
					'" . $quantity . "', '" . $price_total . "')";
					
					$query4 = $con->prepare($sql4);
					$query4->execute();
					
					// SUBTRAI A QUANTIDADE NO ESTOQUE
					$sql5 = "UPDATE product 
					        SET product_inventory_current = product_inventory_current - " . $quantity . "
							WHERE product_id = ?";
					$query5 = $con->prepare($sql5);
					$query5->execute(array($id_product));
					
					$sql5 = "UPDATE checkout
					SET checkout_sin_order = 1
					WHERE checkout_id = ?";
					$query5 = $con->prepare($sql5);
					$query5->execute(array($checkout_id));
						
				}
				$con->commit();
				
				$input = "o=" . $order_id . "&n=" . $order_tracking_number;
				$arr = array("/" => "#", "\\" => "&");
				$ve = rtrim(strtr(base64_encode(gzdeflate($input, 9)), $arr), '=');
				
				try{
					$num_rows6 = 0;
					if($id_customer != 1){
						include('class/Customer.php');
						
						$query6 = $con->prepare('SELECT `customer_name`,`customer_email` FROM customer WHERE customer_id = ?');
						$query6->execute(array($id_customer));
						$num_rows6 = $query6->rowCount();
						$query6->setFetchMode(PDO::FETCH_CLASS, 'Customer');
						
						if($num_rows6 > 0){
							$row6 = $query6->fetch();
						}
						
						$email = $row6->getCustomerEmail();
						$name = $row6->getCustomerName();
					}
					
					if($num_rows6 > 0 || $id_customer == 1){
						
						if($id_customer == 1){
							$name = "Cliente Snack4me";
						}
						
						include ('lib/utilities.php');
						
						$titulo = $order_tracking_number . ".pdf";
						$html = geraHTML($order_id);
						$bolPDF = geraPDF($titulo, $html);

						$assunto = "Pedido realizado - " . $order_tracking_number;
						$message = "Prezado(a), a Snack4me agradece pela compra realizada. Você pode verificar o status do seu pedido fazendo login em sua conta. Confirmação do seu pedido está em anexo.<br /><br />";
							
						$message .= "Obrigado<br /><a href='http://www.snack4me.com' target='_blank'><img src='http://www.snack4me.com/global/images/logo_small.png' title='Snack4me'></a><br /><br />";
						$message .= "<span style='font-size:9px;color:#d5d5d5'>Favor não responder o email.</span><br />";
						
						$envia_email = enviarEmail($name, $email, $assunto, $message, $tipo = $bolPDF, $titulo);
					}
					
					$dados['id_order'] = $order_id;
					$dados['num_order'] = $order_tracking_number;
					$dados['ve'] = $ve;
					
					$resposta["error"] = false;
					$resposta["status"] = 2;
					$resposta["response"] = $dados;
					
				} catch (Exception $e){
					
					$assunto = "Erro no envio de email";
					$message = "Erro no envio de email do pedido ".$order_tracking_number."<br /><br />";
					$name = "Erro Sales";
					$email = "sales@snack4me.com";
					
					$envia_email = enviarEmail($name, $email, $assunto, $message);
					
					$dados['id_order'] = $order_id;
					$dados['num_order'] = $order_tracking_number;
					$dados['ve'] = $ve;
					
					$resposta["error"] = false;
					$resposta["status"] = 2;
					$resposta["response"] = $dados;
				}
				
			} catch (Exception $e) {
				$resposta["error"] = false;
				$resposta["status"] = 1;
				$resposta["message"] = "Erro ao realizar o pedido.";
			}
		}
	}
	
} catch(PDOException $e){
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);