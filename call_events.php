<?php       

$resposta = array();
$events = array();

try{
	include_once('conexao.php');
	$city = $_REQUEST['city'];
	if ($city){
		$query = $con->prepare('SELECT *, DATE_FORMAT(event_date, "%d/%m/%Y") as date, DATE_FORMAT(event_date, "%H:%i") as hour, DATE_FORMAT(event_initial_time, "%H:%i") as initial_time, DATE_FORMAT(event_final_time, "%H:%i") as final_time, sport_name
	            				FROM event INNER JOIN sport ON event.event_sport_id = sport.sport_id WHERE event_city_id = ? AND event_date >= "'.date("Y-m-d H:i:s").'" 
								ORDER BY date ASC');
	    $query->bindParam(1, $city, PDO::PARAM_INT);
	    $query->execute();
	    
	    while ($row = $query->fetch()){
	    	$event['id'] = $row['event_id'];
	    	$event['name'] = $row['event_name'];
	    	$event['date'] = $row['date'];
	    	$event['schedule'] = $row['hour'];
	    	$event['initial_time'] = $row['initial_time'];
	    	$event['final_time'] = $row['final_time'];
	    	$event['image'] = $row['event_image'];
	    	$event['description'] = $row['event_description'];
	    	$event['league'] = $row['sport_name'];
	    	$events[] = $event;
	    }
	    
	    $resposta["error"] = false;
		$resposta["response"] = $events;
	}       
    
} catch (Exception $e){

	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);