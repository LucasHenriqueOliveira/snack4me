<?php

$resposta = array();

try{
	include_once('conexao.php');
	include('class/EventConfiguration.php');
	
	$id_event = $_REQUEST['id_event'];
	
	$query = $con->prepare('SELECT * FROM event_configuration WHERE event_id = ?');
	$query->execute(array($id_event));
	$query->setFetchMode(PDO::FETCH_CLASS, 'EventConfiguration');
	$row = $query->fetch();
	
	if($row->getEventConfigurationFloor()){
		$conf['floor'] = true;
	} else{
		$conf['floor'] = false;
	}
	
	if($row->getEventConfigurationSector()){
		$conf['sector'] = true;
	} else{
		$conf['sector'] = false;
	}
	
	if($row->getEventConfigurationBlock()){
		$conf['block'] = true;
	} else{
		$conf['block'] = false;
	}
	
	if($row->getEventConfigurationRow()){
		$conf['row'] = true;
	} else{
		$conf['row'] = false;
	}
	
	$resposta["conf"] = $conf;
	$resposta["error"] = false;	

} catch (Exception $e){
	
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);
