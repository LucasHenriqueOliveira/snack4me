<?php

$resposta = array();
$sectors = array();

try{
	include_once('conexao.php');
	include('class/EventSector.php');
	
	$id_event = $_REQUEST['id_event'];
	$id_floor = $_REQUEST['id_floor'];
	
	if($id_floor > 0){
		$isFloor = ' AND event_sector_floor_id = '.$id_floor;
	} 
	
	$query = $con->prepare('SELECT * FROM event_sector WHERE event_id = ?' . $isFloor);
	$query->execute(array($id_event));
	$query->setFetchMode(PDO::FETCH_CLASS, 'EventSector');
	
	while($row = $query->fetch()){
		$sector['id_sector'] = $row->getEventSectorId();
		$sector['name_sector'] = $row->getEventSector();
		$sectors[] = $sector;
	}
	
	$resposta["sectors"] = $sectors;
	$resposta["error"] = false;	

} catch (Exception $e){
	
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);
