<?php 
require_once('lib/functions.php');
if(!validRequest()) {
	http_response_code(401); 
	die;
}

/*
session_start();
$headerToken = $_SERVER['HTTP_ACCESS_TOKEN'];
$sessionToken = $_SESSION['XSRF'];

if(!isset($sessionToken) && $headerToken != $sessionToken){
	$resposta["error"] = false;
	$resposta["status"] = 3;
	$resposta["message"] = "Favor realizar novamente o login.";

	echo json_encode($resposta);
	die;
}
*/

date_default_timezone_set('America/Sao_Paulo');
$resposta = array();

if($_POST['checkout'] == ''){
	try{
		include_once('conexao.php');
		
		$num_products = filter_var($_POST['num_products'], FILTER_SANITIZE_NUMBER_INT);
		$prod_qtd = array();
		$itens = 0;
		
		$products = ' AND (';
		for($i = 0; $i < $num_products; $i++){
			if($_POST['qtd_product_'.$i] > 0){
				$itens++;
				$products .= 'product_id = '.$_POST['id_product_'.$i] . ' OR ';
				$prod_qtd[$_POST['id_product_'.$i]] = $_POST['qtd_product_'.$i];
			}
		}
		$products = substr($products,0,-3);
		$products .= ')';
		
		$id_event = filter_var($_POST['id_event'], FILTER_SANITIZE_NUMBER_INT);
		$seat = $_POST['seat'];
		$field_sector = '';
		$field_floor = '';
		$table_sector = '';
		$table_floor = '';
		$sector = '';
		$floor = '';
		$coupon = '';
		$room = '';
		$table = '';
		$coupon_number = '';
		$coupon_tax = '';
		$coupon_id = null;
		$num_rows3 = 0;
		$id_user = filter_var($_POST['id_user'], FILTER_SANITIZE_NUMBER_INT);
		$tip = $_POST['tip'];
		
		if(isset($_POST['sector']) && !empty($_POST['sector']) && $_POST['sector'] != 'null'){
			$sector = ' AND es.event_sector_id = '.$_POST['sector'];
			$field_sector = ", es.event_sector";
			$table_sector = ' INNER JOIN event_sector as es on e.event_id=es.event_id';
		}
		
		if(isset($_POST['floor']) && !empty($_POST['floor']) && $_POST['floor'] != 'null'){
			$floor = ' AND ef. event_floor_id = '.$_POST['floor'];
			$field_floor = ", ef.event_floor";
			$table_floor = ' INNER JOIN event_floor as ef ON e.event_id =  ef.event_id';
		}
		
		if(isset($_POST['coupon']) && !empty($_POST['coupon']) && $_POST['coupon'] != 'null'){
			$coupon = filter_var($_POST['coupon'], FILTER_SANITIZE_MAGIC_QUOTES);
		
			include('class/Coupon.php');
			$sql3 = 'SELECT c.coupon_number, c.coupon_tax, c.coupon_id, c.coupon_event_id FROM `coupon` as c WHERE c.coupon_number = ? AND coupon_sin_used="N"';
		
			$query3 = $con->prepare($sql3);
			$query3->execute(array($coupon));
			$num_rows3 = $query3->rowCount();
			$query3->setFetchMode(PDO::FETCH_CLASS, 'Coupon');
		
			if($num_rows3 > 0){
				$row3 = $query3->fetch();
				if($row3->getCouponEventId() == '' || $row3->getCouponEventId() == $id_event){
					$coupon_number = $row3->getCouponNumber();
					$coupon_tax = $row3->getCouponTax();
					$coupon_id = $row3->getCouponId();
				}
			}
		}
		
		if(isset($_POST['room']) && !empty($_POST['room']) && $_POST['room'] != 'null'){
			$room = $_POST['room'];
		}
		
		if(isset($_POST['table']) && !empty($_POST['table']) && $_POST['table'] != 'null'){
			$table = $_POST['table'];
		}
		
		include('class/Event.php');
		$sql2 = 'SELECT e.event_name, TIME_FORMAT(e.event_initial_time, "%H:%i") as event_initial_time, TIME_FORMAT(e.event_final_time, "%H:%i") as event_final_time, DATE_FORMAT(e.event_date, "%d/%m/%Y") as date, e.event_date, e.event_tax_service'.$field_sector.$field_floor.' FROM `event` as e
		'.$table_sector.$table_floor.'
		WHERE e.event_id ='.$id_event.$sector.$floor;
		
		$query2 = $con->prepare($sql2);
		$query2->execute(array($id_event));
		$query2->setFetchMode(PDO::FETCH_CLASS, 'Event');
		$row2 = $query2->fetch();
		
		$dados['event_name'] = $row2->getEventName();
		$dados['event_date'] = $row2->getDate();
		$dados['date_now'] = date('d/m/Y');
		$dados['full_datetime_now'] = date('Y-m-d H:i:s');
		$dados['full_datetime_event'] = $row2->getEventDate();
		$dados['initial_time'] = $row2->getEventInitialTime();
		$dados['final_time'] = $row2->getEventFinalTime();
		$dados['sector'] = $row2->getEventSector();
		$dados['floor'] = $row2->getEventFloor();
		$dados['tax_service'] = $row2->getEventTaxService();
		
		include('class/Product.php');
		$sql = 'SELECT `product_id`,`product_name`,`product_price` FROM product WHERE product_event_id = ?'. $products;
		$query = $con->prepare($sql);
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		$query->setFetchMode(PDO::FETCH_CLASS, 'Product');
		
		if($num_rows > 0){
			$total = 0;
			$i = 0;
			$product[] = array();
		
			while ($row = $query->fetch()){
				$produto['id_produto'] = $row->getProductId();
				$produto['name'] = $row->getProductName();
				$produto['price'] = $row->getProductPrice();
				$produto['quantity'] = $prod_qtd[$row->getProductId()];
				$produto['subtotal_unit'] = number_format($row->getProductPrice() * $prod_qtd[$row->getProductId()],2);
				
				$produtos[] = $produto;
				$subtotal += $row->getProductPrice() * $prod_qtd[$row->getProductId()];
					
				$product[$i]['id'] = $row->getProductId();
				$product[$i]['price'] = $row->getProductPrice();
				$product[$i]['quantity'] = $prod_qtd[$row->getProductId()];
				$product[$i]['subtotal'] = number_format($row->getProductPrice() * $prod_qtd[$row->getProductId()],2);
				$i++;
			}
		
			if($num_rows3 > 0){
				$discount = number_format($subtotal * $coupon_tax,2);
				$total = number_format($subtotal - $discount,2);
			} else{
				$discount = '0.00';
				$total = number_format($subtotal,2);
			}
			$subtotal = number_format($subtotal, 2);
			$tip = number_format($tip, 2);
			
			$dados['products'] = $produtos;
			$dados['seat'] = $seat;
			$dados['itens'] = $itens;
			$dados['subtotal'] = $subtotal;
			$dados['discount'] = $discount;
			$dados['tip'] = $tip;
			$dados['coupon_number'] = $coupon_number;
			$dados['coupon_tax'] = $coupon_tax;
			$dados['total'] = $total;
			$date = date('Y-m-d H:i:s');
			
			try{
				$con->beginTransaction();
				
				$sql = "INSERT INTO checkout (`checkout_customer_id`,`checkout_date`,`checkout_chair`,`checkout_table`,
				`checkout_floor`,`checkout_sector`,`checkout_price`,`checkout_price_discount`, `checkout_tip`,
				`checkout_price_total`,`checkout_coupon_id`,`checkout_event_id`)
				VALUES(" . $id_user . ", '" . $date . "', '" . $seat . "', '" . $table . "',
				'" . $_POST['floor'] . "', '" . $_POST['sector'] . "', " . $subtotal . ",
				" . $discount . ", " . $tip . ", " . $total . ", '" . $coupon_id . "', " . $id_event . ")";
	
				$query = $con->prepare($sql);
				$query->execute();
				
				$checkout_id = $con->lastInsertId();
				
				for($i = 0; $i < count($product); $i++){
				
					$sql = "INSERT INTO checkout_item (`checkout_item_checkout_id`, `checkout_item_product_id`,
					`checkout_item_price_unit`, `checkout_item_quantity`, `checkout_item_price_total`)
					VALUES(" . $checkout_id . ", " . $product[$i]['id'] . ",
					" . $product[$i]['price'] . ", " . $product[$i]['quantity'] . ", " . $product[$i]['subtotal'] . ")";
				
					$query = $con->prepare($sql);
					$query->execute();
				
				}
				$dados['checkout_id'] = $checkout_id;
				
				$con->commit();
				
				$resposta["error"] = false;
				$resposta["status"] = 2;
				$resposta['response'] = $dados;
				
			} catch (Exception $e){
				$con->rollBack();
					
				$resposta["error"] = false;
				$resposta["status"] = 1;
				$resposta["message"] = "Erro ao salvar dados do pedido";
			}
			
		} else{
			$resposta["error"] = false;
			$resposta["status"] = 1;
			$resposta["message"] = 'Erro ao salvar dados do pedido';
		}
	} catch (Exception $e){
	
		$resposta["error"] = true;
		$resposta["message"] = $e->getMessage();
	}
	
} else{
	try{
		include_once('conexao.php');
		$checkout_id = $_POST['checkout'];
		
		include('class/Checkout.php');
		$sql = 'SELECT * FROM checkout WHERE checkout_id = ?';
		$query = $con->prepare($sql);
		$query->execute(array($checkout_id));
		$num_rows = $query->rowCount();
		$query->setFetchMode(PDO::FETCH_CLASS, 'Checkout');
		$row = $query->fetch();
	
		if($num_rows > 0){
			
			if($row->getCheckoutSinOrder() == 1){
				$resposta["error"] = false;
				$resposta["status"] = 1;
				$resposta["message"] = 'Pedido já realizado.';
				
			} else{
				$sector_id = $row->getCheckoutSector();
				$floor_id = $row->getCheckoutFloor();
				$coupon_id = $row->getCheckoutCouponId();
				$event_id = $row->getCheckoutEventId();
				$seat = $row->getCheckoutChair();
				$tip = $row->getCheckoutTip();
				$table_sector = $table_floor = $field_sector = $field_floor = $sector = $floor = '';
					
				if(isset($sector_id) && !empty($sector_id) && $sector_id != 'null'){
					$sector = ' AND es.event_sector_id = '.$sector_id;
					$field_sector = ", es.event_sector";
					$table_sector = ' INNER JOIN event_sector as es on e.event_id=es.event_id';
				}
				
				if(isset($floor_id) && !empty($floor_id) && $floor_id != 'null'){
					$floor = ' AND ef. event_floor_id = '.$floor_id;
					$field_floor = ", ef.event_floor";
					$table_floor = ' INNER JOIN event_floor as ef ON e.event_id =  ef.event_id';
				}
				
				if(isset($coupon_id) && !empty($coupon_id) && $coupon_id != 'null'){
					include('class/Coupon.php');
					$sql3 = 'SELECT c.coupon_number, c.coupon_tax, c.coupon_id, c.coupon_event_id FROM `coupon` as c WHERE c.coupon_id = ? AND coupon_sin_used="N"';
						
					$query3 = $con->prepare($sql3);
					$query3->execute(array($coupon_id));
					$num_rows3 = $query3->rowCount();
					$query3->setFetchMode(PDO::FETCH_CLASS, 'Coupon');
						
					if($num_rows3 > 0){
						$row3 = $query3->fetch();
						if($row3->getCouponEventId() == '' || $row3->getCouponEventId() == $event_id){
							$coupon_number = $row3->getCouponNumber();
							$coupon_tax = $row3->getCouponTax();
						}
					}
				}
					
				include('class/Event.php');
				$sql2 = 'SELECT e.event_tax_service, e.event_name, TIME_FORMAT(e.event_initial_time, "%H:%i") as event_initial_time, TIME_FORMAT(e.event_final_time, "%H:%i") as event_final_time, DATE_FORMAT(e.event_date, "%d/%m/%Y") as date, e.event_date'.$field_sector.$field_floor.' FROM `event` as e
				'.$table_sector.$table_floor.'
				WHERE e.event_id = ?'.$sector.$floor;
				
				$query2 = $con->prepare($sql2);
				$query2->execute(array($event_id));
				$query2->setFetchMode(PDO::FETCH_CLASS, 'Event');
				$row2 = $query2->fetch();
				$date_now = new DateTime(date('Y-m-d H:i:s'));
				$date = explode(" ", $row2->getEventDate());
				$date_event = new DateTime($date[0] . ' ' .$row2->getEventFinalTime());
					
				if($date_now > $date_event){
					$resposta["error"] = false;
					$resposta["status"] = 1;
					$resposta["message"] = 'Data do evento excedida.';
				
				} else{
				
					$dados['event_name'] = $row2->getEventName();
					$dados['event_date'] = $row2->getDate();
					$dados["tax_service"] = $row2->getEventTaxService();
					$dados['date_now'] = date('d/m/Y');
					$dados['full_datetime_now'] = date('Y-m-d H:i:s');
					$dados['full_datetime_event'] = $row2->getEventDate() . ' ' .$row2->getEventInitialTime().':00';
					$dados['initial_time'] = $row2->getEventInitialTime();
					$dados['final_time'] = $row2->getEventFinalTime();
					$dados['sector'] = $row2->getEventSector();
					$dados['floor'] = $row2->getEventFloor();
				
					include('class/CheckoutItem.php');
					$query5 = $con->prepare('SELECT * FROM checkout_item WHERE `checkout_item_checkout_id` = ?');
					$query5->execute(array($checkout_id));
					$num_rows5 = $query5->rowCount();
					$query5->setFetchMode(PDO::FETCH_CLASS, 'CheckoutItem');
				
					$prod_qtd = array();
					$itens = 0;
				
					$products = ' AND (';
					while($row5 = $query5->fetch()){
						$itens++;
						$products .= 'product_id = '.$row5->getCheckoutItemProductId() . ' OR ';
						$prod_qtd[$row5->getCheckoutItemProductId()] = $row5->getCheckoutItemQuantity();
					}
					$products = substr($products,0,-3);
					$products .= ')';
				
					include('class/Product.php');
					$sql4 = 'SELECT `product_id`,`product_name`,`product_price` FROM product WHERE product_event_id = ?'. $products;
					$query4 = $con->prepare($sql4);
					$query4->execute(array($event_id));
					$num_rows4 = $query4->rowCount();
					$query4->setFetchMode(PDO::FETCH_CLASS, 'Product');
				
					if($num_rows4 > 0){
						$total = 0;
						$i = 0;
						$product[] = array();
				
						while ($row4 = $query4->fetch()){
							$produto['id_produto'] = $row4->getProductId();
							$produto['name'] = $row4->getProductName();
							$produto['price'] = $row4->getProductPrice();
							$produto['quantity'] = $prod_qtd[$row4->getProductId()];
							$produto['subtotal_unit'] = number_format($row4->getProductPrice() * $prod_qtd[$row4->getProductId()],2);
				
							$produtos[] = $produto;
							$subtotal += $row4->getProductPrice() * $prod_qtd[$row4->getProductId()];
				
							$product[$i]['id'] = $row4->getProductId();
							$product[$i]['price'] = $row4->getProductPrice();
							$product[$i]['quantity'] = $prod_qtd[$row4->getProductId()];
							$product[$i]['subtotal'] = number_format($row4->getProductPrice() * $prod_qtd[$row4->getProductId()],2);
							$i++;
						}
							
						if($num_rows3 > 0){
							$discount = number_format($subtotal * $coupon_tax,2);
							$total = number_format($subtotal - $discount,2);
						} else{
							$discount = '0.00';
							$total = number_format($subtotal,2);
						}
						$subtotal = number_format($subtotal, 2);
						$tip = number_format($tip, 2);
				
						$dados['products'] = $produtos;
						$dados['seat'] = $seat;
						$dados['itens'] = $itens;
						$dados['subtotal'] = $subtotal;
						$dados['discount'] = $discount;
						$dados['tip'] = $tip;
						$dados['coupon_number'] = $coupon_number;
						$dados['coupon_tax'] = $coupon_tax;
						$dados['total'] = $total;
						$dados['checkout_id'] = $checkout_id;
							
						$resposta["error"] = false;
						$resposta["status"] = 2;
						$resposta['response'] = $dados;
							
					} else{
						$resposta["error"] = false;
						$resposta["status"] = 1;
						$resposta["message"] = "Erro ao salvar dados do pedido";
					}
				}
			}
		} else{
			$resposta["error"] = false;
			$resposta["status"] = 1;
			$resposta["message"] = "Dados do pedido inválidos";
		}
	} catch(PDOException $e){
		$resposta["error"] = true;
		$resposta["message"] = $e->getMessage();
	}
}

echo json_encode($resposta);
