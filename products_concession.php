<?php 

$resposta = array();
$produtos = array();

try{
	$id_concession = $_POST['id_concession'];
	$id_event = $_POST['id_event'];
	$tax_service = '0.10';
	
	include_once('conexao.php');
	include('class/Product.php');
	include('class/Event.php');
	
	$query = $con->prepare('SELECT * FROM product
			WHERE product_event_id = ? AND product_concession_id = ? AND product_inventory_current > 0');
	$query->execute(array($id_event, $id_concession));
	$num_rows = $query->rowCount();
	$query->setFetchMode(PDO::FETCH_CLASS, 'Product');
	
	if($num_rows > 0){
		while ($row = $query->fetch()){
			$produto['id'] = $row->getProductId();
			$produto['name'] = $row->getProductName();
			$produto['image'] = $row->getProductImage();
			$produto['price'] = $row->getProductPrice();
			$produto['desc'] = $row->getProductShortDesc();
			$produtos[] = $produto;
		}
	}
	
	$query = $con->prepare('SELECT event_tax_service FROM event WHERE event_id = ?');
	$query->execute(array($id_event));
	$num_rows = $query->rowCount();
	$query->setFetchMode(PDO::FETCH_CLASS, 'Event');
	
	if($num_rows > 0){
		$row = $query->fetch();
		$tax_service = $row->getEventTaxService();
	}
	
	$resposta["error"] = false;
	$resposta["products"] = $produtos;
	$resposta["tax_service"] = $tax_service;
	
} catch (Exception $e){

	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);