<?php

require_once('lib/functions.php');
if(!validRequest()) {
	http_response_code(401); 
	die;
}

/*
session_start();
$headerToken = $_SERVER['HTTP_ACCESS_TOKEN'];
$sessionToken = $_SESSION['XSRF'];

if(!isset($sessionToken) && $headerToken != $sessionToken){
	$resposta["error"] = false;
	$resposta["status"] = 3;
	$resposta["message"] = "Favor realizar novamente o login.";

	echo json_encode($resposta);
	die;
}
*/

$resposta = array();
$products = array();

try{
	include_once('conexao.php');
	include('class/Order.php');
	
	$order = $_REQUEST['order'];
		
	$query = $con->prepare('SELECT *,
			DATE_FORMAT(o.order_date, "%d/%m/%Y %H:%i") as order_date,
			DATE_FORMAT(e.event_date, "%d/%m/%Y") as event_date,
			o.order_tip, o.order_tax_service
			FROM `order` as o INNER JOIN `event` as e ON o.order_event_id = e.event_id
			INNER JOIN `status` as s ON o.order_status_id = s.status_id
			WHERE order_id = ? LIMIT 1');
	$query->execute(array($order));
	$num_rows = $query->rowCount();
	$query->setFetchMode(PDO::FETCH_CLASS, 'Order');
	
	if($num_rows > 0){
		$row = $query->fetch();
		$date_time = explode(" ", $row->getOrderDate());
		
		$input = "o=" . $row->getOrderId() . "&n=" . $row->getOrderTrackingNumber();
		$arr = array("/" => "#", "\\" => "&");
		$ve = rtrim(strtr(base64_encode(gzdeflate($input, 9)), $arr), '=');
		
		$pedido['id_order'] = $row->getOrderId();
		$pedido['num_order'] = $row->getOrderTrackingNumber();
		$pedido['date'] = $date_time[0];
		$pedido['hour'] = $date_time[1];
		$pedido['event_name'] = $row->getEventName();
		$pedido['event_date'] = $row->getEventDate();
		$pedido['ve'] = $ve;
		
		if($row->getOrderFloor() != ''){
			include('class/EventFloor.php');
				
			$query4 = $con->prepare('SELECT * FROM event_floor WHERE event_floor_id = ?');
			$query4->execute(array($row->getOrderFloor()));
			$query4->setFetchMode(PDO::FETCH_CLASS, 'EventFloor');
			$row4 = $query4->fetch();
			
			$pedido['floor'] = $row4->getEventFloor();
		} else{ 
			$pedido['floor'] = ""; 
		}
		
		if($row->getOrderSector() != ''){
			include('class/EventSector.php');
		
			$query3 = $con->prepare('SELECT * FROM event_sector WHERE event_sector_id = ?');
			$query3->execute(array($row->getOrderSector()));
			$query3->setFetchMode(PDO::FETCH_CLASS, 'EventSector');
			$row3 = $query3->fetch();
				
			$pedido['sector'] = $row3->getEventSector();
		} else{
			$pedido['sector'] = "";
		}
		
		if($row->getOrderChair() != ''){
			$pedido['seat'] = $row->getOrderChair();
		}
		
		include('class/Item.php');
			
		$query2 = $con->prepare('SELECT i.item_price_total,i.item_price_unit,i.item_quantity,
				p.product_name, p.product_short_desc, p.product_concession_id
				FROM item as i INNER JOIN product as p ON i.item_product_id = p.product_id
				WHERE item_order_id = ?');
		$query2->execute(array($order));
		$query2->setFetchMode(PDO::FETCH_CLASS, 'Item');
			
		while ($row2 = $query2->fetch()){
			$product['name'] = $row2->getProductName();
			$product['desc'] = $row2->getProductShortDesc();
			$product['price_unit'] = $row2->getItemPriceUnit();
			$product['quantity'] = $row2->getItemQuantity();
			$product['price_total'] = $row2->getItemPriceTotal();
			$products[] = $product;
			$id_concession = $row2->getProductConcessionId();
		}
		
		$pedido['products'] = $products;
		$pedido['price'] = $row->getOrderPrice();
		$pedido['discount'] = $row->getOrderPriceDiscount();
		$pedido['tip'] = $row->getOrderTip();
		$pedido['tax_service'] = $row->getOrderTaxService();
		$pedido['price_total'] = $row->getOrderPriceTotal();
		
		if($id_concession){
			include('class/Concession.php');
				
			$query3 = $con->prepare('SELECT concession_name, concession_image
					FROM concession
					WHERE concession_id = ?');
			$query3->execute(array($id_concession));
			$query3->setFetchMode(PDO::FETCH_CLASS, 'Concession');
			$row3 = $query3->fetch();
			
			$pedido['name_concession'] = $row3->getConcessionName();
			$pedido['image_concession'] = $row3->getConcessionImage();
		}
	
		$resposta["error"] = false;
		$resposta["response"] = $pedido;
		$resposta["status"] = 2;
		
	} else{
		$resposta["error"] = false;
		$resposta["message"] = 'Pedido não encontrado.';
		$resposta["status"] = 1;
	}
} catch(PDOException $e){
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);