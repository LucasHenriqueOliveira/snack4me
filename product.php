<?php 
error_reporting(E_ALL);
ini_set("display_errors", 1);

$resposta = array();
$produtos = array();
$concessions = array();

try{
	$id_event = $_REQUEST['id_event'];
	
	include_once('conexao.php');
	include('class/Concession.php');
	include('class/Product.php');
	include('class/Event.php');
	$tax_service = '0.10';
	
	$query = $con->prepare('SELECT * FROM concession WHERE concession_event_id = ?');
	$query->execute(array($id_event));
	$num_rows = $query->rowCount();
	$query->setFetchMode(PDO::FETCH_CLASS, 'Concession');
	
	if($num_rows > 0){
		while ($row = $query->fetch()){
			$concession['id'] = $row->getConcessionId();
			$concession['name'] = $row->getConcessionName();
			$concession['image'] = $row->getConcessionImage();
			
			$query2 = $con->prepare('SELECT * FROM product
					WHERE product_event_id = ? AND product_inventory_current > 0 AND product_concession_id = ?');
			$query2->execute(array($id_event, $row->getConcessionId()));
			$query2->setFetchMode(PDO::FETCH_CLASS, 'Product');
			
			while ($row2 = $query2->fetch()){
				$produto['id'] = $row2->getProductId();
				$produto['name'] = $row2->getProductName();
				$produto['image'] = $row2->getProductImage();
				$produto['price'] = $row2->getProductPrice();
				$produto['desc'] = $row2->getProductShortDesc();
				$produtos[] = $produto;
			}
			$concession['products'] = $produtos;
			unset($produtos);
			$concessions[] = $concession;
		}
		
		$query = $con->prepare('SELECT event_tax_service FROM event WHERE event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		$query->setFetchMode(PDO::FETCH_CLASS, 'Event');
		
		if($num_rows > 0){
			$row = $query->fetch();
			$tax_service = $row->getEventTaxService();
		}
		
		$resposta["error"] = false;
		$resposta["concessions"] = $concessions;
		$resposta["tax_service"] = $tax_service;
		
	} else {
		
		$query = $con->prepare('SELECT * FROM product
				WHERE product_event_id = ? AND product_inventory_current > 0');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		$query->setFetchMode(PDO::FETCH_CLASS, 'Product');
		
		if($num_rows > 0){
			while ($row = $query->fetch()){
				$produto['id'] = $row->getProductId();
				$produto['name'] = $row->getProductName();
				$produto['image'] = $row->getProductImage();
				$produto['price'] = $row->getProductPrice();
				$produto['desc'] = $row->getProductShortDesc();
				$produtos[] = $produto;
			}
		}
		
		$query = $con->prepare('SELECT event_tax_service FROM event WHERE event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		$query->setFetchMode(PDO::FETCH_CLASS, 'Event');
		
		if($num_rows > 0){
			$row = $query->fetch();
			$tax_service = $row->getEventTaxService();
		}
		
		$concession['id'] = '0';
		$concession['name'] = 'Concessão';
		$concession['image'] = 'concession.png';
		$concession['products'] = $produtos;
		$concessions[] = $concession;
		
		$resposta["error"] = false;
		$resposta["concessions"] = $concessions;
		$resposta["tax_service"] = $tax_service;
	}
} catch (Exception $e){

	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);