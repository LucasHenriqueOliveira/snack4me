<?php

require_once('lib/functions.php');
if(!validRequest()) {
	http_response_code(401); 
	die;
}

/*
session_start();
$headerToken = $_SERVER['HTTP_ACCESS_TOKEN'];
$sessionToken = $_SESSION['XSRF'];

if(!isset($sessionToken) && $headerToken != $sessionToken){
	$resposta["error"] = false;
	$resposta["status"] = 3;
	$resposta["message"] = "Favor realizar novamente o login.";

	echo json_encode($resposta);
	die;
}
*/

$resposta = array();

try{
	include_once('conexao.php');
	
	$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
	$name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
	$password = filter_var($_POST['password'], FILTER_SANITIZE_MAGIC_QUOTES);
	$ve = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
	
	include('class/Customer.php');
	$sql = 'SELECT c.customer_email, c.customer_password, c.customer_id, c.customer_type FROM customer c WHERE c.customer_id = ?';
	$query = $con->prepare($sql);
	$query->execute(array($ve));
	$query->setFetchMode(PDO::FETCH_CLASS, 'Customer');
	$num_rows = $query->rowCount();
	$row = $query->fetch();
	
	if($num_rows > 0){
	
		if(sha1($password) == $row->getCustomerPassword()){
			$email_antigo = $row->getCustomerEmail();
	
			if($email_antigo != $email){
				$sql3 = 'SELECT c.customer_email, c.customer_id FROM customer c WHERE c.customer_email = ?';
				$query3 = $con->prepare($sql3);
				$query3->execute(array($email));
				$num_rows3 = $query3->rowCount();
					
				if($num_rows3 == 0){
					$sql2 = 'UPDATE customer SET customer_email = ?, customer_name = ? WHERE customer_id = ?';
					$query2 = $con->prepare($sql2);
					$query2->execute(array($email, $name, $row->getCustomerId()));
					$num_rows2 = $query2->rowCount();
	
					if($num_rows2 > 0){
						
						$resposta["error"] = false;
						$resposta["status"] = 2;
						$resposta["message"] = "Dados salvos com sucesso.";
							
						try{
							$sql4 = 'UPDATE customer SET customer_sin_valid = "N", customer_valid_date = "" WHERE customer_id = ?';
							$query4 = $con->prepare($sql4);
							$query4->execute(array($row->getCustomerId()));
							
							$input = "email=" . $email . "&name=" . $name;
							$arr = array("/" => "#", "\\" => "&");
							$ve = rtrim(strtr(base64_encode(gzdeflate($input, 9)), $arr), '=');
								
							include ('lib/utilities.php');
							$assunto = "Alteração de dados na Snack4me";
							$message = "Prezado(a) <strong>". $name . "</strong>, nós precisamos verificar o seu novo email. Para completar esse processo por favor clique no link abaixo ou copie e cole no seu navegador. <br /><br />" ;
							$message .= "Link de verificação: <a href='http://www.snack4me.com/global/#/check-email/".$ve."' target='_blank'>http://www.snack4me.com/global/#/check-email/" . $ve . "</a> <br /><br />";
							$message .= "Obrigado<br /><a href='http://www.snack4me.com' target='_blank'><img src='http://www.snack4me.com/global/images/logo_small.png' title='Snack4me'></a><br /><br />";
							$message .= "<span style='font-size:9px;color:#d5d5d5'>Favor não responder o email.</span><br />";
								
							$envia_email = enviarEmail($name, $email, $assunto, $message);
								
						} catch(PDOException $e){
							$envia_email = false;
						}
					} else{
						$resposta["error"] = false;
						$resposta["status"] = 1;
						$resposta["message"] = "Erro ao salvar os dados.";
					}
				} else{
					$resposta["error"] = false;
					$resposta["status"] = 1;
					$resposta["message"] = "Email já cadastrado.";
				}
			} else{
				$sql2 = 'UPDATE customer SET customer_name = ? WHERE customer_id = ?';
				$query2 = $con->prepare($sql2);
				$query2->execute(array($name, $row->getCustomerId()));
				$num_rows2 = $query2->rowCount();
					
				if($num_rows2 > 0){
					
					$resposta["error"] = false;
					$resposta["status"] = 2;
					$resposta["message"] = "Dados salvos com sucesso.";
					
				} else{
					$resposta["error"] = false;
					$resposta["status"] = 1;
					$resposta["message"] = "Erro ao salvar os dados.";
				}
			}
		} else{
			$resposta["error"] = false;
			$resposta["status"] = 1;
			$resposta["message"] = "Senha não confere.";
		}

	} else{
		$resposta["error"] = false;
		$resposta["status"] = 1;
		$resposta["message"] = "Usuário não encontrado.";
	}
} catch(PDOException $e){
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);