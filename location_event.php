<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

$resposta = array();
$floors = array();
$rows = array();
$seats = array();
$map = array();

try{
	include_once('conexao.php');
	include('class/EventConfiguration.php');
	include('class/EventFloor.php');
	include('class/EventSector.php');
	include('class/EventRow.php');
	include('class/EventSeat.php');
	
	$id_event = $_REQUEST['id_event'];
	
	$query = $con->prepare('SELECT `c`.`event_id`, `e`.`event_name`, `c`.`event_configuration_floor`,
				`c`.`event_configuration_sector`, `c`.`event_configuration_row`, `c`.`event_configuration_chair`
				FROM event_configuration as c
				INNER JOIN event as e ON c.event_id = e.event_id WHERE c.event_id = ?');
	$query->execute(array($id_event));
	$query->setFetchMode(PDO::FETCH_CLASS, 'EventConfiguration');
	$row = $query->fetch();
	$response['arena'] = $row->getEventName();
	$response['id'] = $row->getEventId();
	
	if($row->getEventConfigurationFloor() == 1){
		array_push($map, "level");
	}
	
	if($row->getEventConfigurationSector() == 1){
		array_push($map, "sector");
	}
	
	if($row->getEventConfigurationRow() == 1){
		array_push($map, "row");
	}
	
	if($row->getEventConfigurationChair() == 1){
		array_push($map, "seat");
	}
	
	$response['map'] = $map;
	
	$query = $con->prepare('SELECT * FROM event_floor WHERE event_id = ?');
	$query->execute(array($id_event));
	$query->setFetchMode(PDO::FETCH_CLASS, 'EventFloor');
	
	while($row = $query->fetch()){
		$floor['id'] = $row->getEventFloorId();
		$floor['label'] = $row->getEventFloor();
		
		$query2 = $con->prepare('SELECT * FROM event_sector WHERE event_id = ? AND event_sector_floor_id = ?');
		$query2->execute(array($id_event, $row->getEventFloorId()));
		$query2->setFetchMode(PDO::FETCH_CLASS, 'EventSector');
		
		while($row2 = $query2->fetch()){
			$sector['id'] = $row2->getEventSectorId();
			$sector['label'] = $row2->getEventSector();
			
			$query3 = $con->prepare('SELECT * FROM event_row WHERE event_id = ? AND event_row_sector_id = ?');
			$query3->execute(array($id_event, $row2->getEventSectorId()));
			$query3->setFetchMode(PDO::FETCH_CLASS, 'EventRow');
			
			while($row3 = $query3->fetch()){
				$value_row['id'] = $row3->getEventRowId();
				$value_row['label'] = $row3->getEventRow();
				
				$query4 = $con->prepare('SELECT * FROM event_seat WHERE event_id = ? AND event_seat_row_id = ?');
				$query4->execute(array($id_event, $row3->getEventRowId()));
				$query4->setFetchMode(PDO::FETCH_CLASS, 'EventSeat');
				
				while($row4 = $query4->fetch()){
					$seat['id'] = $row4->getEventSeatId();
					$seat['label'] = $row4->getEventSeat();
					$seats[] = $seat;
				}
				$value_row['seat'] = $seats;
				unset($seats);
				$rows[] = $value_row;
			}
			$sector['row'] = $rows;
			unset($rows);
			$sectors[] = $sector;
			
		}
		$floor['sector'] = $sectors;
		unset($sectors);
		$floors[] = $floor;
	}
	$response['level'] = $floors;
	$resposta = $response;

} catch (Exception $e){
	
	$resposta = $e->getMessage();
}

$dir = "./arenas/" . $id_event;

if (!file_exists($dir)){
	mkdir($dir, 0777);
	shell_exec('chmod -R 777 '.$dir);
}

$file = "./arenas/".$id_event."/config.json";
$fp = fopen($file, "a");
$escreve = fwrite($fp, json_encode($resposta));
fclose($fp);
//echo json_encode($resposta);