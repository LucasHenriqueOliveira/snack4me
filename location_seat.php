<?php

$resposta = array();
$seats = array();

try{
	include_once('conexao.php');
	include('class/EventSeat.php');
	
	$id_event = $_REQUEST['id_event'];
	$id_row = $_REQUEST['id_row'];
	
	$query = $con->prepare('SELECT * FROM event_seat WHERE event_id = ? AND event_seat_row_id = ?');
	$query->execute(array($id_event, $id_row));
	$query->setFetchMode(PDO::FETCH_CLASS, 'EventSeat');
	
	while($row = $query->fetch()){
		$seat['id_seat'] = $row->getEventSeatId();
		$seat['name_seat'] = $row->getEventSeat();
		$seats[] = $seat;
	}
	
	$resposta["seats"] = $seats;
	$resposta["error"] = false;	

} catch (Exception $e){
	
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);
