<?php

$resposta = array();

function separar_parametros($param) {
	return explode('&',$param);
}

function separar_valor($opcao) {
	return explode('=',$opcao);
}

function isMail($email)
{
	$er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
	if(preg_match($er,$email))
		return true;
	else
		return false;
}

try{
	$ve = $_POST['ve'];
	$arr = array("#" => "/", "&" => "\\");
	
	$parametros = separar_parametros(gzinflate(base64_decode(strtr($ve,$arr))));
	foreach($parametros as $opcao) {
		$valor[] = separar_valor($opcao);
	}
	
	$email = '';
	if(isset($valor[0][1])){
		$email = $valor[0][1];
	}
	
	if(isMail($email)){
		
		include_once('conexao.php');
		
		$sql = 'SELECT c.customer_email, c.customer_id, c.customer_name, c.customer_type FROM customer c WHERE c.customer_email = ? AND customer_sin_valid = "N"';
		$query = $con->prepare($sql);
		$query->execute(array($email));
		$num_rows = $query->rowCount();
		
		if($num_rows > 0){
			$row = $query->fetch();
			
			$sql = 'UPDATE customer SET customer_sin_valid = "S", customer_valid_date = ? WHERE customer_email = ?';
			$query = $con->prepare($sql);
			$query->execute(array(date('Y-m-d H:m:s'), $email));
			
			session_start();
			
			$token = hash('sha256', uniqid(mt_rand() . $_SERVER['HTTP_USER_AGENT'], true));
			
			$_SESSION['id'] = $row['customer_id'];
			$_SESSION['email'] = $row['customer_email'];
			$_SESSION['name'] = $row['customer_name'];
			$_SESSION['type'] = $row['customer_type'];
			$_SESSION['XSRF'] = $token;
			
			$user['id'] = $row['customer_id'];
			$user['name'] = $row['customer_name'];
			$user['email'] = $row['customer_email'];
			$user['type'] = $row['customer_type'];
			$user['XSRF'] = $token;
			
			$resposta["error"] = false;
			$resposta["response"] = $user;
			$resposta["message"] = 'Email validado com sucesso.';
			
		} else{
			$resposta["error"] = true;
			$resposta["message"] = 'Email já válido.';
		}
		
	} else{
		$resposta["error"] = true;
		$resposta["message"] = 'Erro na validação do email.';
	}
	
} catch(PDOException $e){
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);