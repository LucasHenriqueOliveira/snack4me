<!DOCTYPE html>
<html ng-app="Reports">
<head>
	<meta charset="utf-8" />

	<title>Snack4me</title>
	<meta name="description" content="">
	<meta name="author" content="snack4me">
	<meta name="HandheldFriendly" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Import CSS -->
	<link rel="stylesheet" href="css/style.css" />
</head>
<body ng-controller="ReportsCtrl">
	
	<select ng-model="evento" id="event" ng-controller="SelectReportsCtrl" ng-change="setGraph(evento)">
		<option value="">Gráficos Gerais</option>
		<optgroup label="Por Evento">
			<option ng-repeat="evento in eventos" value="{{evento.id}}">
				{{evento.name}}
			</option>
		</optgroup>
	</select>
	
	<div ng-view></div>	
	
	<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
	<script src="../js/angular.min.js"></script>
	<script src="../js/angular-route.min.js"></script>
	<script src="../js/jquery.min.js"></script>
	<script src="js/app.js"></script>
	<script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/highcharts-more.js"></script>
    <!--
	<script src="http://code.highcharts.com/highcharts-3d.js"></script>
	-->
</body>
</html>