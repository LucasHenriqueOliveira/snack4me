<?php
include_once('../conexao.php');

$query = $con->prepare('SELECT count(*) as qtd, MONTH(order_date) as mes, YEAR(order_date) as ano, order_date
						FROM `order`
						GROUP BY MONTH(order_date);');

$query->execute();

while ($row = $query->fetch()){
	
	$data = new DateTime($row['order_date']);
	$mes['name'] = $data->format('M').'/'.$row['ano'];
	$qtd['data'] = (int)$row['qtd'];
	$meses[] = $mes;
	$qtds[] = $qtd;
}

$arrs['mes'] = $meses;
$arrs['qtd'] = $qtds;

echo json_encode($arrs);