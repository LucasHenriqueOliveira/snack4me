<?php
include_once('../conexao.php');

$query = $con->prepare("SELECT COUNT(o.order_customer_id) as qtd, 
									case c.customer_type
										when '1' then 'Sem registro'
										when '2' then 'Sócio Snack'
										when '3' then 'Facebook'
										when '4' then 'Google +'
									end as 'name'
								FROM `order` as o INNER JOIN `customer` AS c ON o.order_customer_id = c.customer_id
								GROUP BY c.customer_type");

$query->execute();

while ($row = $query->fetch()){
	$arr['name'] = $row['name'];
	$arr['y'] = (int)$row['qtd'];
	$arrs[] = $arr;
}

echo json_encode($arrs);