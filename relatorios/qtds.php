<?php 

$resposta = array();
$tempoxpedido = array();
$vendaxnivel = array();
$receitaxnivel = array();
$vendaxsetor = array();
$receitaxsetor = array();
$produtos = array();
$vendaxconcessions = array();
$receitaxconcessions = array();
$vendas = array();
$vendaxlogin = array();
$receitaxlogin = array();
$deliverymen = array();
$qtd_product_name = array();
$qtd_product_qtd_sale = array();
$qtd_product_qtd_inventory = array();

try{
	
	$id_event = $_POST['id'];
	$nivel = $_POST['nivel'];
	$setor = $_POST['setor'];
	$produto = $_POST['produto'];
	$concession = $_POST['concession'];
	$venda = $_POST['venda'];
	$initial_time = $_POST['initial_time'].':00';
	$final_time = $_POST['final_time'].':00';
	$tax_service = $_POST['tax_service'];
	include_once('../conexao.php');
	
	
	if($venda > 0){
		$query = $con->prepare('SELECT IF(`o`.`order_schedule_date` = "00:00:00", DATE_FORMAT(`o`.`order_date`, "%H:%i:%s"), `o`.`order_schedule_date`) as time
									FROM `order` as `o` 
									WHERE `o`.`order_event_id` = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		
		if($num_rows > 0){
			
			$HoraEntrada = new DateTime($initial_time);
			$HoraEntrada2 = new DateTime($initial_time);
			$HoraSaida   = new DateTime($final_time);
			
			$diffHoras = $HoraSaida->diff($HoraEntrada);
			$horas = $diffHoras->format('%h');
			$minutos = $diffHoras->format('%i');
			$hora_minuto = $horas * 60;
			$minutos = $minutos + $hora_minuto;
			$div = $minutos/15;
			$periodo = array();
			
			for($i = 0; $i < $div; $i++){
				$HoraEntrada = new DateTime($initial_time);
				$HoraEntrada2 = new DateTime($initial_time);
				$periodo[$i]['inicio'] = $HoraEntrada->modify("+".($i * 15)." minutes");
				$periodo[$i]['fim'] = $HoraEntrada2->modify("+".($i * 15 + 15)." minutes");
				$periodo[$i]['qtd'] = 0;
				$periodo[$i]['legenda'] = $periodo[$i]['inicio']->format("H:i") .' até '. $periodo[$i]['fim']->format("H:i");
			}
			
			while($row = $query->fetch()){
				$order = new DateTime($row['time']);
				
				for($i = 0; $i < count($periodo); $i++){
					if($i == 0){
						if($order >= $periodo[$i]['inicio'] && $order <= $periodo[$i]['fim']){
							$periodo[$i]['qtd']++;
						}
					} else if($order > $periodo[$i]['inicio'] && $order <= $periodo[$i]['fim']){
						$periodo[$i]['qtd']++;
					}
				}
			}
		}
	}
	
	if($nivel > 0){
		$query = $con->prepare('SELECT * FROM qtdxnivel WHERE order_event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		
		if($num_rows > 0){
			while($row = $query->fetch()){
				$floor['name'] = $row['event_floor'];
				$floor['y'] = (int)$row['qtd'];
				$vendaxnivel[] = $floor;
			}
		}
	}
	
	if($nivel > 0){
		$query = $con->prepare('SELECT * FROM receitaxnivel WHERE order_event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
	
		if($num_rows > 0){
			while($row = $query->fetch()){
				$recxnivel['name'] = $row['event_floor'];
				$recxnivel['data'] = [(double)$row['qtd']];
				$receitaxnivel[] = $recxnivel;
			}
		}
	}
	
	if($setor > 0){
		$query = $con->prepare('SELECT * FROM qtdxsetor WHERE order_event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		
		if($num_rows > 0){
			while($row = $query->fetch()){
				$s['name'] = $row['event_sector'];
				$s['y'] = (int)$row['qtd'];
				$vendaxsetor[] = $s;
			}
		}
	}
	
	if($setor > 0){
		$query = $con->prepare('SELECT * FROM receitaxsetor WHERE order_event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
	
		if($num_rows > 0){
			while($row = $query->fetch()){
				$recxsetor['name'] = $row['event_sector'];
				$recxsetor['data'] = [(double)$row['qtd']];
				$receitaxsetor[] = $recxsetor;
			}
		}
	}
	
	if($concession > 0){
		$query = $con->prepare('select 
							        count(`i`.`item_id`) AS `qtd`,
							        `c`.`concession_name` AS `concession_name`
							    from
							        (((`product` `p`
							        join `concession` `c` ON ((`p`.`product_concession_id` = `c`.`concession_id`)))
							        join `item` `i` ON ((`i`.`item_product_id` = `p`.`product_id`)))
							        join `order` `o` ON ((`i`.`item_order_id` = `o`.`order_id`)))
								WHERE order_event_id = ?
							    group by `p`.`product_concession_id`');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		
		if($num_rows > 0){
			while($row = $query->fetch()){
				$conc['name'] = $row['concession_name'];
				$conc['y'] = (int)$row['qtd'];
				$vendaxconcessions[] = $conc;
			}
		}
	}
	
	if($concession > 0){
		$query = $con->prepare('select 
							        SUM(DISTINCT `o`.`order_price_total`) AS `qtd`,
							        `c`.`concession_name` AS `concession_name`
							    from
							        (((`product` `p`
							        join `concession` `c` ON ((`p`.`product_concession_id` = `c`.`concession_id`)))
							        join `item` `i` ON ((`i`.`item_product_id` = `p`.`product_id`)))
							        join `order` `o` ON ((`i`.`item_order_id` = `o`.`order_id`)))
								WHERE order_event_id = ?
							    group by `p`.`product_concession_id`');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
	
		if($num_rows > 0){
			while($row = $query->fetch()){
				$conc['name'] = $row['concession_name'];
				$conc['data'] = [(double)$row['qtd']];
				$receitaxconcessions[] = $conc;
			}
		}
	}
	
	if($produto > 0){
		$query = $con->prepare('SELECT * FROM qtdxproduto WHERE order_event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		
		if($num_rows > 0){
			while($row = $query->fetch()){
				$product['name'] = $row['product_name'];
				$product['y'] = (int)$row['qtd'];
				$produtos[] = $product;
			}
		}
	}
	
	if($venda > 0 && $concession > 0){
		$query = $con->prepare('SELECT * FROM produtoxconcession WHERE order_event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
	
		if($num_rows > 0){
			while($row = $query->fetch()){
				$value = $row['qtd'] + ($row['qtd'] * ($tax_service/100));
				$sale['name'] = $row['product_name'] . ' ('.$row['concession_name'].')';
				$sale['data'] = [$value];
				$vendas[] = $sale;
			}
		}
	} else if($venda > 0){
		$query = $con->prepare('select 
									sum(`o`.`order_price_total`) AS `qtd`,
									`p`.`product_name` AS `product_name`
								from
									((`product` `p`
									join `item` `i` ON ((`i`.`item_product_id` = `p`.`product_id`)))
									join `order` `o` ON ((`i`.`item_order_id` = `o`.`order_id`)))
								WHERE `o`.`order_event_id` = ?
								group by `p`.`product_id`');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		
		if($num_rows > 0){
			while($row = $query->fetch()){
				$sale['name'] = $row['product_name'];
				$sale['data'] = [(double)$row['qtd']];
				$vendas[] = $sale;
			}
		}
	}
	
	if($venda > 0){
		$query = $con->prepare("SELECT COUNT(o.order_customer_id) as qtd, 
									case c.customer_type
										when '1' then 'Sem registro'
										when '2' then 'Sócio Snack'
										when '3' then 'Facebook'
										when '4' then 'Google +'
									end as 'name'
								FROM `order` as o INNER JOIN `customer` AS c ON o.order_customer_id = c.customer_id
								WHERE o.order_event_id = ?
								GROUP BY c.customer_type");
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
	
		if($num_rows > 0){
			while($row = $query->fetch()){
				$login['name'] = $row['name'];
				$login['y'] = (int)$row['qtd'];
				$vendaxlogin[] = $login;
			}
		}
	}
	
	if($venda > 0){
		$query = $con->prepare("SELECT SUM(o.order_price_total) as qtd, 
									case c.customer_type
										when '1' then 'Sem registro'
										when '2' then 'Sócio Snack'
										when '3' then 'Facebook'
										when '4' then 'Google +'
									end as 'name'
								FROM `order` as o INNER JOIN `customer` AS c ON o.order_customer_id = c.customer_id
								WHERE o.order_event_id = ?
								GROUP BY c.customer_type");
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
	
		if($num_rows > 0){
			while($row = $query->fetch()){
				$reclogin['name'] = $row['name'];
				$reclogin['data'] = [(double)$row['qtd']];
				$receitaxlogin[] = $reclogin;
			}
		}
	}
	
	if($venda > 0){
		$query = $con->prepare("select 
									COUNT(`o`.`order_id`) AS `qtd`,
									`u`.`user_name` AS `user_name`
								from
									(`order` `o`
									join `user` `u` ON ((`o`.`order_user_id_delivery` = `u`.`user_id`)))
								WHERE `o`.`order_status_id` = 2 AND `o`.`order_event_id` = ?
								group by `o`.`order_user_id_delivery");
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
	
		if($num_rows > 0){
			while($row = $query->fetch()){
				$delivery['name'] = $row['user_name'];
				$delivery['data'] = [(int)$row['qtd']];
				$deliverymen[] = $delivery;
			}
		}
	}
	
	if($produto > 0){
		$query = $con->prepare('SELECT product_name, product_inventory_qtd, product_inventory_current FROM product WHERE product_event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
	
		if($num_rows > 0){
			while($row = $query->fetch()){
				$product_name['name'] = $row['product_name'];
				$product_qtd_sale['qtd_sale'] = (int)$row['product_inventory_qtd'] - (int)$row['product_inventory_current'];
				$product_qtd_inventory['qtd_inventory'] = (int)$row['product_inventory_current'];
				$qtd_product_name[] = $product_name;
				$qtd_product_qtd_sale[] = $product_qtd_sale;
				$qtd_product_qtd_inventory[] = $product_qtd_inventory;
			}
		}
	}
	
	$resposta["error"] = false;
	$resposta["tempoxpedido"] = $tempoxpedido;
	$resposta["vendaxnivel"] = $vendaxnivel;
	$resposta["receitaxnivel"] = $receitaxnivel;
	$resposta["vendaxsetor"] = $vendaxsetor;
	$resposta["receitaxsetor"] = $receitaxsetor;
	$resposta["produtos"] = $produtos;
	$resposta["vendaxconcessions"] = $vendaxconcessions;
	$resposta["receitaxconcessions"] = $receitaxconcessions;
	$resposta["vendas"] = $vendas;
	$resposta["vendaxlogin"] = $vendaxlogin;
	$resposta["receitaxlogin"] = $receitaxlogin;
	$resposta["deliverymen"] = $deliverymen;
	$resposta["inventory_name"] = $qtd_product_name;
	$resposta["inventory_qtd_sale"] = $qtd_product_qtd_sale;
	$resposta["inventory_qtd_current"] = $qtd_product_qtd_inventory;
	$resposta["periodo"] = $periodo;

} catch (Exception $e){
	
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);