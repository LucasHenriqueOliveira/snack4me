<?php 

$resposta = array();
$eventos = array();
try{
	
	include_once('../conexao.php');
	include('../class/Event.php');

	$query = $con->query('SELECT event_id, event_name, DATE_FORMAT(event_date, "%d/%m/%Y") as date 
						FROM event ORDER BY event_date DESC');
	$num_rows = $query->rowCount();
	$query->setFetchMode(PDO::FETCH_CLASS, 'Event'); 
	
	if($num_rows > 0){
		while ($row = $query->fetch()){
			
			$evento['id'] = $row->getEventId();
			$evento['name'] = $row->getEventName().' - '.$row->getDate();
			$eventos[] = $evento;
		}
	}
	
	$resposta["error"] = false;
	$resposta["response"] = $eventos;
	
} catch (Exception $e){
	
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);