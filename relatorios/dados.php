<?php 

$resposta = array();

try{
	
	$id_event = $_POST['id'];
	include_once('../conexao.php');

	$query = $con->prepare('SELECT * FROM dados_evento WHERE event_id = ?');
	$query->execute(array($id_event));
	$num_rows = $query->rowCount();
	
	if($num_rows > 0){
		$row = $query->fetch();
			
		$evento['id'] = $row['event_id'];
		$evento['name'] = $row['event_name'];
		$evento['date'] = $row['date'];
		$evento['initial_time'] = $row['initial_time'];
		$evento['final_time'] = $row['final_time'];
		$evento['tax_service'] = $row['event_tax_service'];
		$evento['city'] = $row['city_name'];
		
		$query = $con->prepare('SELECT data FROM vendaxeventos WHERE event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		if($num_rows > 0){
			$row = $query->fetch();
			$evento['venda'] = $row['data'];
		} else{
			$evento['venda'] = 0;
		}
		
		$query = $con->prepare('SELECT data FROM receitaxeventos WHERE event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		if($num_rows > 0){
			$row = $query->fetch();
			$evento['receita'] = $row['data'];
		} else{
			$evento['receita'] = 0;
		}
		
		$query = $con->prepare('SELECT data FROM produtoxeventos WHERE event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		if($num_rows > 0){
			$row = $query->fetch();
			$evento['produto'] = $row['data'];
		} else{
			$evento['produto'] = 0;
		}
		
		$query = $con->prepare('SELECT price_min, price_max FROM precoxeventos WHERE event_id = ?');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		if($num_rows > 0){
			$row = $query->fetch();
			$evento['preco_min'] = $row['price_min'];
			$evento['preco_max'] = $row['price_max'];
		} else{
			$evento['preco_min'] = 0;
			$evento['preco_max'] = 0;
		}
		
		$query = $con->prepare('SELECT COUNT(concession_event_id) AS concession FROM concession WHERE concession_event_id = ? GROUP BY concession_event_id');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		if($num_rows > 0){
			$row = $query->fetch();
			$evento['concession'] = $row['concession'];
		} else{
			$evento['concession'] = 0;
		}
		
		$query = $con->prepare('SELECT COUNT(event_id) AS nivel FROM event_floor WHERE event_id = ? GROUP BY event_id');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		if($num_rows > 0){
			$row = $query->fetch();
			$evento['nivel'] = $row['nivel'];
		} else{
			$evento['nivel'] = 0;
		}
		
		$query = $con->prepare('SELECT COUNT(event_id) AS setor FROM event_sector WHERE event_id = ? GROUP BY event_id');
		$query->execute(array($id_event));
		$num_rows = $query->rowCount();
		if($num_rows > 0){
			$row = $query->fetch();
			$evento['setor'] = $row['setor'];
		} else{
			$evento['setor'] = 0;
		}
		
		$resposta["error"] = false;
		$resposta["response"] = $evento;

	} else{
		$resposta["error"] = true;
		$resposta["message"] = $e->getMessage();
	}
} catch (Exception $e){
	
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);