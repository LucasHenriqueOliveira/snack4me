<?php
include_once('../conexao.php');

$query = $con->prepare('SELECT event_name, DATE_FORMAT(event_date, "%d/%m/%Y") as date,
						DATE_FORMAT(event_initial_time, "%H:%i") as initial_time,
						DATE_FORMAT(event_final_time, "%H:%i") as final_time,
						c.city_name, latitude, longitude, event_description
						FROM `event` AS e INNER JOIN `city` as c ON e.event_city_id = c.city_id;');

$query->execute();

while ($row = $query->fetch()){
	$arr['lat'] = (double)$row['latitude'];
	$arr['lng'] = (double)$row['longitude'];
	$arr['nome'] = $row['event_name'];
	$arr['date'] = $row['date'];
	$arr['initial_time'] = $row['initial_time'];
	$arr['final_time'] = $row['final_time'];
	$arr['city_name'] = $row['city_name'];
	$arr['desc'] = $row['event_description'];
	$arrs[] = $arr;
}

echo json_encode($arrs);