"use strict";

(function (){
	var app = angular.module('Reports', ['ngRoute']);
	
	app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'geral.html',
				controller: 'GraphGeneralCtrl'
			})
			.when('/individual/:id', {
				templateUrl: 'individual.html',
				controller: 'GraphIndivualCtrl'
			})
			.otherwise({
		        redirectTo: '/',
		    });

	}]);

	app.controller('ReportsCtrl',['$scope', '$http', '$location', '$log', '$timeout', function($scope, $http, $location, $log, $timeout) {
		$scope.go = function(path) {
	    	$location.path( path );
	    };
	    
	    
	}]);
	
	app.controller('SelectReportsCtrl',['$scope', '$http', '$location', '$log', function($scope, $http, $location, $log) {
		
		$scope.setGraph = function(id) {
			if(id){
				$location.path('/individual/' + id);
			} else{
				$location.path('/');
			}
	    };
	    
		$scope.eventos = [];
    	
    	$http({
			method: 'POST',
			url: 'eventos.php',
			headers:{'Content-type': 'application/x-www-form-urlencoded'}
		})
		.success(function(data){
			if(data.error){
				$log.info(data);
			} else{
				if(data.response){
					$scope.eventos = data.response;
				}
			}
		})
		.error(function(data, status, headers, config){
			throw new Error('Algo deu errado.');
		});
	    
	}]);
	
	app.controller('GraphGeneralCtrl',['$scope', '$http', '$log', '$timeout', function($scope, $http, $log, $timeout) {
		
		$http({
			method: 'POST',
			url: 'map.php',
			headers:{'Content-type': 'application/x-www-form-urlencoded'}
		})
		.success(function(result){
			var map;
			var infoWindow;
			var markersData = result;
			
			function createMarker(latlng, nome, date, initial_time, final_time, city_name, desc){
			   var marker = new google.maps.Marker({
			      map: map,
			      position: latlng,
			      title: nome,
			      icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
			   });

			   google.maps.event.addListener(marker, 'click', function() {
			      
			      var iwContent = '<div id="iw_container">' +
			      '<div class="iw_title">' + nome + '</div>' +
			      '<div class="iw_content">Data: ' + date + '<br />' +
			      'Cidade: ' + city_name + '<br />' +
			      'Início: ' + initial_time + '<br />' +
			      'Fim: ' + final_time + '<br /><br />' +
			      desc + '<br />' +
			      '</div></div>';
			      
			      infoWindow.setContent(iwContent);
			      infoWindow.open(map, marker);
			   });
			}


			function displayMarkers(){
			   var bounds = new google.maps.LatLngBounds();

			   for (var i = 0; i < markersData.length; i++){

			      var latlng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
			      var nome = markersData[i].nome;
			      var date = markersData[i].date;
			      var initial_time = markersData[i].initial_time;
			      var final_time = markersData[i].final_time;
			      var city_name = markersData[i].city_name;
			      var desc = markersData[i].desc;

			      createMarker(latlng, nome, date, initial_time, final_time, city_name, desc);
			      bounds.extend(latlng); 
			   }
			   map.fitBounds(bounds);
			}
			
			var mapOptions = {
	          center: new google.maps.LatLng(35.301861, -54.847693),
	          zoom: 3,
	          mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
			
	        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
	        
	        infoWindow = new google.maps.InfoWindow();
	        
	        google.maps.event.addListener(map, 'click', function() {
	            infoWindow.close();
	         });
	        
	        displayMarkers();
		})
		.error(function(data, status, headers, config){
			throw new Error('Algo deu errado.');
		});	
		
		
		$http({
			method: 'POST',
			url: 'vendaxmes.php',
			headers:{'Content-type': 'application/x-www-form-urlencoded'}
		})
		.success(function(result){
			var data = result;
			var categories = [];
			var qtd = [];
			
			for(var i = 0; i < data.mes.length; i++) {
			    categories.push(data.mes[i].name);
			    qtd.push(data.qtd[i].data);
			}
			
			var chart = new Highcharts.Chart({
		        chart: {
		            renderTo: 'vendaxmes',
		            type: 'column',
		            margin: 75,
		            options3d: {
		                enabled: true,
		                alpha: 15,
		                beta: 15,
		                depth: 50,
		                viewDistance: 25
		            }
		        },
		        title: {
		            text: 'Nº de pedidos por Mês'
		        },
		        subtitle: {
		            text: ''
		        },
		        xAxis: {
		        	categories: categories
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'Nº de pedidos'
		            }
		        },
		        tooltip: {
		            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		            pointFormat: '<tr><td style="color:{series.color};padding:0;font-size:12px">{series.name}: </td>' +
		                '<td style="padding:0"><b>{point.y:1f}</b></td></tr>',
		            footerFormat: '</table>',
		            useHTML: true
		        },
		        plotOptions: {
		            column: {
		                depth: 25,
		                dataLabels: {
		                    enabled: true,
		                    color: 'black'
		                }
		            }
		        },
		        series: [{
		            name: 'Nº de pedidos',
		            data: qtd
		        }]
		    });
		})
		.error(function(data, status, headers, config){
			throw new Error('Algo deu errado.');
		});
		
		        
		$http({
			method: 'POST',
			url: 'vendaxeventos.php',
			headers:{'Content-type': 'application/x-www-form-urlencoded'}
		})
		.success(function(result){
			var data = result;
			
			var chart = new Highcharts.Chart({
		        chart: {
		            renderTo: 'vendaxeventos',
		            type: 'column',
		            margin: 75,
		            options3d: {
		                enabled: true,
		                alpha: 15,
		                beta: 15,
		                depth: 50,
		                viewDistance: 25
		            }
		        },
		        title: {
		            text: 'Nº de pedidos x Eventos'
		        },
		        subtitle: {
		            text: ''
		        },
		        xAxis: {
		            categories: [
		                'Eventos'
		            ]
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'Nº de pedidos'
		            }
		        },
		        tooltip: {
		            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		            pointFormat: '<tr><td style="color:{series.color};padding:0;font-size:12px">{series.name}: </td>' +
		                '<td style="padding:0"><b>{point.y:1f}</b></td></tr>',
		            footerFormat: '</table>',
		            useHTML: true
		        },
		        plotOptions: {
		            column: {
		                depth: 25,
		                dataLabels: {
		                    enabled: true,
		                    color: 'black'
		                }
		            }
		        },
		        series: data
		    });
		})
		.error(function(data, status, headers, config){
			throw new Error('Algo deu errado.');
		});
		
		$http({
			method: 'POST',
			url: 'receitaxeventos.php',
			headers:{'Content-type': 'application/x-www-form-urlencoded'}
		})
		.success(function(result){
			var data = result;
			
			$('#receitaxeventos').highcharts({
		        chart: {
		        	type: 'bar'
		        },
		        title: {
		            text: 'Receita x Eventos'
		        },
		        subtitle: {
		            text: ''
		        },
		        xAxis: {
		            categories: [
		                'Eventos'
		            ]
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'Receita'
		            }
		        },
		        tooltip: {
		            valuePrefix: 'US$'
		        },
		
		        plotOptions: {
		            bar: {
		                dataLabels: {
		                    enabled: true,
		                    formatter: function () {
		                        return 'US$ ' + this.y;
		                    }
		                }
		            }
		        },
		        series: data
			 });
		})
		.error(function(data, status, headers, config){
			throw new Error('Algo deu errado.');
		});
			
		$http({
			method: 'POST',
			url: 'produtoxeventos.php',
			headers:{'Content-type': 'application/x-www-form-urlencoded'}
		})
		.success(function(result){
			var data = result;
			
			var chart = new Highcharts.Chart({
		        chart: {
		            renderTo: 'produtoxeventos',
		            type: 'column',
		            margin: 75,
		            options3d: {
		                enabled: true,
		                alpha: 15,
		                beta: 15,
		                depth: 50,
		                viewDistance: 25
		            }
		        },
		        title: {
		            text: 'Qtd. Produtos x Eventos'
		        },
		        subtitle: {
		            text: ''
		        },
		        xAxis: {
		            categories: [
		                'Eventos'
		            ]
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'Qtd. Produtos'
		            }
		        },
		        tooltip: {
		            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		            pointFormat: '<tr><td style="color:{series.color};padding:0;font-size:12px">{series.name}: </td>' +
		                '<td style="padding:0"><b>{point.y:1f}</b></td></tr>',
		            footerFormat: '</table>',
		            useHTML: true
		        },
		        plotOptions: {
		            column: {
		                pointPadding: 0.2,
		                borderWidth: 0,
		                dataLabels: {
		                    enabled: true,
		                    color: 'black'
		                }
		            }
		        },
		        series: data
			 });
		})
		.error(function(data, status, headers, config){
			throw new Error('Algo deu errado.');
		});
		    
		$http({
			method: 'POST',
			url: 'precoxeventos.php',
			headers:{'Content-type': 'application/x-www-form-urlencoded'}
		})
		.success(function(result){
			var data = result;
			
			var chart = new Highcharts.Chart({
		        chart: {
		            renderTo: 'precoxeventos',
		            type: 'columnrange',
		            inverted: true
		            
		        },
		
		        title: {
		            text: 'Preços x Eventos'
		        },
		
		        xAxis: {
		            categories: ['Eventos']
		        },
		
		        yAxis: {
		            title: {
		                text: 'Preços'
		            }
		        },
		
		        tooltip: {
		            valuePrefix: 'US$'
		        },
		
		        plotOptions: {
		            columnrange: {
		                dataLabels: {
		                    enabled: true,
		                    formatter: function () {
		                        return 'US$ ' + this.y;
		                    }
		                }
		            }
		        },
		
		        legend: {
		            enabled: true
		        },
		
		        series: data
			 });
		})
		.error(function(data, status, headers, config){
			throw new Error('Algo deu errado.');
		});
		
		$http({
			method: 'POST',
			url: 'taxservice.php',
			headers:{'Content-type': 'application/x-www-form-urlencoded'}
		})
		.success(function(result){
			var data = result;
			
			var chart = new Highcharts.Chart({
		        chart: {
		            renderTo: 'taxservice',
		            type: 'column',
		            margin: 75,
		            options3d: {
		                enabled: true,
		                alpha: 15,
		                beta: 15,
		                depth: 50,
		                viewDistance: 25
		            }
		        },
		        title: {
		            text: 'Taxas de Serviço x Eventos'
		        },
		        subtitle: {
		            text: ''
		        },
		        xAxis: {
		            categories: [
		                'Eventos'
		            ]
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'Taxa de Serviço'
		            }
		        },
		        tooltip: {
		        	valueSuffix: '%'
		        },
		        plotOptions: {
		            column: {
		                depth: 25,
		                dataLabels: {
		                    enabled: true,
		                    color: 'black',
		                    formatter: function () {
		                        return this.y + '%';
		                    }
		                }
		            }
		        },
		        series: data
		    });
		})
		.error(function(data, status, headers, config){
			throw new Error('Algo deu errado.');
		});
		
		
		$http({
			method: 'POST',
			url: 'tipologin.php',
			headers:{'Content-type': 'application/x-www-form-urlencoded'}
		})
		.success(function(result){
			var data = result;
			
			$('#tipologin').highcharts({
		        chart: {
		        	type: 'pie',
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        title: {
		            text: 'Vendas por tipo login'
		        },
		        tooltip: {
		            pointFormat: '<b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    format: '<b>{point.name}</b>: {point.percentage:.1f}%',
		                    style: {
		                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                    },
		                    connectorColor: 'silver'
		                }
		            }
		        },
		        series: [{
		            data: data
		        }]
		    });
		})
		.error(function(data, status, headers, config){
			throw new Error('Algo deu errado.');
		});
	}]);
	
	app.controller('GraphIndivualCtrl',['$scope', '$http', '$rootScope', '$location', '$log', '$routeParams', '$interval', function($scope, $http, $rootScope, $location, $log, $routeParams, $interval) {
		
		$scope.name = '';
		$scope.date = '';
		$scope.initial_time = '';
		$scope.final_time = '';
		$scope.city = '';
		$scope.tax_service = '';
		$scope.venda = '';
		$scope.receita = '';
		$scope.produto = '';
		$scope.preco_min = '';
		$scope.preco_max = '';
		$scope.concession = '';
		$scope.nivel = 0;
		$scope.setor = 0;
		
		$scope.getPainel = function(){
		
			var thisData = 'id=' + $routeParams.id;
			$http({
				method: 'POST',
				url: 'dados.php',
				data: thisData,
				headers:{'Content-type': 'application/x-www-form-urlencoded'}
			})
			.success(function(data){
				if(data.error){
					$log.info(data);
				} else{
				
					$scope.name = data.response.name;
					$scope.date = data.response.date;
					$scope.initial_time = data.response.initial_time;
					$scope.final_time = data.response.final_time;
					$scope.city = data.response.city;
					$scope.tax_service = data.response.tax_service * 100;
					$scope.venda = data.response.venda;
					$scope.receita = data.response.receita;
					$scope.produto = data.response.produto;
					$scope.preco_min = data.response.preco_min;
					$scope.preco_max = data.response.preco_max;
					$scope.concession = data.response.concession;
					$scope.nivel = data.response.nivel;
					$scope.setor = data.response.setor;
					
					$('#qtdxevento').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: 'Tipo do Evento'
				        },
				        xAxis: {
				            categories: [
	                     		$scope.name + ' - ' + data.response.date
				            ]
				        },
				        yAxis: {
				            min: 0,
				            title: {
				                text: 'Quantidade'
				            }
				        },
				        tooltip: {
				            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				                '<td style="padding:0"><b>{point.y:.f}</b></td></tr>',
				            footerFormat: '</table>',
				            shared: true,
				            useHTML: true
				        },
				        plotOptions: {
				            column: {
				                pointPadding: 0.2,
				                borderWidth: 0,
				                dataLabels: {
				                    enabled: true,
				                    color: 'black'
				                }
				            }
				        },
				        series: [{
				            name: 'Produtos',
				            data: [Number($scope.produto)]
	
				        }, {
				            name: 'Concessões',
				            data: [Number($scope.concession)]
	
				        }, {
				            name: 'Níveis',
				            data: [Number($scope.nivel)]
	
				        }, {
				            name: 'Setores',
				            data: [Number($scope.setor)]
	
				        }]
				    });
					
					if(($scope.nivel > 0 || $scope.setor > 0 || $scope.produto > 0 || $scope.concession > 0) && $scope.venda > 0){
					
						var thisData = 'id=' + $routeParams.id + '&nivel=' + $scope.nivel + '&setor=' + $scope.setor + 
						'&produto=' + $scope.produto + '&concession=' + $scope.concession + '&venda=' + $scope.venda +
						'&initial_time=' + $scope.initial_time + '&final_time=' + $scope.final_time + '&tax_service=' + $scope.tax_service;
						
						$http({
							method: 'POST',
							url: 'qtds.php',
							data: thisData,
							headers:{'Content-type': 'application/x-www-form-urlencoded'}
						})
						.success(function(data){
							if(data.error){
								$log.info(data);
							} else{
								var vendaxnivel = data.vendaxnivel;
								var receitaxnivel = data.receitaxnivel;
								var vendaxsetor = data.vendaxsetor;
								var receitaxsetor = data.receitaxsetor;
								var vendaxconcessions = data.vendaxconcessions;
								var receitaxconcessions = data.receitaxconcessions;
								var produtos = data.produtos;
								var vendas = data.vendas;
								var vendaxlogin = data.vendaxlogin;
								var receitaxlogin = data.receitaxlogin;
								$scope.delivery = data.deliverymen;
								var deliverymen = data.deliverymen;
								var inventory_name = data.inventory_name;
								var inventory_qtd_sale = data.inventory_qtd_sale;
								var inventory_qtd_current = data.inventory_qtd_current;
								
								if(vendas){
									var categories = [];
									var qtd = [];
									
									for(var i = 0; i < data.periodo.length; i++) {
										categories.push(data.periodo[i].legenda);
									    qtd.push(data.periodo[i].qtd);
									}
									
									$('#intervaloxpedidos').highcharts({
								        chart: {
								            type: 'column'
								        },
								        title: {
								            text: 'Intervalo de Tempo x Pedidos'
								        },
								        xAxis: {
								        	categories: categories,
								        	labels: {
								                rotation: -45
								        	}
								        },
								        yAxis: {
								            min: 0,
								            title: {
								                text: 'Quantidade de Pedidos'
								            }
								        },
								        tooltip: {
								            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								                '<td style="padding:0"><b>{point.y:.f}</b></td></tr>',
								            footerFormat: '</table>',
								            useHTML: true
								        },
								        plotOptions: {
								            column: {
								                pointPadding: 0.2,
								                borderWidth: 0,
								                dataLabels: {
								                    enabled: true,
								                    color: 'black'
								                }
								            }
								        },
								        series: [{
								            name: 'Quantidade de Pedidos',
								            data: qtd
								        }]
								    });
								}
								
								if(inventory_qtd_current){
									var categories = [];
									var qtd_sale = [];
									var qtd = [];
									
									for(var i = 0; i < data.inventory_name.length; i++) {
									    categories.push(data.inventory_name[i].name);
									    qtd_sale.push(data.inventory_qtd_sale[i].qtd_sale);
									    qtd.push(data.inventory_qtd_current[i].qtd_inventory);
									}
									
									$('#inventoryxproduct').highcharts({
								        chart: {
								            type: 'column'
								        },
								        title: {
								            text: 'Estoque'
								        },
								        xAxis: {
								        	categories: categories
								        },
								        yAxis: {
								            min: 0,
								            title: {
								                text: 'Quantidade'
								            }
								        },
								        tooltip: {
								            formatter: function () {
								                return '<b>' + this.x + '</b><br/>' +
								                    this.series.name + ': ' + this.y + '<br/>' +
								                    'Total: ' + this.point.stackTotal;
								            }
								        },
								        plotOptions: {
								            column: {
								            	stacking: 'normal',
								            	dataLabels: {
								                    enabled: true,
								                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
								                    style: {
								                        textShadow: '0 0 3px black, 0 0 3px black'
								                    }
								                }
								            }
								        },
								        series: [{
								            name: 'Vendidos',
								            data: qtd_sale
								        }, {
								            name: 'Estoque',
								            data: qtd
								        }]
								    });
								}
								
								if(vendaxnivel){
								
									$('#vendaxnivel').highcharts({
								        chart: {
								        	type: 'pie',
								            plotBackgroundColor: null,
								            plotBorderWidth: null,
								            plotShadow: false
								        },
								        title: {
								            text: 'Vendas por nível'
								        },
								        tooltip: {
								            pointFormat: '<b>{point.percentage:.1f}%</b>'
								        },
								        plotOptions: {
								            pie: {
								                allowPointSelect: true,
								                cursor: 'pointer',
								                dataLabels: {
								                    enabled: true,
								                    format: '<b>{point.name}</b>: {point.percentage:.1f}%',
								                    style: {
								                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
								                    },
								                    connectorColor: 'silver'
								                }
								            }
								        },
								        series: [{
								            data: vendaxnivel
								        }]
								    });
								}
								
								if(receitaxnivel){
									$('#receitaxnivel').highcharts({
								        chart: {
								            type: 'bar'
								        },
								        title: {
								        	text: 'Receita por nível'
								        },
								        xAxis: {
								            categories: [
								                'Nível'
								            ]
								        },
								        yAxis: {
								            min: 0,
								            title: {
								                text: 'Receita'
								            }
								        },
								        tooltip: {
								            valuePrefix: 'US$'
								        },
								
								        plotOptions: {
								            bar: {
								                dataLabels: {
								                    enabled: true,
								                    formatter: function () {
								                        return 'US$ ' + this.y;
								                    }
								                }
								            }
								        },
								        series: receitaxnivel
								    });
								}
								
								if(vendaxsetor){
								    
									$('#vendaxsetor').highcharts({
								        chart: {
								        	type: 'pie',
								            plotBackgroundColor: null,
								            plotBorderWidth: null,
								            plotShadow: false
								        },
								        title: {
								            text: 'Vendas por setor'
								        },
								        tooltip: {
								            pointFormat: '<b>{point.percentage:.1f}%</b>'
								        },
								        plotOptions: {
								            pie: {
								                allowPointSelect: true,
								                cursor: 'pointer',
								                dataLabels: {
								                    enabled: true,
								                    format: '<b>{point.name}</b>: {point.percentage:.1f}%',
								                    style: {
								                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
								                    },
								                    connectorColor: 'silver'
								                }
								            }
								        },
								        series: [{
								            data: vendaxsetor
								        }]
								    });
								}
								
								if(receitaxsetor){
									$('#receitaxsetor').highcharts({
								        chart: {
								            type: 'bar'
								        },
								        title: {
								        	text: 'Receita por setor'
								        },
								        xAxis: {
								            categories: [
								                'Setor'
								            ]
								        },
								        yAxis: {
								            min: 0,
								            title: {
								                text: 'Receita'
								            }
								        },
								        tooltip: {
								            valuePrefix: 'US$'
								        },
								
								        plotOptions: {
								            bar: {
								                dataLabels: {
								                    enabled: true,
								                    formatter: function () {
								                        return 'US$ ' + this.y;
								                    }
								                }
								            }
								        },
								        series: receitaxsetor
								    });
								}
							
								if(vendaxconcessions){
									$('#vendaxconcessions').highcharts({
								        chart: {
								        	type: 'pie',
								            plotBackgroundColor: null,
								            plotBorderWidth: null,
								            plotShadow: false
								        },
								        title: {
								            text: 'Vendas por concessão'
								        },
								        tooltip: {
								            pointFormat: '<b>{point.percentage:.1f}%</b>'
								        },
								        plotOptions: {
								            pie: {
								                allowPointSelect: true,
								                cursor: 'pointer',
								                dataLabels: {
								                    enabled: true,
								                    format: '<b>{point.name}</b>: {point.percentage:.1f}%',
								                    style: {
								                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
								                    },
								                    connectorColor: 'silver'
								                }
								            }
								        },
								        series: [{
								            data: vendaxconcessions
								        }]
								    });
								}
							
								if(receitaxconcessions){
									$('#receitaxconcessions').highcharts({
								        chart: {
								            type: 'bar'
								        },
								        title: {
								        	text: 'Receita por concessão'
								        },
								        xAxis: {
								            categories: [
								                'Concessão'
								            ]
								        },
								        yAxis: {
								            min: 0,
								            title: {
								                text: 'Receita'
								            }
								        },
								        tooltip: {
								            valuePrefix: 'US$'
								        },
								
								        plotOptions: {
								            bar: {
								                dataLabels: {
								                    enabled: true,
								                    formatter: function () {
								                        return 'US$ ' + this.y;
								                    }
								                }
								            }
								        },
								        series: receitaxconcessions
								    });
								}
							
								if(produtos){
									$('#vendaxproduto').highcharts({
								        chart: {
								        	type: 'pie',
								            plotBackgroundColor: null,
								            plotBorderWidth: null,
								            plotShadow: false
								        },
								        title: {
								            text: 'Vendas por produto'
								        },
								        tooltip: {
								            pointFormat: '<b>{point.percentage:.1f}%</b>'
								        },
								        plotOptions: {
								            pie: {
								                allowPointSelect: true,
								                cursor: 'pointer',
								                dataLabels: {
								                    enabled: true,
								                    format: '<b>{point.name}</b>: {point.percentage:.1f}%',
								                    style: {
								                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
								                    },
								                    connectorColor: 'silver'
								                }
								            }
								        },
								        series: [{
								            data: produtos
								        }]
								    });
								}
							
								if(vendas){
									$('#receitaxproduto').highcharts({
								        chart: {
								            type: 'bar'
								        },
								        title: {
								        	text: 'Receita por produto'
								        },
								        subtitle: {
								            text: 'Obs: Sem os valores de eventuais descontos e gorjetas'
								        },
								        xAxis: {
								            categories: [
								                'Produtos'
								            ]
								        },
								        yAxis: {
								            min: 0,
								            title: {
								                text: 'Receita'
								            }
								        },
								        tooltip: {
								            valuePrefix: 'US$'
								        },
								
								        plotOptions: {
								            bar: {
								                dataLabels: {
								                    enabled: true,
								                    formatter: function () {
								                        return 'US$ ' + this.y;
								                    }
								                }
								            }
								        },
								        series: vendas
								    });
								}
							
								if(vendaxlogin){
									$('#vendaxlogin').highcharts({
								        chart: {
								        	type: 'pie',
								            plotBackgroundColor: null,
								            plotBorderWidth: null,
								            plotShadow: false
								        },
								        title: {
								            text: 'Vendas por tipo login'
								        },
								        tooltip: {
								            pointFormat: '<b>{point.percentage:.1f}%</b>'
								        },
								        plotOptions: {
								            pie: {
								                allowPointSelect: true,
								                cursor: 'pointer',
								                dataLabels: {
								                    enabled: true,
								                    format: '<b>{point.name}</b>: {point.percentage:.1f}%',
								                    style: {
								                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
								                    },
								                    connectorColor: 'silver'
								                }
								            }
								        },
								        series: [{
								            data: vendaxlogin
								        }]
								    });
								}
							
								if(receitaxlogin){
									$('#receitaxlogin').highcharts({
								        chart: {
								            type: 'bar'
								        },
								        title: {
								        	text: 'Receita por tipo login'
								        },
								        xAxis: {
								            categories: [
								                'Login'
								            ]
								        },
								        yAxis: {
								            min: 0,
								            title: {
								                text: 'Receita'
								            }
								        },
								        tooltip: {
								            valuePrefix: 'US$'
								        },
								
								        plotOptions: {
								            bar: {
								                dataLabels: {
								                    enabled: true,
								                    formatter: function () {
								                        return 'US$ ' + this.y;
								                    }
								                }
								            }
								        },
								        series: receitaxlogin
								    });
								}
								
								if(deliverymen){
									$('#deliverymen').highcharts({
								        chart: {
								            type: 'column'
								        },
								        title: {
								            text: 'Eficiência Entregador'
								        },
								        xAxis: {
								            categories: [
					                     		'Delivery'
								            ]
								        },
								        yAxis: {
								            min: 0,
								            title: {
								                text: 'Quantidade'
								            }
								        },
								        tooltip: {
								            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
								            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								                '<td style="padding:0"><b>{point.y:.f}</b></td></tr>',
								            footerFormat: '</table>',
								            shared: true,
								            useHTML: true
								        },
								        plotOptions: {
								            column: {
								                pointPadding: 0.2,
								                borderWidth: 0,
								                dataLabels: {
								                    enabled: true,
								                    color: 'black'
								                }
								            }
								        },
								        series: deliverymen
								    });
								}
							}
						})
						.error(function(data, status, headers, config){
							throw new Error('Algo deu errado.');
						});
					}

				}
			})
			.error(function(data, status, headers, config){
				throw new Error('Algo deu errado.');
			});
		}
			
		var atualizaPainel = function(){
			$scope.getPainel();
		}
		
		$scope.getPainel();
		$interval(atualizaPainel, 90000);
	}]);
})();