<?php

require_once('lib/functions.php');
if(!validRequest()) {
	http_response_code(401); 
	die;
}

/*
session_start();
$headerToken = $_SERVER['HTTP_ACCESS_TOKEN'];
$sessionToken = $_SESSION['XSRF'];

if(!isset($sessionToken) && $headerToken != $sessionToken){
	$resposta["error"] = false;
	$resposta["status"] = 3;
	$resposta["message"] = "Favor realizar novamente o login.";

	echo json_encode($resposta);
	die;
}
*/

$resposta = array();

try{
	
	include_once('conexao.php');
	
	$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
	$antiga_senha = filter_var($_POST['old_password'], FILTER_SANITIZE_MAGIC_QUOTES);
	$nova_senha = filter_var($_POST['new_password'], FILTER_SANITIZE_MAGIC_QUOTES);
	
	include('class/Customer.php');
	$sql = 'SELECT c.customer_email, c.customer_name, c.customer_type, c.customer_id, c.customer_password FROM customer c WHERE c.customer_email = ?';
	$query = $con->prepare($sql);
	$query->execute(array($email));
	$query->setFetchMode(PDO::FETCH_CLASS, 'Customer');
	$num_rows = $query->rowCount();
	$row = $query->fetch();
	
	if($num_rows > 0){
	
		if($row->getCustomerType() == 2){
				
			if(sha1($antiga_senha) == $row->getCustomerPassword()){
				$sql2 = 'UPDATE customer SET customer_password = ?, customer_update_password = ? WHERE customer_id = ?';
				$query2 = $con->prepare($sql2);
				$query2->execute(array(sha1($nova_senha), date('Y-m-d H:m:s'), $row->getCustomerId()));
				$num_rows2 = $query2->rowCount();
	
				if($num_rows2 > 0){
					$resposta["error"] = false;
					$resposta["status"] = 2;
					$resposta["message"] = "Senha alterada com sucesso.";
						
					try{
						include ('lib/utilities.php');
						$assunto = "Troca de senha - Snack4me";
						$message = "Prezado(a) <strong>". $row->getCustomerName() . "</strong>, a sua senha foi alterada na Snack4me.<br /><br />" ;
						$message .= "Obrigado<br /><a href='http://www.snack4me.com' target='_blank'><img src='http://www.snack4me.com/hom/images/logo_small.png' title='Snack4me'></a><br /><br />";
						$message .= "<span style='font-size:9px;color:#d5d5d5'>Favor não responder o email.</span><br />";
							
						$envia_email = enviarEmail($row->getCustomerName(), $email, $assunto, $message);
	
					} catch(PDOException $e){
						$envia_email = false;
					}
				} else{
					$resposta["error"] = false;
					$resposta["status"] = 1;
					$resposta["message"] = "Erro ao salvar nova senha.";
				}
			} else{
				$resposta["error"] = false;
				$resposta["status"] = 1;
				$resposta["message"] = "Senha antiga não confere.";
			}
				
		} else if($row->getCustomerType() == 3 || $row->getCustomerType() == 4){
			$resposta["error"] = false;
			$resposta["status"] = 1;
			$resposta["message"] = "Acesso à Snack4me por meio de rede sociais. Impossível alterar senha.";
		}
	} else{
		$resposta["error"] = false;
		$resposta["status"] = 1;
		$resposta["message"] = "Email não encontrado.";
	}

} catch(PDOException $e){
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);