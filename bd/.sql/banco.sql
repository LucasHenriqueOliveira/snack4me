CREATE DATABASE  IF NOT EXISTS `snack4me` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `snack4me`;
-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: snack4me
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cardtype`
--

DROP TABLE IF EXISTS `cardtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cardtype` (
  `cardtype_id` int(11) NOT NULL AUTO_INCREMENT,
  `cardtype_name` varchar(45) NOT NULL,
  PRIMARY KEY (`cardtype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `checkout`
--

DROP TABLE IF EXISTS `checkout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checkout` (
  `checkout_id` int(11) NOT NULL AUTO_INCREMENT,
  `checkout_customer_id` int(11) NOT NULL,
  `checkout_date` datetime NOT NULL,
  `checkout_chair` varchar(45) DEFAULT NULL,
  `checkout_table` varchar(45) DEFAULT NULL,
  `checkout_floor` varchar(45) DEFAULT NULL,
  `checkout_sector` varchar(45) DEFAULT NULL,
  `checkout_block` int(11) DEFAULT NULL,
  `checkout_price` decimal(10,2) NOT NULL,
  `checkout_price_discount` decimal(10,2) DEFAULT NULL,
  `checkout_tip` decimal(10,2) DEFAULT NULL,
  `checkout_price_total` decimal(10,2) NOT NULL,
  `checkout_coupon_id` int(11) DEFAULT NULL,
  `checkout_event_id` int(11) NOT NULL,
  `checkout_sin_order` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`checkout_id`)
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `checkout_item`
--

DROP TABLE IF EXISTS `checkout_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checkout_item` (
  `checkout_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `checkout_item_checkout_id` int(11) NOT NULL,
  `checkout_item_product_id` int(11) NOT NULL,
  `checkout_item_price_unit` decimal(10,2) NOT NULL,
  `checkout_item_quantity` int(11) NOT NULL,
  `checkout_item_price_total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`checkout_item_id`),
  KEY `fk_checkout_item_idx` (`checkout_item_checkout_id`),
  CONSTRAINT `fk_checkout_item` FOREIGN KEY (`checkout_item_checkout_id`) REFERENCES `checkout` (`checkout_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(100) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `concession`
--

DROP TABLE IF EXISTS `concession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `concession` (
  `concession_id` int(11) NOT NULL AUTO_INCREMENT,
  `concession_name` varchar(45) DEFAULT NULL,
  `concession_event_id` int(11) NOT NULL,
  `concession_active` tinyint(1) DEFAULT '1',
  `concession_image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`concession_id`),
  KEY `fk_concession_event_idx` (`concession_event_id`),
  CONSTRAINT `fk_concession_event` FOREIGN KEY (`concession_event_id`) REFERENCES `event` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coupon`
--

DROP TABLE IF EXISTS `coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_number` varchar(45) NOT NULL,
  `coupon_tax` float DEFAULT NULL,
  `coupon_sin_used` char(1) DEFAULT 'N',
  `coupon_event_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`coupon_id`),
  KEY `fk_coupon_event_idx` (`coupon_event_id`),
  CONSTRAINT `fk_coupon_event` FOREIGN KEY (`coupon_event_id`) REFERENCES `event` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(150) DEFAULT NULL,
  `customer_email` varchar(100) DEFAULT NULL,
  `customer_password` varchar(45) DEFAULT NULL,
  `customer_registration_date` datetime DEFAULT NULL,
  `customer_sin_valid` char(1) DEFAULT 'N',
  `customer_valid_date` datetime DEFAULT NULL,
  `customer_phone` varchar(20) DEFAULT NULL,
  `customer_cardtype_id` int(11) DEFAULT NULL,
  `customer_card_number` varchar(45) DEFAULT NULL,
  `customer_card_name` varchar(150) DEFAULT NULL,
  `customer_security_code` int(11) DEFAULT NULL,
  `customer_type` int(11) DEFAULT '1',
  `customer_update_password` datetime DEFAULT NULL,
  PRIMARY KEY (`customer_id`),
  KEY `fk_user_cardtype_idx` (`customer_cardtype_id`),
  CONSTRAINT `fk_customer_cardtype` FOREIGN KEY (`customer_cardtype_id`) REFERENCES `cardtype` (`cardtype_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `dados_evento`
--

DROP TABLE IF EXISTS `dados_evento`;
/*!50001 DROP VIEW IF EXISTS `dados_evento`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `dados_evento` (
  `event_id` tinyint NOT NULL,
  `event_name` tinyint NOT NULL,
  `date` tinyint NOT NULL,
  `initial_time` tinyint NOT NULL,
  `final_time` tinyint NOT NULL,
  `event_tax_service` tinyint NOT NULL,
  `city_name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(45) NOT NULL,
  `event_date` date NOT NULL,
  `event_initial_time` time DEFAULT NULL,
  `event_final_time` time DEFAULT NULL,
  `event_image` varchar(45) DEFAULT NULL,
  `event_city_id` int(11) NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `event_description` varchar(255) DEFAULT NULL,
  `event_tax_service` decimal(10,2) DEFAULT NULL,
  `event_sport_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`),
  KEY `fk_event_city_idx` (`event_city_id`),
  KEY `fk_event_sport_idx` (`event_sport_id`),
  CONSTRAINT `fk_event_city` FOREIGN KEY (`event_city_id`) REFERENCES `city` (`city_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_sport` FOREIGN KEY (`event_sport_id`) REFERENCES `sport` (`sport_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_block`
--

DROP TABLE IF EXISTS `event_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_block` (
  `event_block_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `event_block` varchar(50) NOT NULL,
  `event_sector_id` int(11) NOT NULL,
  PRIMARY KEY (`event_block_id`),
  KEY `fk_event_block_idx` (`event_id`),
  KEY `fk_event_block_sector_idx` (`event_sector_id`),
  CONSTRAINT `fk_event_block` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_block_sector` FOREIGN KEY (`event_sector_id`) REFERENCES `event_sector` (`event_sector_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_configuration`
--

DROP TABLE IF EXISTS `event_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_configuration` (
  `event_configuration_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `event_configuration_sector` tinyint(1) NOT NULL DEFAULT '0',
  `event_configuration_row` tinyint(1) NOT NULL DEFAULT '0',
  `event_configuration_chair` tinyint(1) NOT NULL DEFAULT '0',
  `event_configuration_table` tinyint(1) NOT NULL DEFAULT '0',
  `event_configuration_block` tinyint(1) NOT NULL DEFAULT '0',
  `event_configuration_floor` tinyint(1) NOT NULL DEFAULT '0',
  `event_configuration_room` tinyint(1) NOT NULL DEFAULT '0',
  `event_configuration_concession` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`event_configuration_id`),
  KEY `fk_event_configuration_1_idx` (`event_id`),
  CONSTRAINT `fk_event_configuration_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_floor`
--

DROP TABLE IF EXISTS `event_floor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_floor` (
  `event_floor_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `event_floor` varchar(50) NOT NULL,
  PRIMARY KEY (`event_floor_id`),
  KEY `fk_event_floor_event_idx` (`event_id`),
  CONSTRAINT `fk_event_floor_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_seat`
--

DROP TABLE IF EXISTS `event_seat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_seat` (
  `event_seat_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_seat_columns` int(11) NOT NULL,
  `event_seat_rows` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`event_seat_id`),
  KEY `fk_event_seat_idx` (`event_id`),
  CONSTRAINT `fk_event_seat` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_sector`
--

DROP TABLE IF EXISTS `event_sector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_sector` (
  `event_sector_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `event_sector` varchar(50) NOT NULL,
  PRIMARY KEY (`event_sector_id`),
  KEY `fk_event_section_event_idx` (`event_id`),
  CONSTRAINT `fk_event_sector` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_user`
--

DROP TABLE IF EXISTS `event_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_user` (
  `event_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_user_event_id` int(11) NOT NULL,
  `event_user_user_id` int(11) NOT NULL,
  `event_user_active` tinyint(1) NOT NULL DEFAULT '1',
  `event_user_user_id_activation` int(11) DEFAULT NULL,
  `event_user_dth_activation` datetime DEFAULT NULL,
  `event_user_user_id_deactivation` int(11) DEFAULT NULL,
  `event_user_dth_deactivation` datetime DEFAULT NULL,
  PRIMARY KEY (`event_user_id`),
  KEY `fk_event_user_event_idx` (`event_user_event_id`),
  KEY `fk_event_user_user_idx` (`event_user_user_id`),
  CONSTRAINT `fk_event_user_event` FOREIGN KEY (`event_user_event_id`) REFERENCES `event` (`event_id`),
  CONSTRAINT `fk_event_user_user` FOREIGN KEY (`event_user_user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_order_id` int(11) NOT NULL,
  `item_product_id` int(11) NOT NULL,
  `item_price_unit` decimal(10,2) NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `item_price_total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `fk_item_order_idx` (`item_order_id`),
  KEY `fk_item_product_idx` (`item_product_id`),
  CONSTRAINT `fk_item_order` FOREIGN KEY (`item_order_id`) REFERENCES `order` (`order_id`),
  CONSTRAINT `fk_item_product` FOREIGN KEY (`item_product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter` (
  `newsletter_id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletter_email` varchar(45) NOT NULL,
  `newsletter_active` tinyint(1) NOT NULL DEFAULT '1',
  `newsletter_dth_active` datetime NOT NULL,
  PRIMARY KEY (`newsletter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_customer_id` int(11) NOT NULL,
  `order_tracking_number` varchar(20) NOT NULL,
  `order_date` datetime NOT NULL,
  `order_chair` varchar(45) DEFAULT NULL,
  `order_table` varchar(45) DEFAULT NULL,
  `order_floor` int(11) DEFAULT NULL,
  `order_sector` int(11) DEFAULT NULL,
  `order_block` int(11) DEFAULT NULL,
  `order_price` decimal(10,2) NOT NULL,
  `order_price_discount` decimal(10,2) DEFAULT NULL,
  `order_tip` decimal(10,2) DEFAULT NULL,
  `order_tax_service` decimal(10,2) DEFAULT NULL,
  `order_price_total` decimal(10,2) NOT NULL,
  `order_schedule_date` time DEFAULT NULL,
  `order_delivery_date` datetime DEFAULT NULL,
  `order_coupon_id` int(11) DEFAULT NULL,
  `order_event_id` int(11) NOT NULL,
  `order_saletype_id` int(11) DEFAULT NULL,
  `order_status_id` int(11) DEFAULT NULL,
  `order_customer_email` varchar(150) DEFAULT NULL,
  `order_user_id_delivery` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `fk_order_user_idx` (`order_customer_id`),
  KEY `fk_order_coupon_idx` (`order_coupon_id`),
  KEY `fk_order_event_idx` (`order_event_id`),
  KEY `fk_order_saletype_idx` (`order_saletype_id`),
  KEY `fk_order_status_idx` (`order_status_id`),
  KEY `fk_order_user_idx1` (`order_user_id_delivery`),
  CONSTRAINT `fk_order_coupon` FOREIGN KEY (`order_coupon_id`) REFERENCES `coupon` (`coupon_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_customer` FOREIGN KEY (`order_customer_id`) REFERENCES `customer` (`customer_id`),
  CONSTRAINT `fk_order_event` FOREIGN KEY (`order_event_id`) REFERENCES `event` (`event_id`),
  CONSTRAINT `fk_order_saletype` FOREIGN KEY (`order_saletype_id`) REFERENCES `saletype` (`saletype_id`),
  CONSTRAINT `fk_order_status` FOREIGN KEY (`order_status_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_user` FOREIGN KEY (`order_user_id_delivery`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `precoxeventos`
--

DROP TABLE IF EXISTS `precoxeventos`;
/*!50001 DROP VIEW IF EXISTS `precoxeventos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `precoxeventos` (
  `event_id` tinyint NOT NULL,
  `price_min` tinyint NOT NULL,
  `price_max` tinyint NOT NULL,
  `name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `product_image` varchar(45) DEFAULT NULL,
  `product_short_desc` varchar(255) NOT NULL,
  `product_long_desc` text NOT NULL,
  `product_update_date` datetime NOT NULL,
  `product_category_id` int(11) DEFAULT NULL,
  `product_event_id` int(11) NOT NULL,
  `product_concession_id` int(11) DEFAULT NULL,
  `product_inventory_qtd` int(11) NOT NULL,
  `product_inventory_current` int(11) NOT NULL,
  `product_inventory_maximum` int(11) NOT NULL,
  `product_inventory_minimum` int(11) NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `fk_product_category_idx` (`product_category_id`),
  KEY `fk_product_event_idx` (`product_event_id`),
  KEY `fk_product_concession_idx` (`product_concession_id`),
  CONSTRAINT `fk_product_category` FOREIGN KEY (`product_category_id`) REFERENCES `category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_concession` FOREIGN KEY (`product_concession_id`) REFERENCES `concession` (`concession_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_event` FOREIGN KEY (`product_event_id`) REFERENCES `event` (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `produtoxconcession`
--

DROP TABLE IF EXISTS `produtoxconcession`;
/*!50001 DROP VIEW IF EXISTS `produtoxconcession`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `produtoxconcession` (
  `qtd` tinyint NOT NULL,
  `product_name` tinyint NOT NULL,
  `concession_name` tinyint NOT NULL,
  `order_event_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `produtoxeventos`
--

DROP TABLE IF EXISTS `produtoxeventos`;
/*!50001 DROP VIEW IF EXISTS `produtoxeventos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `produtoxeventos` (
  `event_id` tinyint NOT NULL,
  `data` tinyint NOT NULL,
  `name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_name` varchar(45) NOT NULL,
  `profile_active` tinyint(1) NOT NULL DEFAULT '1',
  `profile_dth_activation` datetime DEFAULT NULL,
  `profile_dth_deactivation` datetime DEFAULT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `qtdxnivel`
--

DROP TABLE IF EXISTS `qtdxnivel`;
/*!50001 DROP VIEW IF EXISTS `qtdxnivel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `qtdxnivel` (
  `order_event_id` tinyint NOT NULL,
  `qtd` tinyint NOT NULL,
  `order_floor` tinyint NOT NULL,
  `event_floor` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `qtdxproduto`
--

DROP TABLE IF EXISTS `qtdxproduto`;
/*!50001 DROP VIEW IF EXISTS `qtdxproduto`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `qtdxproduto` (
  `qtd` tinyint NOT NULL,
  `order_event_id` tinyint NOT NULL,
  `product_name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `qtdxsetor`
--

DROP TABLE IF EXISTS `qtdxsetor`;
/*!50001 DROP VIEW IF EXISTS `qtdxsetor`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `qtdxsetor` (
  `order_event_id` tinyint NOT NULL,
  `qtd` tinyint NOT NULL,
  `order_sector` tinyint NOT NULL,
  `event_sector` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `receitaxeventos`
--

DROP TABLE IF EXISTS `receitaxeventos`;
/*!50001 DROP VIEW IF EXISTS `receitaxeventos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `receitaxeventos` (
  `event_id` tinyint NOT NULL,
  `data` tinyint NOT NULL,
  `name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `receitaxnivel`
--

DROP TABLE IF EXISTS `receitaxnivel`;
/*!50001 DROP VIEW IF EXISTS `receitaxnivel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `receitaxnivel` (
  `order_event_id` tinyint NOT NULL,
  `qtd` tinyint NOT NULL,
  `order_floor` tinyint NOT NULL,
  `event_floor` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `receitaxsetor`
--

DROP TABLE IF EXISTS `receitaxsetor`;
/*!50001 DROP VIEW IF EXISTS `receitaxsetor`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `receitaxsetor` (
  `order_event_id` tinyint NOT NULL,
  `qtd` tinyint NOT NULL,
  `order_sector` tinyint NOT NULL,
  `event_sector` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `saletype`
--

DROP TABLE IF EXISTS `saletype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saletype` (
  `saletype_id` int(11) NOT NULL AUTO_INCREMENT,
  `saletype_name` varchar(45) NOT NULL,
  PRIMARY KEY (`saletype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sport`
--

DROP TABLE IF EXISTS `sport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sport` (
  `sport_id` int(11) NOT NULL AUTO_INCREMENT,
  `sport_name` varchar(45) NOT NULL,
  PRIMARY KEY (`sport_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(45) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) NOT NULL,
  `user_password` varchar(45) NOT NULL,
  `user_active` tinyint(1) NOT NULL DEFAULT '1',
  `user_profile_id` int(11) NOT NULL,
  `user_login_default` tinyint(1) NOT NULL DEFAULT '1',
  `user_id_activation` int(11) DEFAULT NULL,
  `user_dth_activation` datetime DEFAULT NULL,
  `user_id_deactivation` int(11) DEFAULT NULL,
  `user_dth_deactivation` datetime DEFAULT NULL,
  `user_dth_update` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_user_profile_idx` (`user_profile_id`),
  CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_profile_id`) REFERENCES `profile` (`profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `vendaxeventos`
--

DROP TABLE IF EXISTS `vendaxeventos`;
/*!50001 DROP VIEW IF EXISTS `vendaxeventos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vendaxeventos` (
  `event_id` tinyint NOT NULL,
  `data` tinyint NOT NULL,
  `name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `dados_evento`
--

/*!50001 DROP TABLE IF EXISTS `dados_evento`*/;
/*!50001 DROP VIEW IF EXISTS `dados_evento`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `dados_evento` AS select `e`.`event_id` AS `event_id`,`e`.`event_name` AS `event_name`,date_format(`e`.`event_date`,'%d/%m/%Y') AS `date`,date_format(`e`.`event_initial_time`,'%H:%i') AS `initial_time`,date_format(`e`.`event_final_time`,'%H:%i') AS `final_time`,`e`.`event_tax_service` AS `event_tax_service`,`c`.`city_name` AS `city_name` from (`event` `e` join `city` `c` on((`e`.`event_city_id` = `c`.`city_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `precoxeventos`
--

/*!50001 DROP TABLE IF EXISTS `precoxeventos`*/;
/*!50001 DROP VIEW IF EXISTS `precoxeventos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `precoxeventos` AS select `e`.`event_id` AS `event_id`,min(`p`.`product_price`) AS `price_min`,max(`p`.`product_price`) AS `price_max`,concat(`e`.`event_name`,' - ',convert(date_format(`e`.`event_date`,'%d/%m/%Y') using latin1)) AS `name` from (`product` `p` join `event` `e` on((`p`.`product_event_id` = `e`.`event_id`))) group by `p`.`product_event_id` order by `e`.`event_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `produtoxconcession`
--

/*!50001 DROP TABLE IF EXISTS `produtoxconcession`*/;
/*!50001 DROP VIEW IF EXISTS `produtoxconcession`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `produtoxconcession` AS select sum(`i`.`item_price_total`) AS `qtd`,`p`.`product_name` AS `product_name`,`c`.`concession_name` AS `concession_name`,`o`.`order_event_id` AS `order_event_id` from (((`product` `p` join `concession` `c` on((`p`.`product_concession_id` = `c`.`concession_id`))) join `item` `i` on((`i`.`item_product_id` = `p`.`product_id`))) join `order` `o` on((`i`.`item_order_id` = `o`.`order_id`))) group by `p`.`product_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `produtoxeventos`
--

/*!50001 DROP TABLE IF EXISTS `produtoxeventos`*/;
/*!50001 DROP VIEW IF EXISTS `produtoxeventos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `produtoxeventos` AS select `e`.`event_id` AS `event_id`,count(`p`.`product_event_id`) AS `data`,concat(`e`.`event_name`,' - ',convert(date_format(`e`.`event_date`,'%d/%m/%Y') using latin1)) AS `name` from (`product` `p` join `event` `e` on((`p`.`product_event_id` = `e`.`event_id`))) group by `p`.`product_event_id` order by `e`.`event_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `qtdxnivel`
--

/*!50001 DROP TABLE IF EXISTS `qtdxnivel`*/;
/*!50001 DROP VIEW IF EXISTS `qtdxnivel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `qtdxnivel` AS select `o`.`order_event_id` AS `order_event_id`,count(`o`.`order_floor`) AS `qtd`,`o`.`order_floor` AS `order_floor`,`e`.`event_floor` AS `event_floor` from (`order` `o` join `event_floor` `e` on((`o`.`order_floor` = `e`.`event_floor_id`))) group by `o`.`order_floor` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `qtdxproduto`
--

/*!50001 DROP TABLE IF EXISTS `qtdxproduto`*/;
/*!50001 DROP VIEW IF EXISTS `qtdxproduto`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `qtdxproduto` AS select sum(`i`.`item_quantity`) AS `qtd`,`o`.`order_event_id` AS `order_event_id`,`p`.`product_name` AS `product_name` from ((`order` `o` join `item` `i` on((`o`.`order_id` = `i`.`item_order_id`))) join `product` `p` on((`i`.`item_product_id` = `p`.`product_id`))) group by `i`.`item_product_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `qtdxsetor`
--

/*!50001 DROP TABLE IF EXISTS `qtdxsetor`*/;
/*!50001 DROP VIEW IF EXISTS `qtdxsetor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `qtdxsetor` AS select `o`.`order_event_id` AS `order_event_id`,count(`o`.`order_sector`) AS `qtd`,`o`.`order_sector` AS `order_sector`,`e`.`event_sector` AS `event_sector` from (`order` `o` join `event_sector` `e` on((`o`.`order_sector` = `e`.`event_sector_id`))) group by `o`.`order_sector` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `receitaxeventos`
--

/*!50001 DROP TABLE IF EXISTS `receitaxeventos`*/;
/*!50001 DROP VIEW IF EXISTS `receitaxeventos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `receitaxeventos` AS select `e`.`event_id` AS `event_id`,sum(`o`.`order_price_total`) AS `data`,concat(`e`.`event_name`,' - ',convert(date_format(`e`.`event_date`,'%d/%m/%Y') using latin1)) AS `name` from (`order` `o` join `event` `e` on((`o`.`order_event_id` = `e`.`event_id`))) group by `o`.`order_event_id` order by `e`.`event_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `receitaxnivel`
--

/*!50001 DROP TABLE IF EXISTS `receitaxnivel`*/;
/*!50001 DROP VIEW IF EXISTS `receitaxnivel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `receitaxnivel` AS select `o`.`order_event_id` AS `order_event_id`,sum(`o`.`order_price_total`) AS `qtd`,`o`.`order_floor` AS `order_floor`,`e`.`event_floor` AS `event_floor` from (`order` `o` join `event_floor` `e` on((`o`.`order_floor` = `e`.`event_floor_id`))) group by `o`.`order_floor` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `receitaxsetor`
--

/*!50001 DROP TABLE IF EXISTS `receitaxsetor`*/;
/*!50001 DROP VIEW IF EXISTS `receitaxsetor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `receitaxsetor` AS select `o`.`order_event_id` AS `order_event_id`,sum(`o`.`order_price_total`) AS `qtd`,`o`.`order_sector` AS `order_sector`,`e`.`event_sector` AS `event_sector` from (`order` `o` join `event_sector` `e` on((`o`.`order_sector` = `e`.`event_sector_id`))) group by `o`.`order_sector` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vendaxeventos`
--

/*!50001 DROP TABLE IF EXISTS `vendaxeventos`*/;
/*!50001 DROP VIEW IF EXISTS `vendaxeventos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vendaxeventos` AS select `e`.`event_id` AS `event_id`,count(`o`.`order_event_id`) AS `data`,concat(`e`.`event_name`,' - ',convert(date_format(`e`.`event_date`,'%d/%m/%Y') using latin1)) AS `name` from (`order` `o` join `event` `e` on((`o`.`order_event_id` = `e`.`event_id`))) group by `o`.`order_event_id` order by `e`.`event_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-03 14:04:39
