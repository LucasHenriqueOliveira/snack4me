<?php

$resposta = array();
$rows = array();

try{
	include_once('conexao.php');
	include('class/EventRow.php');
	
	$id_event = $_REQUEST['id_event'];
	$id_sector = $_REQUEST['id_sector'];
	
	if($id_sector > 0){
		$isSector = ' AND event_row_sector_id = '.$id_sector;
	}
	
	$query = $con->prepare('SELECT * FROM event_row WHERE event_id = ?' . $isSector);
	$query->execute(array($id_event));
	$query->setFetchMode(PDO::FETCH_CLASS, 'EventRow');
	
	while($row = $query->fetch()){
		$value_row['id_row'] = $row->getEventRowId();
		$value_row['name_row'] = $row->getEventRow();
		$rows[] = $value_row;
	}
	
	$resposta["rows"] = $rows;
	$resposta["error"] = false;	

} catch (Exception $e){
	
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);
