<?php

$resposta = array();
$products = array();

function separar_parametros($param) {
	return explode('&',$param);
}

function separar_valor($opcao) {
	return explode('=',$opcao);
}

try{
	include_once('conexao.php');
	include('class/Order.php');

	$ve = $_POST['ve'];
	$arr = array("#" => "/", "&" => "\\");
	
	$parametros = separar_parametros(gzinflate(base64_decode(strtr($ve,$arr))));
	foreach($parametros as $opcao) {
		$valor[] = separar_valor($opcao);
	}
	
	$order = $number = '';
	if(isset($valor[0][1])){
		$order = $valor[0][1];
	}
	
	if(isset($valor[1][1])){
		$number = $valor[1][1];
	}
	
	$query = $con->prepare('SELECT *,
			DATE_FORMAT(o.order_date, "%d/%m/%Y %H:%i") as order_date,
			DATE_FORMAT(e.event_date, "%d/%m/%Y") as event_date
			FROM `order` as o INNER JOIN `event` as e ON o.order_event_id = e.event_id
			INNER JOIN `status` as s ON o.order_status_id = s.status_id
			WHERE order_id = ? and order_tracking_number = ? LIMIT 1');
	$query->execute(array($order, $number));
	$query->setFetchMode(PDO::FETCH_CLASS, 'Order');
	$num_rows = $query->rowCount();
	
	if($num_rows > 0){
		$row = $query->fetch();
		$date_time = explode(" ", $row->getOrderDate());
		
		$pedido['num_order'] = $row->getOrderTrackingNumber();
		$pedido['date'] = $date_time[0];
		$pedido['hour'] = $date_time[1];
		$pedido['event_name'] = $row->getEventName();
		$pedido['event_date'] = $row->getEventDate();
		
		if($row->getOrderFloor() != ''){
			include('class/EventFloor.php');
		
			$query4 = $con->prepare('SELECT * FROM event_floor WHERE event_floor_id = ?');
			$query4->execute(array($row->getOrderFloor()));
			$query4->setFetchMode(PDO::FETCH_CLASS, 'EventFloor');
			$row4 = $query4->fetch();
				
			$pedido['floor'] = $row4->getEventFloor();
		} else{
			$pedido['floor'] = "";
		}
		
		if($row->getOrderSector() != ''){
			include('class/EventSector.php');
		
			$query3 = $con->prepare('SELECT * FROM event_sector WHERE event_sector_id = ?');
			$query3->execute(array($row->getOrderSector()));
			$query3->setFetchMode(PDO::FETCH_CLASS, 'EventSector');
			$row3 = $query3->fetch();
		
			$pedido['sector'] = $row3->getEventSector();
		} else{
			$pedido['sector'] = "";
		}
		
		if($row->getOrderChair() != ''){
			$pedido['seat'] = $row->getOrderChair();
		}
		
		include('class/Item.php');
			
		$query2 = $con->prepare('SELECT i.item_price_total,i.item_price_unit,i.item_quantity,
				p.product_name, p.product_short_desc
				FROM item as i INNER JOIN product as p ON i.item_product_id = p.product_id
				WHERE item_order_id = ?');
		$query2->execute(array($order));
		$query2->setFetchMode(PDO::FETCH_CLASS, 'Item');
			
		while ($row2 = $query2->fetch()){
			$product['name'] = $row2->getProductName();
			$product['desc'] = $row2->getProductShortDesc();
			$product['price_unit'] = $row2->getItemPriceUnit();
			$product['quantity'] = $row2->getItemQuantity();
			$product['price_total'] = $row2->getItemPriceTotal();
			$products[] = $product;
		}
		
		$pedido['products'] = $products;
		$pedido['price'] = $row->getOrderPrice();
		$pedido['discount'] = $row->getOrderPriceDiscount();
		$pedido['tip'] = $row->getOrderTip();
		$pedido['tax_service'] = $row->getOrderTaxService();
		$pedido['price_total'] = $row->getOrderPriceTotal();
		
		$resposta["error"] = false;
		$resposta["response"] = $pedido;
		
	} else{
		$resposta["error"] = false;
		$resposta["response"] = 'Pedido não encontrado.';
	}

} catch(PDOException $e){
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);