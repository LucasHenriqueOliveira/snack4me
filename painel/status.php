<?php 
$orders = array();
try{
	include_once('../class/Painel.php');
	
	$orders = explode(",", $_POST['id_order']);

	$consulta_status = new Painel();
	$resposta["error"] = false;
	$resposta["response"] = $consulta_status->consultaStatus($orders);
	
} catch (Exception $e){
	
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);