<?php 
$resposta = array();
try{
	include_once('../class/Painel.php');

	$pedidos_realizados = new Painel();
	$pedidos_realizados->setIdEvent($_POST['id_event']);
	$pedidos_realizados->setUltimoId($_POST['ultimo_id']);
	$resposta["error"] = false;
	$resposta["response"] = $pedidos_realizados->consultaPedidos();
	
} catch (Exception $e){
	
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);