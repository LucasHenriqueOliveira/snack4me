<?php 
	include('../conexao.php');
	$id_event = $_GET['id_event'];
	
	include_once('../class/Event.php');
	
	$query = $con->prepare('SELECT event_name, DATE_FORMAT(event_date, "%d/%m/%Y") as date
			FROM event WHERE event_id = ?');
	$query->execute(array($id_event));
	$query->setFetchMode(PDO::FETCH_CLASS, 'Event');
	$row = $query->fetch();
	$event = $row->getEventName() . ' (' . $row->getDate() . ')';
?>
<!DOCTYPE html>
<html ng-app = "Painel">
<head>
	<meta charset="utf-8" />

	<title>Snack4me</title>
	<meta name="description" content="">
	<meta name="author" content="snack4me">
	<meta name="viewport" content="width=device-width">
	<!-- Import CSS -->
	<link type="text/css" rel="stylesheet" href="css/pure-min.css" />
	<link type="text/css" rel="stylesheet" href="css/responsive-tabs.css" />	
	<link rel="stylesheet" href="css/print.css" media="print">
	<link type="text/css" rel="stylesheet" href="../css/font-awesome.min.css" />						
</head>
<body ng-controller = "Realizados" style="margin: 5px;">
	<section class="topo" style="width:100%; display: inline-block;">
		
		<div style="position:relative;float:left; margin-left: 15px;">
			<h3 class="topo"><?=$event?></h3>
		</div>
		<div style="position:relative;float:right">
			<img src="../images/logo.png" width="150px" class="topo">
		</div>
	</section>
	<section>
		<div class="row">
			<div id="horizontalTab">
		        <ul>
		            <li><a ng-click="atualizaStatus(1)">Pedidos realizados</a></li>
		            <li><a ng-click="atualizaStatus(5)">Em preparo</a></li>
		            <li><a ng-click="atualizaStatus(3)">Saiu para entrega</a></li>
		            <li><a ng-click="atualizaStatus(4)">Aguardando retirada</a></li>
		            <li><a ng-click="atualizaStatus(2)">Pedidos Entregues</a></li>
		        </ul>
		        
				<div class="tab">
					
					<div class="config">
						<label for="floor">Nível</label>
						<select name="floor" id="floor" class="pure-input-1-2"
							ng-model="selectedFloor" ng-change="atualizaFloor()" ng-options="floor for floor in floors" ng-show="floors.length > 0">
							<option value="">Todos</option>
						</select>
						
						<label for="sector">Setor</label>
						<select name="sector" id="sector" class="pure-input-1-2"
							ng-model="selectedSector" ng-change="atualizaSector()" ng-options="sector for sector in sectors" ng-show="sectors.length > 0">
							<option value="">Todos</option>
						</select>
					</div>
					
					<div class="atualizar">
						<button class="button-secondary pure-button" ng-click="getPedidos()"><i class="fa fa-refresh"></i>  Atualizar</button>
					</div>
					
					<br /><br />
					<table class="pure-table-striped" width="100%">
						<thead class="top">
							<tr>
						    	<th>Pedido #</th>
						    	<th>Localização</th>
						    	<th>Produtos</th>
						    	<th>Preço</th>
						    	<th class="center">Ações</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat = "pedido in pedidos | filter: filtroPainel">
								<td id="id_{{pedido.numero_pedido}}">{{pedido.numero_pedido}}</td>
								<td id="local_{{pedido.numero_pedido}}">
									<span ng-show="pedido.floor">{{pedido.floor}}<br /></span>
									<span ng-show="pedido.sector">{{pedido.sector}}<br /></span>
									Cadeira: {{pedido.chair}}</td>
								<td id="products_{{pedido.numero_pedido}}">
									<div ng-repeat = "produto in pedido.produtos">
										{{produto.name}}: {{produto.qtd}} unid.(s)
									</div>
								</td>
								<td id="total_{{pedido.numero_pedido}}">US$ {{pedido.total}}</td>
								<td class="acoes center">
									<button class="button-success pure-button">Imprimir</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
		    </div>
		</div>
		
	</section>
	
	<script src="../js/jquery.min.js"></script>
	<script src="js/jquery.responsiveTabs.js"></script>
	<script src="../js/angular.min.js"></script>

    <script>
        $(document).ready(function () {

        	$('#horizontalTab').responsiveTabs({
                rotate: false,
                startCollapsed: 'accordion',
                collapsible: 'accordion',
                setHash: true,
                //disabled: [3,4],
                activate: function(e, tab) {
                    $('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
                },
                activateState: function(e, state) {
                    //console.log(state);
                    $('.info').html('Switched from <strong>' + state.oldState + '</strong> state to <strong>' + state.newState + '</strong> state!');
                }
            });

            $('.select-tab').on('click', function() {
                $('#horizontalTab').responsiveTabs('activate', $(this).val());
            });

            $('.select-tab').on('click', function() {
                $('#horizontalTab').responsiveTabs('activate', $(this).val());
            });

        });
    </script>
    
    <script>
		var app = angular.module('Painel', []);

		app.controller('Realizados',[
		    '$scope',
		    '$http',
		    '$log',
		    '$interval',
		    function($scope, $http, $log, $interval){
				// propriedade
				$scope.pedidos = [];
				$scope.selectedFloor = '';
				$scope.floors = [];
				
				$scope.selectedSector = '';
				$scope.sectors = [];
				$scope.filtroPainel = { order_status_id: 1 };

				// função
				$scope.atualizaFloor = function() {
					
					if($scope.selectedFloor){
						$scope.filtroPainel.floor = $scope.selectedFloor;
					}else{
						delete $scope.filtroPainel.floor;
					}
							
					
				};

				$scope.atualizaSector = function() {
					
					if($scope.selectedSector){
						$scope.filtroPainel.sector = $scope.selectedSector;
					}else{
						delete $scope.filtroPainel.sector;
					}
							
					
				};

				$scope.atualizaStatus = function(status) {
					
					$scope.filtroPainel.order_status_id = status;
					
				};
				
			  	$scope.getPedidos = function(){
				  	
					var thisData = 'id_event=<?= $id_event?>';
					
					if($scope.pedidos.length){
						thisData += '&ultimo_id=' + $scope.pedidos[$scope.pedidos.length - 1].order_id;
				  	}
				  	
					$http({
						method: 'POST',
						url: 'realizados.php',
						data: thisData,
						headers:{'Content-type': 'application/x-www-form-urlencoded'}
					})
					.success(function(data){
						if(data.error){
							$log.info(data);
						} else{
							if(data.response){
								$scope.pedidos = $scope.pedidos.concat(data.response);

								for(var i = 0; i < $scope.pedidos.length; i++){
									existsSector = false;
									for(var j = 0; j < $scope.sectors.length; j++){
										if($scope.sectors[j] == $scope.pedidos[i].sector)
											existsSector = true;
									}
									if(!existsSector && $scope.pedidos[i].sector !== ''){
										$scope.sectors.push($scope.pedidos[i].sector);
									}

									existsFloor = false;
									for(var m = 0; m < $scope.floors.length; m++){
										if($scope.floors[m] == $scope.pedidos[i].floor)
											existsFloor = true;
									}
									
									if(!existsFloor && $scope.pedidos[i].floor !== ''){
										$scope.floors.push($scope.pedidos[i].floor);
									}
								}
								$scope.sectors.sort();
								$scope.floors.sort();
							}
						}
					})
					.error(function(data, status, headers, config){
						throw new Error('Algo deu errado.');
					});
				};

				$scope.getStatus = function(){

					var ids = [];
				  	for(var i = 0; i < $scope.pedidos.length; i++){
					  	ids.push($scope.pedidos[i].order_id);
				  	}
				  	
					var thisData = 'id_order='+ encodeURIComponent(ids);
				  	
					$http({
						method: 'POST',
						url: 'status.php',
						data: thisData,
						headers:{'Content-type': 'application/x-www-form-urlencoded'}
					})
					.success(function(data){
						if(data.error){
							$log.info(data);
						} else{
							if(data.response){
								for(var i = 0; i < $scope.pedidos.length; i++){
									for(var l = 0; l < data.response.length; l++){
										if($scope.pedidos[i].order_id == data.response[l].order_id){
											$scope.pedidos[i].order_status_id = data.response[l].order_status_id;
										}
									}
								}
							}
						}
					})
					.error(function(data, status, headers, config){
						throw new Error('Algo deu errado.');
					});
				};
				
				var atualizaPedidos = function(){
					$scope.getPedidos();
					$scope.getStatus();
				};

				$scope.newStatus = function(status){
					console.log(status);
				};

				$(document).on('click', 'td button', function() {

					switch($scope.filtroPainel.order_status_id) {
					    case 1:
					    	$scope.$msg = "Deseja imprimir a etiqueta?";
					        break;
					    case 2:
					    	$scope.$msg = "Deseja imprimir a etiqueta?";
					        break;
					    case 3:
					    	$scope.$msg = "Deseja confirmar a saída para entrega?";
					        break;
					    case 4:
					    	$scope.$msg = "Deseja confirmar a entrega do produto?";
					        break;
					    case 5:
					    	$scope.$msg = "Deseja confirmar a saída para entrega?";
					        break;
					}
					
					var question = confirm($scope.$msg);
	            	if(!question){
	                	return false;
	            	} else{
						if($scope.filtroPainel.order_status_id == 1){
							$(this).closest('tr').addClass('imprimir');
				           	print();
				           	$(this).closest('tr').removeClass('imprimir');
						}
		            }

		           	$scope.newStatus($scope.filtroPainel.order_status_id);
	            });
				
				//executa método
				$scope.getPedidos();
				$interval(atualizaPedidos, 15000);
			}]);
			
    </script> 
</body>
</html>