<?php 

$resposta = array();

try{
	include_once('conexao.php');
	
	$email = $_POST['email'];
	$datetime = date('Y-m-d H:i:s');
	
	$sql = "SELECT * FROM `newsletter` WHERE newsletter_email = '" . $email ."' AND newsletter_active = 1";
	$query = $con->prepare($sql);
	$query->execute();
	$num_rows = $query->rowCount();
	
	if($num_rows == 0){
		$sql2 = "INSERT INTO `newsletter` (`newsletter_email`, `newsletter_dth_active`)
		VALUES('" . $email . "', '" . $datetime . "')";
		
		$query2 = $con->prepare($sql2);
		$query2->execute();
		$num_rows2 = $query2->rowCount();
		
		if($num_rows2 > 0){
			$resposta["error"] = false;
			$resposta["status"] = 2;
		} else{
			$resposta["error"] = false;
			$resposta["status"] = 1;
			$resposta["message"] = "Não foi possível assinar a newsletter.";
		}
	} else{
		$resposta["error"] = false;
		$resposta["status"] = 1;
		$resposta["message"] = "Email já consta na assinatura da newsletter.";
	}

} catch(PDOException $e){
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}

echo json_encode($resposta);