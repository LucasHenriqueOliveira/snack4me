<?php

function enviarEmail($nome_destinatario, $destinatario, $assunto, $mensagem, $tipo = false, $titulo = '') {
	$root = realpath($_SERVER["DOCUMENT_ROOT"]);
	// Inclui o arquivo class.phpmailer.php localizado na pasta phpmailer
	
	require "$root/global/lib/phpmailer/class.smtp.php";
	require "$root/global/lib/phpmailer/class.phpmailer.php";
	
	// Inicia a classe PHPMailer
	$mail = new PHPMailer();
	
	// Define os dados do servidor e tipo de conexão
	// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	$mail->IsSMTP(); // Define que a mensagem será SMTP
	$mail->Host = "mail.snack4me.com"; // Endereço do servidor SMTP
	$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
	$mail->Username = 'noreply@snack4me.com'; // Usuário do servidor SMTP
	$mail->Password = 'Snack4me'; // Senha do servidor SMTP
	//$mail->Port = 25;
	
	// Define o remetente
	// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	$mail->From = "noreply@snack4me.com"; // Seu e-mail
	$mail->FromName = "Snack4me"; // Seu nome
	
	// Define os destinatário(s)
	// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	$mail->AddAddress($destinatario, $nome_destinatario);
	//$mail->AddAddress('ciclano@site.net');
	//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
	//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta
	
	// Define os dados técnicos da Mensagem
	// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
	$mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)
	
	// Define a mensagem (Texto e Assunto)
	// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	// $mail->Subject  = "Mensagem Teste"; // Assunto da mensagem
	$mail->Subject = $assunto; // Assunto da mensagem
	//$mail->Body = "Este é o corpo da mensagem de teste, em <b>HTML</b>! <br />";
	$mail->Body = $mensagem;
	//$mail->AltBody = "Este é o corpo da mensagem de teste, em Texto Plano! \r\n";
	
	// Define os anexos (opcional)
	// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	//$mail->AddAttachment("c:/temp/documento.pdf", "novo_nome.pdf");  // Insere um anexo
	if($tipo){
		$path = "./orders/" . $titulo;
		chmod($path, 0777);
	
		$mail->AddAttachment($path);  // Insere um anexo
	}
	
	// Envia o e-mail
	$enviado = $mail->Send();
	
	// Limpa os destinatários e os anexos
	$mail->ClearAllRecipients();
	$mail->ClearAttachments();
	
	// Exibe uma mensagem de resultado
	if ($enviado) {
		return true;
	} else {
		return false;
	}
}

function geraHTML($order_id) {
	include('./conexao.php');
	include('./class/Order.php');
	
	$query = $con->prepare('SELECT *,
			DATE_FORMAT(o.order_date, "%d/%m/%Y %H:%i") as order_date,
			DATE_FORMAT(e.event_date, "%d/%m/%Y") as event_date,
			DATE_FORMAT(o.order_schedule_date, "%H:%i") as order_schedule_date,
			o.order_tip, o.order_tax_service
			FROM `order` as o INNER JOIN `event` as e ON o.order_event_id = e.event_id
			WHERE order_id = ? LIMIT 1');
	$query->execute(array($order_id));
	$query->setFetchMode(PDO::FETCH_CLASS, 'Order');
	$row = $query->fetch();
	$date_time = explode(" ", $row->getOrderDate());
	
	$input = "o=" . $order_id . "&n=" . $row->getOrderTrackingNumber();
	$arr = array("/" => "#", "\\" => "&");
	$ve = rtrim(strtr(base64_encode(gzdeflate($input, 9)), $arr), '=');
	$url = "http://www.snack4me.com/global/read.php?ve=".$ve;
	
	$html = '<!doctype html>
				<html> 
				    <head>
				    </head> 
				    <body>
				    	<table boder="0" width="100%">
							<tr>
								<td width="50%">
									<h2 id="logo"><img src="images/logo.png" width="200px" height="100px" title="Snack4me" class="logo" /></h2>
								</td>
								<td width="50%" style="text-align:right">
									<img src="http://chart.apis.google.com/chart?chs=150x150&cht=qr&chld=L|0&chl='.$url.'" alt="QR code"/>
								</td>
							</tr>
						</table>
						<div style="text-align:left;">
							<h2>Pedido Nº '.$row->getOrderTrackingNumber(). '</h2>
							<ul style="list-style-type: none;">
								<li><strong>Data do pedido:</strong> '. $date_time[0] .'</li>
								<li><strong>Horário do pedido:</strong> '. $date_time[1] .'</li><br /><br />';
	
								$html .= '<li>'.$row->getEventName() . ' (' . $row->getEventDate() .')</li>';
								if($row->getOrderFloor() != ''){
									include('class/EventFloor.php');
								
									$query4 = $con->prepare('SELECT * FROM event_floor WHERE event_floor_id = ?');
									$query4->execute(array($row->getOrderFloor()));
									$query4->setFetchMode(PDO::FETCH_CLASS, 'EventFloor');
									$row4 = $query4->fetch();
								
									$html .= '<li><strong>Nível:</strong> '.$row4->getEventFloor() . '</li>';
								}
								if($row->getOrderSector() != ''){
									include('class/EventSector.php');
										
									$query3 = $con->prepare('SELECT * FROM event_sector WHERE event_sector_id = ?');
									$query3->execute(array($row->getOrderSector()));
									$query3->setFetchMode(PDO::FETCH_CLASS, 'EventSector');
									$row3 = $query3->fetch();
								
									$html .= '<li><strong>Setor:</strong> '.$row3->getEventSector() . '</li>';
								}
								if($row->getOrderChair() != ''){
									$html .= '<li><strong>Cadeira:</strong> '.$row->getOrderChair() . '</li>';
								}
					$html .= '</ul>
						</div>';
					$html .= '<div>';
					
						include('./class/Item.php');
						
						$query2 = $con->prepare('SELECT i.item_price_total,i.item_price_unit,i.item_quantity,
								p.product_name, p.product_short_desc
								FROM item as i INNER JOIN product as p ON i.item_product_id = p.product_id
								WHERE item_order_id = ?');
						$query2->execute(array($order_id));
						$query2->setFetchMode(PDO::FETCH_CLASS, 'Item');
						
						$html .= "<table width='100%' style='border:1px solid black;border-collapse: collapse;' border='1'>
						<tr>
						<th style='background: #f1f1f1;'>Produtos</th>
						<th style='background: #f1f1f1;'>Preço</th>
						<th style='background: #f1f1f1;'>Qtd</th>
						<th style='background: #f1f1f1;'>Subtotal</th>
						</tr>";
						while ($row2 = $query2->fetch()){
							$html .= '<tr><td><strong>' . $row2->getProductName() . '</strong><br />' . $row2->getProductShortDesc() . '</td>
							<td style="text-align:center;">' . $row2->getItemPriceUnit() . '</td>
							<td style="text-align:center;">' . $row2->getItemQuantity() . ' </td>
							<td style="text-align:center;">' . $row2->getItemPriceTotal() . '</td>
							</tr>';
						}
						$html .= "<tr><td colspan='3' style='text-align: right'>Subtotal (R$)</td>
						<td style='text-align:center;'>".$row->getOrderPrice()."</td></tr>";
						$html .= "<tr><td colspan='3' style='text-align: right'>Desconto (R$)</td>
						<td style='text-align:center;'>".$row->getOrderPriceDiscount()."</td></tr>";
						$html .= "<tr><td colspan='3' style='text-align: right'>Taxa de Serviço (R$)</td>
						<td style='text-align:center;'>".$row->getOrderTaxService()."</td></tr>";
						$html .= "<tr><td colspan='3' style='text-align: right'>Gorjeta (R$)</td>
						<td style='text-align:center;'>". $row->getOrderTip() ."</td></tr>";
						$html .= "<tr><td colspan='3' style='text-align: right'>Total (R$)</td>
						<td style='text-align:center;'>".$row->getOrderPriceTotal()."</td></tr>";
						$html .= "</table>";
					$html .= '</div>';
					
					if($row->getOrderScheduleDate() != ''){
						$html .= '<p>Obs: O seu pedido foi agendado para ser entregue às ' . $row->getOrderScheduleDate() . '
						do dia ' . $row->getEventDate() . '.</p>';
					}
					$html .= '</body> 
				</html>';
					
	return $html;
	
}

function geraPDF($titulo, $html) {
	$root = realpath($_SERVER["DOCUMENT_ROOT"]);
	
	require_once "$root/global/lib/dompdf/dompdf_config.inc.php";
	
	$dompdf = new DOMPDF();
	$dompdf->set_paper("A4", "portrail"); // Altera o papel para modo paisagem.
	$dompdf->load_html($html); // Carrega o HTML para a classe.
	$dompdf->render();
	$pdf = $dompdf->output(); // Cria o pdf
	$arquivo = "./orders/".$titulo; // Caminho onde será salvo o arquivo.

	if (file_put_contents($arquivo,$pdf)) { //Tenta salvar o pdf gerado
		return true; // Salvo com sucesso.
	} else {
		return false; // Erro ao salvar o arquivo
	}
}