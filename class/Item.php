<?php

class Item{
	private $item_id;
	private $item_order_id;
	private $item_product_number;
	private $item_price_unit;
	private $item_quantity;
	private $item_price_total;
	private $product_name;
	private $product_short_desc;
	private $product_concession_id;
	
	// ITEM ID
	public function getItemId() {
		return $this->item_id;
	}
	
	public function setItemId($item_id) {
		$this->item_id = $item_id;
		return $this;
	}
	
	// ITEM ORDER ID
	public function getItemOrderId() {
		return $this->item_order_id;
	}
	
	public function setItemOrderId($item_order_id) {
		$this->item_order_id = $item_order_id;
		return $this;
	}
	
	// ITEM PRODUCT ID
	public function getItemProductId() {
		return $this->item_product_id;
	}
	
	public function setItemProductId($item_product_id) {
		$this->item_product_id = $item_product_id;
		return $this;
	}
	
	// ITEM PRICE UNIT
	public function getItemPriceUnit() {
		return $this->item_price_unit;
	}
	
	public function setItemPriceUnit($item_price_unit) {
		$this->item_price_unit = $item_price_unit;
		return $this;
	}
	
	// ITEM QUANTITY
	public function getItemQuantity() {
		return $this->item_quantity;
	}
	
	public function setItemQuantity($item_quantity) {
		$this->item_quantity = $item_quantity;
		return $this;
	}
	
	// ITEM PRICE TOTAL
	public function getItemPriceTotal() {
		return $this->item_price_total;
	}
	
	public function setItemPriceTotal($item_price_total) {
		$this->item_price_total = $item_price_total;
		return $this;
	}
	
	// PRODUCT NAME
	public function getProductName() {
		return $this->product_name;
	}
	
	public function setProductName($product_name) {
		$this->product_name = $product_name;
		return $this;
	}
	
	// PRODUCT SHORT DESC
	public function getProductShortDesc() {
		return $this->product_short_desc;
	}
	
	public function setProductShortDesc($product_short_desc) {
		$this->product_short_desc = $product_short_desc;
		return $this;
	}
	
	// PRODUCT CONCESSION ID
	public function getProductConcessionId() {
		return $this->product_concession_id;
	}
	
	public function setProductConcessionId($product_concession_id) {
		$this->product_concession_id = $product_concession_id;
		return $this;
	}

}