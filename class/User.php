<?php

class User{
	private $user_id;
	private $user_name;
	private $user_profile_id;
	private $profile_name;
	private $user_password;
	
	// USER ID
	public function getUserId() {
		return $this->user_id;
	}
	
	public function setUserId($user_id) {
		$this->user_id = $user_id;
		return $this;
	}
	
	// USER ID
	public function getUserName() {
		return $this->user_name;
	}
	
	public function setUserName($user_name) {
		$this->user_name = $user_name;
		return $this;
	}
	
	// USER PROFILE ID
	public function getUserProfileId() {
		return $this->user_product_id;
	}
	
	public function setUserProfileId($user_profile_id) {
		$this->user_profile_id = $user_profile_id;
		return $this;
	}
	
	// PROFILE NAME
	public function getProfileName() {
		return $this->profile_name;
	}
	
	public function setProfileName($profile_name) {
		$this->profile_name = $profile_name;
		return $this;
	}
	
	// USER PASSWORD
	public function getUserPassword() {
		return $this->user_password;
	}
	
	public function setUserPassword($user_password) {
		$this->user_password = $user_password;
		return $this;
	}

}