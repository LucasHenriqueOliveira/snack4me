<?php

class Event{
	private $event_id;
	private $event_name;
	private $event_date;
	private $date;
	private $hour;
	private $event_initial_time;
	private $event_final_time;
	private $initial_time;
	private $final_time;
	private $event_image;
	private $event_description;
	private $event_tax_service;
	private $distance;
	private $event_sector_id;
	private $event_sector;
	private $event_floor_id;
	private $event_floor;
	private $event_city_id;
	private $city_name;
	private $sport_name;
	
	// ID
	public function getEventId() {
		return $this->event_id;
	}
	
	public function setEventId($event_id) {
		$this->event_id = $event_id;
		return $this;
	}
	
	// NAME
	public function getEventName() {
		return $this->event_name;
	}
	
	public function setEventName($event_name) {
		$this->event_name = $event_name;
		return $this;
	}
	
	// EVENT DATE
	public function getEventDate() {
		return $this->event_date;
	}
	
	public function setEventDate($event_date) {
		$this->event_date = $event_date;
		return $this;
	}
	
	// DATE
	public function getDate() {
		return $this->date;
	}
	
	public function setDate($date) {
		$this->date = $date;
		return $this;
	}
	
	// HOUR
	public function getHour() {
		return $this->hour;
	}
	
	public function setHour($hour) {
		$this->hour = $hour;
		return $this;
	}
	
	// INITIAL TIME
	public function getEventInitialTime() {
		return $this->event_initial_time;
	}
	
	public function setEventInitialTime($event_initial_time) {
		$this->event_initial_time = $event_initial_time;
		return $this;
	}
	
	// FINAL TIME
	public function getEventFinalTime() {
		return $this->event_final_time;
	}
	
	public function setEventFinalTime($event_final_time) {
		$this->event_final_time = $event_final_time;
		return $this;
	}
	
	// INITIAL TIME
	public function getInitialTime() {
		return $this->initial_time;
	}
	
	public function setInitialTime($initial_time) {
		$this->initial_time = $initial_time;
		return $this;
	}
	
	// FINAL TIME
	public function getFinalTime() {
		return $this->final_time;
	}
	
	public function setFinalTime($final_time) {
		$this->final_time = $final_time;
		return $this;
	}
	
	// IMAGE
	public function getEventImage() {
		return $this->event_image;
	}
	
	public function setEventImage($event_image) {
		$this->event_image = $event_image;
		return $this;
	}
	
	// DESCRIPTION
	public function getEventDescription() {
		return $this->event_description;
	}
	
	public function setEventDescription($event_description) {
		$this->event_description = $event_description;
		return $this;
	}
	
	// EVENT TAX SERVICE
	public function getEventTaxService() {
		return $this->event_tax_service;
	}
	
	public function setEventTaxService($event_tax_service) {
		$this->event_tax_service = $event_tax_service;
		return $this;
	}
	
	// DISTANCE
	public function getDistance() {
		return number_format($this->distance,1);
	}
	
	public function setDistance($distance) {
		$this->distance = $distance;
		return $this;
	}
	
	// Sector ID
	public function getEventSectorId() {
		return $this->event_sector_id;
	}
	
	public function setEventSectorId($event_sector_id) {
		$this->event_sector_id = $event_sector_id;
		return $this;
	}
	
	// Sector NAME
	public function getEventSector() {
		return $this->event_sector;
	}
	
	public function setEventSector($event_sector) {
		$this->event_sector = $event_sector;
		return $this;
	}
	
	// FLOOR ID
	public function getEventFloorId() {
		return $this->event_floor_id;
	}
	
	public function setEventFloorId($event_floor_id) {
		$this->event_floor_id = $event_floor_id;
		return $this;
	}
	
	// FLOOR NAME
	public function getEventFloor() {
		return $this->event_floor;
	}
	
	public function setEventFloor($event_floor) {
		$this->event_floor = $event_floor;
		return $this;
	}
	
	// CITY ID
	public function getEventCityId() {
		return $this->event_city_id;
	}
	
	public function setEventCityId($event_city_id) {
		$this->event_city_id = $event_city_id;
		return $this;
	}
	
	// CITY NAME
	public function getCityName() {
		return $this->city_name;
	}
	
	public function setCityName($city_name) {
		$this->city_name = $city_name;
		return $this;
	}
	
	// SPORT NAME
	public function getSportName() {
		return $this->sport_name;
	}
	
	public function setSportName($sport_name) {
		$this->sport_name = $sport_name;
		return $this;
	}
}