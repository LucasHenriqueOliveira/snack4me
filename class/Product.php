<?php

class Product{
	private $product_id;
	private $product_name;
	private $product_image;
	private $product_price;
	private $product_short_desc;
	private $product_long_desc;
	private $product_event_id;
	private $product_concession_id;
	private $product_inventory_qtd;
	private $product_inventory_current;
	private $product_inventory_maximum;
	private $product_inventory_minimum;
	
	// PRODUCT ID
	public function getProductId() {
		return $this->product_id;
	}
	
	public function setProductId($product_id) {
		$this->product_id = $product_id;
		return $this;
	}
	
	// PRODUCT NAME
	public function getProductName() {
		return $this->product_name;
	}
	
	public function setProductName($product_name) {
		$this->product_name = $product_name;
		return $this;
	}
	
	// PRODUCT IMAGE
	public function getProductImage() {
		return $this->product_image;
	}
	
	public function setProductImage($product_image) {
		$this->product_image = $product_image;
		return $this;
	}
	
	// PRODUCT PRICE
	public function getProductPrice() {
		return $this->product_price;
	}
	
	public function setProductPrice($product_price) {
		$this->product_price = $product_price;
		return $this;
	}
	
	// PRODUCT SHORT DESC
	public function getProductShortDesc() {
		return $this->product_short_desc;
	}
	
	public function setProductShortDesc($product_short_desc) {
		$this->product_short_desc = $product_short_desc;
		return $this;
	}
	
	// PRODUCT LONG DESC
	public function getProductLongDesc() {
		return $this->product_long_desc;
	}
	
	public function setProductLongDesc($product_long_desc) {
		$this->product_long_desc = $product_long_desc;
		return $this;
	}
	
	// PRODUCT EVENT ID
	public function getProductEventId() {
		return $this->product_event_id;
	}
	
	public function setProductEventId($product_event_id) {
		$this->product_event_id = $product_event_id;
		return $this;
	}
	
	// PRODUCT CONCESSION ID
	public function getProductConcessionId() {
		return $this->product_concession_id;
	}
	
	public function setProductConcessionId($product_concession_id) {
		$this->product_concession_id = $product_concession_id;
		return $this;
	}
	
	// PRODUCT INVENTORY QTD
	public function getProductInventoryQtd() {
		return $this->product_inventory_qtd;
	}
	
	public function setProductInventoryQtd($product_inventory_qtd) {
		$this->product_inventory_qtd = $product_inventory_qtd;
		return $this;
	}
	
	// PRODUCT INVENTORY CURRENT
	public function getProductInventoryCurrent() {
		return $this->product_inventory_current;
	}
	
	public function setProductInventoryCurrent($product_inventory_current) {
		$this->product_inventory_current = $product_inventory_current;
		return $this;
	}
	
	// PRODUCT INVENTORY MAXIMUM
	public function getProductInventoryMaximum() {
		return $this->product_inventory_maximum;
	}
	
	public function setProductInventoryMaximum($product_inventory_maximum) {
		$this->product_inventory_maximum = $product_inventory_maximum;
		return $this;
	}
	
	// PRODUCT INVENTORY MINIMUM
	public function getProductInventoryMinimum() {
		return $this->product_inventory_minimum;
	}
	
	public function setProductInventoryMinimum($product_inventory_minimum) {
		$this->product_inventory_minimum = $product_inventory_minimum;
		return $this;
	}

}