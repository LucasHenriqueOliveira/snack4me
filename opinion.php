<?php

require_once('lib/functions.php');
if(!validRequest()) {
	http_response_code(401); 
	die;
}

/*
session_start();
$headerToken = $_SERVER['HTTP_ACCESS_TOKEN'];
$sessionToken = $_SESSION['XSRF'];

if(!isset($sessionToken) && $headerToken != $sessionToken){
	$resposta["error"] = false;
	$resposta["status"] = 3;
	$resposta["message"] = "Favor realizar novamente o login.";

	echo json_encode($resposta);
	die;
}
*/

$resposta = array();

function separar_parametros($param) {
	return explode('&',$param);
}

function separar_valor($opcao) {
	return explode('=',$opcao);
}

try{
	include_once('conexao.php');
	include('class/Order.php');

	$ve = $_POST['ve'];
	$rating = $_POST['rating'];
	$arr = array("#" => "/", "&" => "\\");
	
	$parametros = separar_parametros(gzinflate(base64_decode(strtr($ve,$arr))));
	foreach($parametros as $opcao) {
		$valor[] = separar_valor($opcao);
	}
	
	$order = $number = '';
	if(isset($valor[0][1])){
		$order = $valor[0][1];
	}
	
	if(isset($valor[1][1])){
		$number = $valor[1][1];
	}
	
	$query = $con->prepare('UPDATE `order` 
					        SET `order_vote_buying` = ?
							WHERE `order_id` = ? AND `order_tracking_number` = ?');
	$query->execute(array($rating, $order, $number));
	$num_rows = $query->rowCount();
	
	if($num_rows > 0){
		$resposta["message"] = 'Obrigado por responder a nossa pesquisa de satisfação.';
		$resposta["status"] = 2;
		
	} else{
		$resposta["message"] = 'Infelizmente não foi possível registrar a sua resposta.';
		$resposta["status"] = 1;
	}
} catch(PDOException $e){
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);