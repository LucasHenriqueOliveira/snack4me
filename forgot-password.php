<?php 

$resposta = array();

try{
	$email = addslashes($_POST['email']);

	include_once('conexao.php');
	include('class/Customer.php');
	
	$query = $con->prepare('SELECT c.customer_email, c.customer_name, c.customer_type FROM customer c WHERE c.customer_email = ?');
	$query->execute(array($email));
	$query->setFetchMode(PDO::FETCH_CLASS, 'Customer');
	$num_rows = $query->rowCount();
	$row = $query->fetch();
	
	if($num_rows > 0){
	
		if($row->getCustomerType() == 1){
			
			$input = "a=1&e=" . $email . "&n=" . $row->getCustomerName();
			$ve = rtrim(strtr(base64_encode(gzdeflate($input, 9)), '+.', '-_'), '=');
			
			include ('lib/utilities.php');
			$assunto = "Nova senha Snack4me";
			$message = "Prezado(a) <strong>". $row->getCustomerName() . "</strong>, clique no link abaixo para informação de nova senha:<br /><br />" ;
			$message .= "<a href='http://www.snack4me.com/hom/verify_senha.php?ve=".$ve."' target='_blank'>http://www.snack4me.com/hom/verify_senha.php?ve=" . $ve . "</a> <br /><br />";
			$message .= "Obrigado<br /><a href='http://www.snack4me.com' target='_blank'><img src='http://www.snack4me.com/hom/images/logo_small.png' title='Snack4me'></a><br /><br />";
			$message .= "<span style='font-size:9px;color:#d5d5d5'>Favor não responder o email.</span><br />";
			
			$envia_email = enviarEmail($row->getCustomerName(), $email, $assunto, $message);
			
			$mensagem = 'Email enviado para ' . $email . ' com sucesso.';
			$resposta["status"] = 1;
		} else if($row->getCustomerType() == 2){
			$mensagem = 'Acesso anterior efetuado pelo Facebook.';
			$resposta["status"] = 2;
		} else if($row->getCustomerType() == 3){
			$mensagem = 'Acesso anterior efetuado pelo Twitter.';
			$resposta["status"] = 2;
		}
		
		$resposta["error"] = false;
		$resposta["message"] = $mensagem;
		
	
	} else{
		
		$resposta["error"] = true;
		$resposta["message"] = "Email não encontrado.";
	}

} catch (Exception $e){

	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);