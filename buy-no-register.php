<?php 

$resposta = array();

try{
	include_once('conexao.php');
	
	$num_products = filter_var($_POST['num_products'], FILTER_SANITIZE_NUMBER_INT);
	$prod_qtd = array();
	$itens = 0;
	
	$products = ' AND (';
	for($i = 0; $i < $num_products; $i++){
		if($_POST['qtd_product_'.$i] > 0){
			$itens++;
			$products .= 'product_id = '.$_POST['id_product_'.$i] . ' OR ';
			$prod_qtd[$_POST['id_product_'.$i]] = $_POST['qtd_product_'.$i];
		}
	}
	$products = substr($products,0,-3);
	$products .= ')';
	
	$id_event = filter_var($_POST['id_event'], FILTER_SANITIZE_NUMBER_INT);
	$seat = $_POST['seat'];
	$field_sector = '';
	$field_floor = '';
	$table_sector = '';
	$table_floor = '';
	$sector = '';
	$floor = '';
	$coupon = '';
	$coupon_number = '';
	$coupon_tax = '';
	$coupon_id = null;
	$num_rows3 = 0;
	$id_user = 1;
	$tip = filter_var($_POST['tip'], FILTER_SANITIZE_NUMBER_FLOAT);
	
	if(isset($_POST['sector']) && !empty($_POST['sector']) && $_POST['sector'] != 'null'){
		$sector = ' AND es.event_sector_id = ' . filter_var($_POST['sector'], FILTER_SANITIZE_NUMBER_INT);
		$field_sector = ", es.event_sector";
		$table_sector = ' INNER JOIN event_sector as es on e.event_id=es.event_id';
	}
	
	if(isset($_POST['floor']) && !empty($_POST['floor']) && $_POST['floor'] != 'null'){
		$floor = ' AND ef. event_floor_id = ' . filter_var($_POST['floor'], FILTER_SANITIZE_NUMBER_INT);
		$field_floor = ", ef.event_floor";
		$table_floor = ' INNER JOIN event_floor as ef ON e.event_id =  ef.event_id';
	}
	
	if(isset($_POST['coupon']) && !empty($_POST['coupon']) && $_POST['coupon'] != 'null'){
		$coupon = filter_var($_POST['coupon'], FILTER_SANITIZE_MAGIC_QUOTES);
	
		include('class/Coupon.php');
		$sql3 = 'SELECT c.coupon_number, c.coupon_tax, c.coupon_id, c.coupon_event_id FROM `coupon` as c WHERE c.coupon_number = ? AND coupon_sin_used="N"';
	
		$query3 = $con->prepare($sql3);
		$query3->execute(array($coupon));
		$num_rows3 = $query3->rowCount();
		$query3->setFetchMode(PDO::FETCH_CLASS, 'Coupon');
	
		if($num_rows3 > 0){
			$row3 = $query3->fetch();
			if($row3->getCouponEventId() == '' || $row3->getCouponEventId() == $id_event){
				$coupon_number = $row3->getCouponNumber();
				$coupon_tax = $row3->getCouponTax();
				$coupon_id = $row3->getCouponId();
			}
		}
	}
	
	if(isset($_POST['room']) && !empty($_POST['room']) && $_POST['room'] != 'null'){
		$room = $_POST['room'];
	}
	
	if(isset($_POST['table']) && !empty($_POST['table']) && $_POST['table'] != 'null'){
		$table = $_POST['table'];
	}
	
	include('class/Event.php');
	$sql2 = 'SELECT e.event_name, TIME_FORMAT(e.event_initial_time, "%H:%i") as event_initial_time, TIME_FORMAT(e.event_final_time, "%H:%i") as event_final_time, DATE_FORMAT(e.event_date, "%d/%m/%Y") as date'.$field_sector.$field_floor.' FROM `event` as e
	'.$table_sector.$table_floor.'
	WHERE e.event_id ='.$id_event.$sector.$floor;

	$query2 = $con->prepare($sql2);
	$query2->execute(array($id_event));
	$query2->setFetchMode(PDO::FETCH_CLASS, 'Event');
	$row2 = $query2->fetch();
	
	include('class/Product.php');
	$sql = 'SELECT `product_id`,`product_name`,`product_price` FROM product WHERE product_event_id = ?'. $products;
	$query = $con->prepare($sql);
	$query->execute(array($id_event));
	$num_rows = $query->rowCount();
	$query->setFetchMode(PDO::FETCH_CLASS, 'Product');
	
	if($num_rows > 0){
		$total = 0;
		$i = 0;
		$product[] = array();
	
		while ($row = $query->fetch()){
			$subtotal += $row->getProductPrice() * $prod_qtd[$row->getProductId()];
	
			$product[$i]['id'] = $row->getProductId();
			$product[$i]['price'] = $row->getProductPrice();
			$product[$i]['quantity'] = $prod_qtd[$row->getProductId()];
			$product[$i]['subtotal'] = number_format($row->getProductPrice() * $prod_qtd[$row->getProductId()],2);
			$i++;
		}
	
		if($num_rows3 > 0){
			$discount = number_format($subtotal * $coupon_tax,2);
			$total = number_format($subtotal - $discount,2);
		} else{
			$discount = '0.00';
			$total = number_format($subtotal,2);
		}
		$subtotal = number_format($subtotal, 2);
		$date = date('Y-m-d H:i:s');
		
		$con->beginTransaction();
	
		$sql = "INSERT INTO checkout (`checkout_customer_id`,`checkout_date`,`checkout_chair`,`checkout_table`,
		`checkout_floor`,`checkout_sector`,`checkout_price`,`checkout_price_discount`, `checkout_tip`,
		`checkout_price_total`,`checkout_coupon_id`,`checkout_event_id`)
		VALUES(" . $id_user . ", '" . $date . "', '" . $seat . "', '" . $table . "',
		'" . $_POST['floor'] . "', '" . $_POST['sector'] . "', " . $subtotal . ",
		" . $discount . ", " . $tip . ", " . $total . ", '" . $coupon_id . "', " . $id_event . ")";
	
		$query = $con->prepare($sql);
		$query->execute();
	
		$checkout_id = $con->lastInsertId();
	
		for($i = 0; $i < count($product); $i++){
	
			$sql = "INSERT INTO checkout_item (`checkout_item_checkout_id`, `checkout_item_product_id`,
			`checkout_item_price_unit`, `checkout_item_quantity`, `checkout_item_price_total`)
			VALUES(" . $checkout_id . ", " . $product[$i]['id'] . ",
			" . $product[$i]['price'] . ", " . $product[$i]['quantity'] . ", " . $product[$i]['subtotal'] . ")";
	
			$query = $con->prepare($sql);
			$query->execute();
		}
		
		$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
		$input = "e=" . $email . "&id=".$checkout_id;
		$arr = array("/" => "#", "\\" => "&");
		$ve = rtrim(strtr(base64_encode(gzdeflate($input, 9)), $arr), '=');
		
		include ('lib/utilities.php');
		$assunto = "Validação de compra sem cadastro - Snack4me";
		$mensagem = "Prezado(a), para realizar a compra sem cadastro na Snack4me,
		clique no link abaixo:<br /><br />" ;
		$mensagem .= "<a href='http://www.snack4me.com/global/#/anonymous/".$ve."' target='_blank'>http://www.snack4me.com/global/#/anonymous/" . $ve . "</a> <br /><br />";
		$mensagem .= "Obrigado<br /><a href='http://www.snack4me.com' target='_blank'><img src='http://www.snack4me.com/global/images/logo_small.png' title='Snack4me'></a><br /><br />";
		$mensagem .= "<span style='font-size:9px;color:#d5d5d5'>Favor não responder o email.</span><br />";
		
		$envia_email = enviarEmail('Cliente Snack4me', $email, $assunto, $mensagem);

		if($envia_email){
			$con->commit();

			$resposta["error"] = false;
			$resposta["message"] = "Email enviado com sucesso.";
			
		} else{
			$con->rollBack();
			
			$resposta["error"] = true;
			$resposta["message"] = "Ocorreu um erro no envio do email.";
			
		}
	} else{
		$resposta["error"] = true;
		$resposta["message"] = "Ocorreu um erro no envio do email.";
	}
	
} catch (Exception $e){
	$resposta["error"] = true;
	$resposta["message"] = "Ocorreu um erro no envio do email.";
	$resposta["console"] = $e->getMessage();
}
echo json_encode($resposta);