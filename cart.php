<?php 
date_default_timezone_set('America/Sao_Paulo');
$resposta = array();
$dados = array();
$produtos = array();

try{
	include_once('conexao.php');

	$num_products = $_POST['num_products'];

	if($num_products > 0){
		$prod_qtd = array();

		$products = ' AND (';
		for($i = 0; $i < $num_products; $i++){
			if($_POST['qtd_product_'.$i] > 0){
				$products .= 'product_id = '.$_POST['id_product_'.$i] . ' OR ';
				$prod_qtd[$_POST['id_product_'.$i]] = $_POST['qtd_product_'.$i];
			}
		}
		$products = substr($products,0,-3);
		$products .= ')';
	}

	$id_event = $_POST['id_event'];
	$seat = $_POST['seat'];
	$field_sector = '';
	$field_floor = '';
	$table_sector = '';
	$table_floor = '';
	$sector = '';
	$floor = '';
	$room = '';
	$table = '';

	if(isset($_POST['sector']) && !empty($_POST['sector']) && $_POST['sector'] != 'null'){
		$sector = ' AND es.event_sector_id = '.$_POST['sector'];
		$field_sector = ", es.event_sector";
		$table_sector = ' INNER JOIN event_sector as es on e.event_id=es.event_id';
	}

	if(isset($_POST['floor']) && !empty($_POST['floor']) && $_POST['floor'] != 'null'){
		$floor = ' AND ef. event_floor_id = '.$_POST['floor'];
		$field_floor = ", ef.event_floor";
		$table_floor = ' INNER JOIN event_floor as ef ON e.event_id =  ef.event_id';
	}

	if(isset($_POST['room']) && !empty($_POST['room']) && $_POST['room'] != 'null'){
		$room = $_POST['room'];
	}

	if(isset($_POST['table']) && !empty($_POST['table']) && $_POST['table'] != 'null'){
		$table = $_POST['table'];
	}

	include('class/Event.php');
	$sql2 = 'SELECT e.event_name, e.event_tax_service, e.event_final_time, DATE_FORMAT(e.event_date, "%d/%m/%Y") as event_date, DATE_FORMAT(e.event_date, "%d-%m-%Y") as date'.$field_sector.$field_floor.' FROM `event` as e
	'.$table_sector.$table_floor.'
	WHERE e.event_id =' . $id_event . ' AND e.event_date >= "'.date("Y-m-d").'"' . $sector . $floor;

	$query2 = $con->prepare($sql2);
	$query2->execute(array($id_event));
	$query2->setFetchMode(PDO::FETCH_CLASS, 'Event');
	$num_rows2 = $query2->rowCount();
	$row2 = $query2->fetch();

	if($num_rows2 > 0){
		
		$time_now = new DateTime(date('Y-m-d H:i:s'));
		$final_time = new DateTime($row2->getDate() . ' ' . $row2->getEventFinalTime());

		if($time_now > $final_time){
			$resposta["error"] = false;
			$resposta["status"] = 1;
			$resposta["message"] = 'Não existe nenhum produto em seu carrinho.';
			
			echo json_encode($resposta);
			die;
		}

		$dados['event_name'] = $row2->getEventName();
		$dados['event_date'] = $row2->getEventDate();
		$dados['date'] = $row2->getDate();
		$dados['sector'] = $row2->getEventSector();
		$dados['floor'] = $row2->getEventFloor();
		$dados['seat'] = $seat;

		if($num_products > 0){
			include('class/Product.php');
			$sql = 'SELECT `product_id`,`product_name`,`product_price` FROM product WHERE product_event_id = ?'. $products;
			$query = $con->prepare($sql);
			$query->execute(array($id_event));
			$num_rows = $query->rowCount();
			$query->setFetchMode(PDO::FETCH_CLASS, 'Product');
		
			if($num_rows > 0){
					
				$total = 0;
				$i = 0;
				$product[] = array();
					
				while ($row = $query->fetch()){
		
					$produto['id_produto'] = $row->getProductId();
					$produto['name'] = $row->getProductName();
					$produto['price'] = $row->getProductPrice();
					$produto['quantity'] = $prod_qtd[$row->getProductId()];
					$produto['subtotal_unit'] = number_format($row->getProductPrice() * $prod_qtd[$row->getProductId()],2);
		
					$produtos[] = $produto;
					$subtotal += $row->getProductPrice() * $prod_qtd[$row->getProductId()];
		
					$product[$i]['id'] = $row->getProductId();
					$product[$i]['price'] = $row->getProductPrice();
					$product[$i]['quantity'] = $prod_qtd[$row->getProductId()];
					$product[$i]['subtotal'] = number_format($row->getProductPrice() * $prod_qtd[$row->getProductId()],2);
					$i++;
				}
					
				$total = number_format($subtotal,2);
				$subtotal = number_format($subtotal, 2);
		
				$dados['products'] = $produtos;
				$dados['subtotal'] = $subtotal;
				$dados['tax_service'] = $row2->getEventTaxService();
				$dados['total'] = $total;
		
				$resposta["error"] = false;
				$resposta["status"] = 2;
				$resposta['response'] = $dados;
		
			} else {
				$dados['products'] = $produtos;
				$dados['tax_service'] = $row2->getEventTaxService();
				$dados['subtotal'] = "";
				$dados['total'] = "";
		
				$resposta["error"] = false;
				$resposta["status"] = 2;
				$resposta['response'] = $dados;
			}
		} else{
			$dados['products'] = $produtos;
			$dados['subtotal'] = "";
			$dados['tax_service'] = $row2->getEventTaxService();
			$dados['total'] = "";
		
			$resposta["error"] = false;
			$resposta["status"] = 2;
			$resposta['response'] = $dados;
		}
	} else{
		$resposta["error"] = false;
		$resposta["status"] = 1;
		$resposta["message"] = 'Não existe nenhum produto em seu carrinho.';
	}

} catch (Exception $e){

	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);