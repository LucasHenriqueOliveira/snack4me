<?php 
date_default_timezone_set('America/Sao_Paulo');

$resposta = array();
$eventos = array();
try{
	$latitude = $_REQUEST['lat'];
	$longitude = $_REQUEST['lon'];
	
	include_once('conexao.php');
	include('class/Event.php');

	$query = $con->query('SELECT *, DATE_FORMAT(event_date, "%m-%d-%Y") as date, DATE_FORMAT(event_date, "%H:%i") as hour, DATE_FORMAT(event_initial_time, "%H:%i") as initial_time, DATE_FORMAT(event_final_time, "%H:%i") as final_time,
			( 6371 * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) AS distance, sport_name
			FROM event INNER JOIN sport ON event.event_sport_id = sport.sport_id WHERE event_date >= "'.date("Y-m-d").'"
			ORDER BY event_date, distance
			LIMIT 0 , 6');
	$num_rows = $query->rowCount();
	$query->setFetchMode(PDO::FETCH_CLASS, 'Event'); 
	
	if($num_rows > 0){
		while ($row = $query->fetch()){
			
			$date_now = new DateTime(date('Y-m-d H:i:s'));
			$date = explode(" ", $row->getEventDate());
			$date_event = new DateTime($date[0] . ' ' .$row->getEventFinalTime());
			
			if($date_now < $date_event){
				$evento['id'] = $row->getEventId();
				$evento['name'] = $row->getEventName();
				$evento['date'] = $row->getDate();
				$evento['schedule'] = $row->getHour();
				$evento['initial_time'] = $row->getInitialTime();
				$evento['final_time'] = $row->getFinalTime();
				$evento['distance'] = $row->getDistance();
				$evento['image'] = $row->getEventImage();
				$evento['description'] = $row->getEventDescription();
				$evento['league'] = $row->getSportName();
				$eventos[] = $evento;
			}
		}
	}
	
	$resposta["error"] = false;
	$resposta["response"] = $eventos;
	
} catch (Exception $e){
	
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);