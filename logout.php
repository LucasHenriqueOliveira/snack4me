<?php

if (!isset($_SESSION)) {
	session_start();
}

session_unset();
session_destroy();

if(isset($_POST['email'])){
	$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);

	$query = $con->prepare("UPDATE `customer` SET `customer_token` = null, `customer_device_id` = null WHERE `customer_email` = ?");
	$query->execute(array($email));
}
				