<?php

include("../seguranca.php");

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$usuario = (isset($_POST['user'])) ? $_POST['user'] : '';
	$senha = (isset($_POST['password'])) ? $_POST['password'] : '';
	
	if (validaUsuarioRestrito($usuario, $senha) == true) {
		$mensagem = "";
		$status = 1;
		
		$json = array("message" => $mensagem, "status" => $status);
	} else{
		$mensagem = "Usuário e/ou senha inválidos.";
		$status = 2;
		
		$json = array("message" => $mensagem, "status" => $status);
	}
	echo json_encode($json);
}