<?php
include_once('../conexao.php');

$number = $_REQUEST['pedido'];

include('../class/Order.php');
$query = $con->prepare('SELECT *,
			DATE_FORMAT(o.order_date, "%d/%m/%Y %H:%i") as order_date,
			DATE_FORMAT(e.event_date, "%d/%m/%Y") as event_date
			FROM `order` as o INNER JOIN `event` as e ON o.order_event_id = e.event_id
			INNER JOIN `status` as s ON o.order_status_id = s.status_id
			WHERE order_tracking_number = ? LIMIT 1');
$query->execute(array($number));
$query->setFetchMode(PDO::FETCH_CLASS, 'Order');
$num_rows = $query->rowCount();
$row = $query->fetch();

if($num_rows > 0){
	$date_time = explode(" ", $row->getOrderDate());
	$floor = $sector = '';
	
	if($row->getOrderFloor() != ''){
		include('../class/EventFloor.php');
			
		$query4 = $con->prepare('SELECT * FROM event_floor WHERE event_floor_id = ?');
		$query4->execute(array($row->getOrderFloor()));
		$query4->setFetchMode(PDO::FETCH_CLASS, 'EventFloor');
		$row4 = $query4->fetch();
			
		$floor = $row4->getEventFloor();
	}
	if($row->getOrderSector() != ''){
		include('../class/EventSector.php');
	
		$query3 = $con->prepare('SELECT * FROM event_sector WHERE event_sector_id = ?');
		$query3->execute(array($row->getOrderSector()));
		$query3->setFetchMode(PDO::FETCH_CLASS, 'EventSector');
		$row3 = $query3->fetch();
			
		$sector = $row3->getEventSector();
	}
	
	include('../class/Item.php');
		
	$query2 = $con->prepare('SELECT i.item_price_total,i.item_price_unit,i.item_quantity,
			p.product_name, p.product_short_desc
			FROM item as i INNER JOIN product as p ON i.item_product_id = p.product_id
			WHERE item_order_id = ?');
	$query2->execute(array($row->getOrderId()));
	$query2->setFetchMode(PDO::FETCH_CLASS, 'Item');
	
	$products = array();
	$product = '';
	while ($row2 = $query2->fetch()){
		unset($product);
		$product['product_name'] = $row2->getProductName();
		$product['product_short_desc'] = $row2->getProductShortDesc();
		$product['product_price_unit'] = $row2->getItemPriceUnit();
		$product['product_quantity'] = $row2->getItemQuantity();
		$product['product_price_total'] = $row2->getItemPriceTotal();
		array_push($products, $product);
	}
	$mensagem = '';
	$status = 1;
	
	$json = array("date_order" => $date_time[0],
			"time_order" => $date_time[1],
			"event_name" => $row->getEventName(),
			"event_date" => $row->getEventDate(),
			"event_floor" => $floor,
			"event_sector" => $sector,
			"order_chair" => $row->getOrderChair(),
			"products" => $products,
			"order_price" => $row->getOrderPrice(),
			"order_price_discount" => $row->getOrderPriceDiscount(),
			"order_price_total" => $row->getOrderPriceTotal(),
			"message" => $mensagem,
			"status" => $status
	);
	
} else{
	$mensagem = "Pedido nº ".$number." não encontrado.";
	$status = 2;
	
	$json = array("message" => $mensagem, "status" => $status);
}
echo json_encode($json);