<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
	<title>Snack4me</title>
	<meta name="description" content="">
	<meta name="author" content="snack4me">
	<meta name="HandheldFriendly" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="css/main.css">
</head>

<body>
	<div id="box_login_user">
		<div id="content">
			<div style="text-align: center">
				<img src="../images/logo.png" title="Snack4me" class="logo">
			</div>

			<br />
			<div style="text-align: center">
				<span class="ajax-loader"></span>
				<span class="message"></span>
			</div>
			
			<!-- Login Fields -->
			<div id="social">
				<input name="name" class="login user" id="login_name" placeholder="Usuário"/>
				<input type="password" name='password' class="login password" id="login_password" placeholder="Senha"/>
				</div>

			<!-- Green Button -->
			<div class="button_login green" style="text-align: center">
				<button name="entrar" id="entrar" class="btn btn-success btn-large">Entrar</button>
			</div>
		</div>
	</div>
	<script src="js/jquery.min.js"></script>
	<script>
		$(document).ready(function () {
			$(document).on("click", "#entrar", function() {
				var login_name = $("#login_name").val();
				var login_password = $("#login_password").val();
				
				if(login_name == '' || login_password == ''){
					alert('Informe usuário e senha.');
					return false;
				}

				$.ajax({
					type: "POST",
					url:"verify_user.php",
					data: 'user='+ login_name + '&password='+ login_password,
					dataType: "JSON",
					cache: false,
					success:function(result){
						if(result.status == 2){
							$(".message").html(result.message);
							$(".message").css({color:"red"});
						} else{
						    $(location).attr("href", "index.php");
						}
						$('.ajax-loader').html("");
			    	},
					beforeSend: function(){
				    	$('.ajax-loader').html("<img src='../images/loading_pequeno.gif'>");
				  	},
				  	complete: function(){
				    	$('.ajax-loader').hide();
				  	}
			    });
			});
		});
	</script>
</body>
</html>
