<?php 
//include("seguranca.php"); // Inclui o arquivo com o sistema de segurança
//protegePagina(); // Chama a função que protege a página

include('../conexao.php');
$id_user = 1;

include_once('../class/User.php');

$query = $con->prepare('SELECT u.user_name, p.profile_name
		FROM user AS u INNER JOIN profile AS p ON u.user_profile_id = p.profile_id 
		WHERE u.user_id = ?');
$query->execute(array($id_user));
$query->setFetchMode(PDO::FETCH_CLASS, 'User');
$row = $query->fetch();

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Snack4me</title>
		<meta name="description" content="">
		<meta name="author" content="snack4me">
		<meta name="HandheldFriendly" content="true">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/fonts.css?family=Source+Sans+Pro:400,400italic,700|Open+Sans+Condensed:300,700" rel="stylesheet" />
		<link rel="stylesheet" href="../css/buttons.css" />
	</head>
	<body class="left-sidebar">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Content -->
					<div id="content">
						<div id="content-inner">
							<label style="font-weight:bold;">Nome: </label><span><?= $row->getUserName(); ?></span><br />
							<label style="font-weight:bold;">Função: </label><span><?= $row->getProfileName(); ?></span><br /><br />
							<label style="font-weight:bold;">Trocar senha</label><br />
							<div class="input-group" style="width: 275px; margin-bottom: 5px;">
								<span class="input-group-addon"><i class="fa fa-key"></i></span>
								<input type="password" class="form-control" value="" name="senha" id="senha" placeholder="Digite a sua senha">
							</div>
							<div class="input-group" style="width: 275px; margin-bottom: 5px;">
								<span class="input-group-addon"><i class="fa fa-key"></i></span>
								<input type="password" class="form-control" value="" name="nova_senha" id="nova_senha" placeholder="Digite a sua nova senha">
							</div>
							<div class="input-group" style="width: 275px;">
								<span class="input-group-addon"><i class="fa fa-key"></i></span>
								<input type="password" class="form-control" value="" name="confirma_senha" id="confirma_senha" placeholder="Confirme a sua nova senha"><span class="ajax-loader"></span>
							</div>
							
							<span class="message"></span>
							<input type="hidden" name="title" id="title" value="Dados do Usuário">
							<div style="width: 250px; text-align: center; margin-top: 20px;">
								<button name="trocar" id="trocar" class="btn btn-success btn-large">Trocar Senha</button>
							</div>
						</div>
					</div>
					
					<?php include ('sidebar.php')?>
				
			</div>
			
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<script src="js/init.js"></script>
		<script>
			$(document).ready(function () {
				$('button').click(function() {
					var senha = $("#senha").val();
					var nova_senha = $("#nova_senha").val();
					var confirma_senha = $("#confirma_senha").val();
					
					if(senha == '' || nova_senha == '' || confirma_senha == ''){
						alert('Informe todos os campos.');
						return false;
					}

					if(nova_senha != confirma_senha){
						alert('A confirmação da senha é diferente da nova senha.');
						return false;
					}
					
					$.ajax({
						type: "POST",
						url:"change_password.php",
						data: 'senha='+ senha + '&nova_senha='+ nova_senha,
						dataType: "JSON",
						cache: false,
						success:function(result){
							$(".message").html(result.message);
							if(result.status == 2){
								$(".message").css({color:"red"});
							} else{
								$(".message").css({color:"#363843"});
							}
							$('.ajax-loader').html("");

							$("input[type=password]").each(function () {
								$(this).val("");            
					        });
							event.preventDefault();
				    	},
						beforeSend: function(){
					    	$('.ajax-loader').html("<img src='../images/loading_pequeno.gif'>");
					  	},
					  	complete: function(){
					    	$('.ajax-loader').css({display:"none"});
					  	}
				    });
	            });
			});
		</script>
	</body>
</html>