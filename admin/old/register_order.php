<?php 
//include("seguranca.php"); // Inclui o arquivo com o sistema de segurança
//protegePagina(); // Chama a função que protege a página
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Snack4me</title>
		<meta name="description" content="">
		<meta name="author" content="snack4me">
		<meta name="HandheldFriendly" content="true">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/fonts.css?family=Source+Sans+Pro:400,400italic,700|Open+Sans+Condensed:300,700" rel="stylesheet" />
		<link rel="stylesheet" href="../css/buttons.css" />
	</head>
	<body class="left-sidebar">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Content -->
					<div id="content">
						<div id="content-inner">
							<div class="input-group" style="width: 275px;">
								<span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
								<input type="number" class="form-control" value="" name="pedido" placeholder="Digite o número do pedido"><span class="ajax-loader"></span>
							</div>
							
							<span class="message"></span>
							<input type="hidden" name="title" id="title" value="Registrar uma entrega">
							<div style="width: 250px; text-align: center; margin-top: 20px;">
								<button name="registrar" id="registrar" class="btn btn-success btn-large">Registrar</button>
							</div>
						</div>
					</div>
					
					<?php include ('sidebar.php')?>
				
			</div>
			
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<script src="js/init.js"></script>
		<script>
			$(document).ready(function () {
				$('button').click(function() {
					var pedido = $("input[type=number]").val();
					
					if (confirm('Confirma a entrega do pedido nº ' + pedido + '?')) {
						$.ajax({
							type: "POST",
							url:"delivery_order.php",
							data: 'pedido='+ pedido,
							dataType: "JSON",
							cache: false,
							success:function(result){
								$(".message").html(result.message);
								if(result.status == 2){
									$(".message").css({color:"red"});
								} else{
									$(".message").css({color:"#363843"});
								}
								$('.ajax-loader').html("");
					    	},
							beforeSend: function(){
						    	$('.ajax-loader').html("<img src='../images/loading_pequeno.gif'>");
						  	},
						  	complete: function(){
						    	$('.ajax-loader').css({display:"none"});
						  	}
					    });
					}
	            });
			});
		</script>
	</body>
</html>