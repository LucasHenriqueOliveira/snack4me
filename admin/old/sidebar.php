<!-- Sidebar -->
<div id="sidebar">

	<!-- Logo -->
		<a href="admin.php"><div id="logo"></div></a>

	<!-- Nav -->
		<nav id="nav">
			<ul>
				<li ng-class="getClass('/register')" ><a href="#/register"><i class="fa fa-pencil-square-o"></i> Registrar uma entrega</a></li>
				<li ng-class="getClass('/search')"><a href="#/search"><i class="fa fa-search"></i> Pesquisar um pedido</a></li>
				<li ng-class="getClass('/history')"><a href="#/history"><i class="fa fa-folder-open"></i> Histórico de pedidos</a></li>
				<li ng-class="getClass('/user')"><a href="#/user"><i class="fa fa-user"></i> Dados do Usuário</a></li>
				<li><a href="logout.php"><i class="fa fa-power-off"></i> Sair</a></li>
			</ul>
		</nav>
</div>
