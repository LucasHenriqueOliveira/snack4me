<?php 
//include("seguranca.php"); // Inclui o arquivo com o sistema de segurança
//protegePagina(); // Chama a função que protege a página

include('../conexao.php');
$id_event = 1;
$id_user = 1;

include_once('../class/Order.php');

$query = $con->prepare('SELECT order_tracking_number, order_chair, order_price_total,
							   DATE_FORMAT(order_delivery_date, "%H:%i") as order_delivery_date
		FROM `order` WHERE order_event_id = ? AND order_user_id_delivery = ? AND order_status_id = ?');
$query->execute(array($id_event, $id_user, 2));
$query->setFetchMode(PDO::FETCH_CLASS, 'Order');
$num_rows = $query->rowCount();

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Snack4me</title>
		<meta name="description" content="">
		<meta name="author" content="snack4me">
		<meta name="HandheldFriendly" content="true">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/fonts.css?family=Source+Sans+Pro:400,400italic,700|Open+Sans+Condensed:300,700" rel="stylesheet" />
	</head>
	<body class="left-sidebar">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Content -->
					<div id="content">
						<div id="content-inner">
							<?php 
							if($num_rows > 0){
								echo "<table>
										<tr>
											<th style='background: #f1f1f1;'>Pedido</th>
											<th style='background: #f1f1f1;'>Hora Entrega</th>
											<th style='background: #f1f1f1;'>Cadeira</th>
											<th style='background: #f1f1f1;'>Preço</th>
										</tr>";
								
								while($row = $query->fetch()){
									echo "<tr>
											<td>" . $row->getOrderTrackingNumber() . "</td> 
											<td>" . $row->getOrderDeliveryDate() . "</td>
											<td>" . $row->getOrderChair() . "</td>
											<td>R$" . $row->getOrderPriceTotal() . "</td>
										  </tr>";
								}
								echo "</table>";
								
							} else{
								echo "<span>Nenhum pedido foi entregue.</span>";
							}
							?>
							<input type="hidden" name="title" id="title" value="Histórico de Pedidos">
						</div>
					</div>
					
					<?php include ('sidebar.php')?>
				
			</div>
			
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<script src="js/init.js"></script>
	</body>
</html>