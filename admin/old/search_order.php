<?php 
//include("seguranca.php"); // Inclui o arquivo com o sistema de segurança
//protegePagina(); // Chama a função que protege a página
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Snack4me</title>
		<meta name="description" content="">
		<meta name="author" content="snack4me">
		<meta name="HandheldFriendly" content="true">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/fonts.css?family=Source+Sans+Pro:400,400italic,700|Open+Sans+Condensed:300,700" rel="stylesheet" />
		<link rel="stylesheet" href="../css/buttons.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
		<!--[if lte IE 7]><link rel="stylesheet" href="css/ie7.css" /><![endif]-->
	</head>
	<body class="left-sidebar">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Content -->
					<div id="content">
						<div id="content-inner">
							<div class="input-group" style="width: 275px;">
								<span class="input-group-addon"><i class="fa fa-search"></i></span>
								<input type="number" class="form-control" value="" name="pedido" placeholder="Digite o número do pedido"><span class="ajax-loader"></span>
							</div>
							
							<span class="message"></span>
							<input type="hidden" name="title" id="title" value="Pesquisar um pedido">
							<div style="width: 250px; text-align: center; margin-top: 20px;">
								<button name="pesquisar" id="pesquisar" class="btn btn-success btn-large">Pesquisar</button>
							</div>
							
							<div class="order">
							</div>
						</div>
					</div>
					
					<?php include ('sidebar.php')?>
				
			</div>
			
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<script src="js/init.js"></script>
		<script>
			$(document).ready(function () {
				$('button').click(function() {
					var pedido = $("input[type=number]").val();
					
					$.ajax({
						type: "POST",
						url:"order_search.php",
						data: 'pedido='+ pedido,
						dataType: "JSON",
						cache: false,
						success:function(result){
							
							if(result.status == 2){
								$(".message").html(result.message);
								$(".message").css({color:"red"});
								$(".order").html("");
							} else{
								
								$(".message").html("");
								var html = "";

								html += "<h2>Pedido Nº " + pedido + "</h2>";
								html += "<ul class='pedido'><li>Data do Pedido: " + result.date_order + "</li>";
								html += "<li>Horário do pedido: " + result.time_order + "</li>";
								html += "<li>" + result.event_name + " (" + result.event_date + ") </li>";

								if(result.event_floor != ''){
									html += "<li>Nível: " + result.event_floor + "</li>";
								}
								if(result.event_sector != ''){
									html += "<li>Setor: " + result.event_sector + "</li>";
								}
								html += "<li>Cadeira: " + result.order_chair + "</li></ul>";
			
								html += "<table><tr>";
								html += "<th style='background: #f1f1f1;'>Produtos</th>";
								html += "<th style='background: #f1f1f1;'>Preço</th>";
								html += "<th style='background: #f1f1f1;'>Qtd</th>";
								html += "<th style='background: #f1f1f1;'>Subtotal</th>";
								html += "</tr>";

								for(var i = 0; i < result.products.length; i++){
									html += "<tr>";
									html += "<td><strong>" + result.products[i].product_name + "</strong><br/>" + result.products[i].product_short_desc + "</td>";
									html += "<td>" + result.products[i].product_price_unit + "</td>";
									html += "<td>" + result.products[i].product_quantity + "</td>";
									html += "<td>" + result.products[i].product_price_total + "</td>";
									html += "</tr>";
								}
								html += "<tr><td colspan='3' style='text-align: right'>Subtotal (R$)</td><td>" + result.order_price + "</td></tr>";
								html += "<tr><td colspan='3' style='text-align: right'>Desconto (R$)</td><td>" + result.order_price_discount + "</td></tr>";
								html += "<tr><td colspan='3' style='text-align: right'>Taxa de Serviço (R$)</td><td> 3.00 </td></tr>";
								html += "<tr><td colspan='3' style='text-align: right'>Total (R$)</td><td>" + result.order_price_total + "</td></tr>";
								html += "</table>";
								
								$(".order").html(html);
							}
							$('.ajax-loader').html("");
				    	},
						beforeSend: function(){
					    	$('.ajax-loader').html("<img src='../images/loading_pequeno.gif'>");
					  	},
					  	complete: function(){
					    	$('.ajax-loader').css({display:"none"});
					  	}
				    });
	            });
			});
		</script>
	</body>
</html>