<?php
include_once('../conexao.php');

$senha = $_POST['senha'];
$nova_senha = $_POST['nova_senha'];
$id_user = 1;

include('../class/User.php');
$query = $con->prepare('SELECT * FROM user AS u WHERE u.user_id = ? AND u.user_password = SHA1(?)');
$query->execute(array($id_user, $senha));
$query->setFetchMode(PDO::FETCH_CLASS, 'User');
$num_rows = $query->rowCount();
$row = $query->fetch();

if($num_rows > 0){
	
	date_default_timezone_set('America/Sao_Paulo');
	$date_time = date("Y-m-d H:m:s");
	
	$sql2 = 'UPDATE `user` SET user_password = SHA1(?), user_login_default = ?, user_dth_update = ? WHERE user_id = ?';
	$query2 = $con->prepare($sql2);
	$query2->execute(array($nova_senha, 0, $date_time, $row->getUserId()));
	$num_rows2 = $query2->rowCount();
	
	if($num_rows2 > 0){
		$mensagem = 'Senha alterada com sucesso.';
		$status = 1;
	} else{
		$mensagem = "Erro ao alterar a senha.";
		$status = 2;
	}
	
} else{
	$mensagem = "Senha atual não confere.";
	$status = 2;
}

echo json_encode(array("message" => $mensagem, "status" => $status));