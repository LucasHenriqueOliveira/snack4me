<?php
include_once('../conexao.php');

$number = $_REQUEST['pedido'];

include('../class/Order.php');
$query = $con->prepare('SELECT * FROM `order` WHERE order_tracking_number = ? LIMIT 1');
$query->execute(array($number));
$query->setFetchMode(PDO::FETCH_CLASS, 'Order');
$num_rows = $query->rowCount();
$row = $query->fetch();

date_default_timezone_set('America/Sao_Paulo');
$date_time = date("Y-m-d H:m:s");
$date_email = date("d/m/Y");
$time_email = date("H:m");

if($num_rows > 0){
	
	if($row->getOrderStatusId() == 2){
		$mensagem = 'Pedido nº '.$number.' já entregue.';
		$status = 2;
	} else{
		
		$sql2 = 'UPDATE `order` SET order_status_id = ?, order_delivery_date = ? WHERE order_id = ?';
		$query2 = $con->prepare($sql2);
		$query2->execute(array(2, $date_time, $row->getOrderId()));
		$num_rows2 = $query2->rowCount();
		
		if($num_rows2 > 0){
			$mensagem = 'Entrega nº ' . $number . ' realizada com sucesso.';
			$status = 1;
			
			try{
				include('../lib/utilities.php');
				$assunto = "Entrega de pedido - Snack4me";
				$message = "Prezado(a), o seu pedido de número ". $number ." foi entregue no dia ".$date_email." às ".$time_email.".<br /><br />" ;
				$message .= "Obrigado<br /><a href='http://www.snack4me.com' target='_blank'><img src='http://www.snack4me.com/hom/images/logo_small.png' title='Snack4me'></a><br /><br />";
				$message .= "<span style='font-size:9px;color:#d5d5d5'>Favor não responder o email.</span><br />";

				$envia_email = enviarEmail('Cliente Snack4me', $row->getOrderCustomerEmail(), $assunto, $message);
			
			} catch (Exception $e){
				$assunto = "Erro no envio de confirmação de entrega";
				$message = "Erro no envio de confirmação de entrega nº ".$number."<br /><br />";
				$name = "Erro Entrega";
				$email = "sales@snack4me.com";
					
				$envia_email = enviarEmail($name, $email, $assunto, $message);
			}
			
		} else{
			$mensagem = 'Não foi possível alterar o status do pedido nº '.$number.'.';
			$status = 2;
		}
	}
	
} else{
	$mensagem = 'Pedido nº '.$number.' não encontrado.';
	$status = 2;
}

$json = array("message" => $mensagem, "status" => $status);
echo json_encode($json);