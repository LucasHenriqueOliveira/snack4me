<?php 
include("../seguranca.php"); // Inclui o arquivo com o sistema de segurança
protegePaginaRestrito(); // Chama a função que protege a página
?>
<!DOCTYPE HTML>
<html ng-app="delivery">
	<head>
		<meta charset="utf-8" />
		<title>Snack4me</title>
		<meta name="description" content="">
		<meta name="author" content="snack4me">
		<meta name="HandheldFriendly" content="true">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/fonts.css?family=Source+Sans+Pro:400,400italic,700|Open+Sans+Condensed:300,700" rel="stylesheet" />
		<link rel="stylesheet" href="css/buttons.css" />
	</head>
	<body class="left-sidebar" ng-controller="DeliveryCtrl">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Content -->
				<div id="content">
					<div id="content-inner">
						<div ng-view></div>
					</div>
				</div>
				
				<div id="sidebar">
					<!-- Logo -->
					<a href="#/" ng-click="titleSideBar('')"><div id="logo"></div></a>
				
					<!-- Nav -->
					<nav id="nav">
						<ul>
							<li ng-class="getClass('/register')"><a href="#/register" ng-click="titleSideBar('Registrar uma entrega')"><i class="fa fa-pencil-square-o"></i> Registrar uma entrega</a></li>
							<li ng-class="getClass('/search')"><a href="#/search" ng-click="titleSideBar('Pesquisar um pedido')"><i class="fa fa-search"></i> Pesquisar um pedido</a></li>
							<li ng-class="getClass('/history')"><a href="#/history" ng-click="titleSideBar('Histórico de pedidos')"><i class="fa fa-folder-open"></i> Histórico de pedidos</a></li>
							<li ng-class="getClass('/user')"><a href="#/user" ng-click="titleSideBar('Dados do Usuário')"><i class="fa fa-user"></i> Dados do Usuário</a></li>
							<li><a href="logout.php"><i class="fa fa-power-off"></i> Sair</a></li>
						</ul>
					</nav>
				</div>
			</div>
			
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-route.min.js"></script>
		<script src="js/app.js"></script>
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<script src="js/init.js"></script>
		<script src="js/func.js"></script>
	</body>
</html>