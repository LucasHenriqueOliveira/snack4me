angular
	.module('delivery', ['ngRoute'])
	
	.config(function($routeProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'home.html' 
			})
			.when('/register', {
				templateUrl: 'register.html' 
			})
			.when('/search', {
				templateUrl: 'search.html' 
			})
			.when('/history', {
				templateUrl: 'history.php' 
			})
			.when('/user', {
				templateUrl: 'user.php' 
			});
	})
	
	.controller('DeliveryCtrl', function($scope, $location) {
		
		$scope.getClass = function(path) {
		    if ($location.path().substr(0, path.length) == path) {
		      return "current_page_item";
		    } else {
		      return "";
		    }
		}
		
		$scope.titleSideBar = function(title) {
			$(document).ready(function () {
				$("#titulo").html(title);
			});
	    };

	});