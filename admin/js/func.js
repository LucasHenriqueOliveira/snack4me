$(document).ready(function () {
	$(document).on("click", "#registrar", function() {
		var pedido = $("input[type=number]").val();
		
		if (confirm('Confirma a entrega do pedido nº ' + pedido + '?')) {
			$.ajax({
				type: "POST",
				url:"delivery_order.php",
				data: 'pedido='+ pedido,
				dataType: "JSON",
				cache: false,
				success:function(result){
					$(".message").html(result.message);
					if(result.status == 2){
						$(".message").css({color:"red"});
					} else{
						$(".message").css({color:"#363843"});
					}
					$('.ajax-loader').html("");
		    	},
				beforeSend: function(){
			    	$('.ajax-loader').html("<img src='../images/loading_pequeno.gif'>");
			  	},
			  	complete: function(){
			    	$('.ajax-loader').hide();
			  	}
		    });
		}
	});
	
	$(document).on("click", "#pesquisar", function() {
		var pedido = $("input[type=number]").val();
		
		$.ajax({
			type: "POST",
			url:"order_search.php",
			data: 'pedido='+ pedido,
			dataType: "JSON",
			cache: false,
			success:function(result){
				
				if(result.status == 2){
					$(".message").html(result.message);
					$(".message").css({color:"red"});
					$(".order").html("");
				} else{
					
					$(".message").html("");
					var html = "";

					html += "<h2>Pedido Nº " + pedido + "</h2>";
					html += "<ul class='pedido'><li>Data do Pedido: " + result.date_order + "</li>";
					html += "<li>Horário do pedido: " + result.time_order + "</li>";
					html += "<li>" + result.event_name + " (" + result.event_date + ") </li>";

					if(result.event_floor != ''){
						html += "<li>Nível: " + result.event_floor + "</li>";
					}
					if(result.event_sector != ''){
						html += "<li>Setor: " + result.event_sector + "</li>";
					}
					html += "<li>Cadeira: " + result.order_chair + "</li></ul>";

					html += "<table><tr>";
					html += "<th style='background: #f1f1f1;'>Produtos</th>";
					html += "<th style='background: #f1f1f1;'>Preço</th>";
					html += "<th style='background: #f1f1f1;'>Qtd</th>";
					html += "<th style='background: #f1f1f1;'>Subtotal</th>";
					html += "</tr>";

					for(var i = 0; i < result.products.length; i++){
						html += "<tr>";
						html += "<td><strong>" + result.products[i].product_name + "</strong><br/>" + result.products[i].product_short_desc + "</td>";
						html += "<td>" + result.products[i].product_price_unit + "</td>";
						html += "<td>" + result.products[i].product_quantity + "</td>";
						html += "<td>" + result.products[i].product_price_total + "</td>";
						html += "</tr>";
					}
					html += "<tr><td colspan='3' style='text-align: right'>Subtotal (R$)</td><td>" + result.order_price + "</td></tr>";
					html += "<tr><td colspan='3' style='text-align: right'>Desconto (R$)</td><td>" + result.order_price_discount + "</td></tr>";
					html += "<tr><td colspan='3' style='text-align: right'>Taxa de Serviço (R$)</td><td> 3.00 </td></tr>";
					html += "<tr><td colspan='3' style='text-align: right'>Total (R$)</td><td>" + result.order_price_total + "</td></tr>";
					html += "</table>";
					
					$(".order").html(html);
				}
				$('.ajax-loader').html("");
	    	},
			beforeSend: function(){
		    	$('.ajax-loader').html("<img src='../images/loading_pequeno.gif'>");
		  	},
		  	complete: function(){
		    	$('.ajax-loader').hide();
		  	}
	    });
    });
	
	$(document).on("click", "#trocar", function() {
		var senha = $("#senha").val();
		var nova_senha = $("#nova_senha").val();
		var confirma_senha = $("#confirma_senha").val();
		
		if(senha == '' || nova_senha == '' || confirma_senha == ''){
			alert('Informe todos os campos.');
			return false;
		}

		if(nova_senha != confirma_senha){
			alert('A confirmação da senha é diferente da nova senha.');
			return false;
		}
		
		$.ajax({
			type: "POST",
			url:"change_password.php",
			data: 'senha='+ senha + '&nova_senha='+ nova_senha,
			dataType: "JSON",
			cache: false,
			success:function(result){
				$(".message").html(result.message);
				if(result.status == 2){
					$(".message").css({color:"red"});
				} else{
					$(".message").css({color:"#363843"});
				}
				$('.ajax-loader').html("");

				$("input[type=password]").each(function () {
					$(this).val("");            
		        });
				event.preventDefault();
	    	},
			beforeSend: function(){
		    	$('.ajax-loader').html("<img src='../images/loading_pequeno.gif'>");
		  	},
		  	complete: function(){
		    	$('.ajax-loader').hide();
		  	}
	    });
    });
});