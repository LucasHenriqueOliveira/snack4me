<?php

include('../conexao.php');
$id_event = 1;
$id_user = 1;

include_once('../class/Order.php');

$query = $con->prepare('SELECT order_tracking_number, order_chair, order_price_total,
							   DATE_FORMAT(order_delivery_date, "%H:%i") as order_delivery_date
		FROM `order` WHERE order_event_id = ? AND order_user_id_delivery = ? AND order_status_id = ?');
$query->execute(array($id_event, $id_user, 2));
$query->setFetchMode(PDO::FETCH_CLASS, 'Order');
$num_rows = $query->rowCount();

if($num_rows > 0){
	echo "<table>
	<tr>
	<th style='background: #f1f1f1;'>Pedido</th>
	<th style='background: #f1f1f1;'>Hora Entrega</th>
	<th style='background: #f1f1f1;'>Cadeira</th>
	<th style='background: #f1f1f1;'>Preço</th>
	</tr>";

	while($row = $query->fetch()){
		echo "<tr>
		<td>" . $row->getOrderTrackingNumber() . "</td>
		<td>" . $row->getOrderDeliveryDate() . "</td>
		<td>" . $row->getOrderChair() . "</td>
		<td>R$" . $row->getOrderPriceTotal() . "</td>
		</tr>";
	}
	echo "</table>";

} else{
	echo "<span>Nenhum pedido foi entregue.</span>";
}
echo '<input type="hidden" name="title" id="title" value="Histórico de Pedidos">';