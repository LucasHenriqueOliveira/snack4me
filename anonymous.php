<?php

$resposta = array();

function separar_parametros($param) {
	return explode('&',$param);
}

function separar_valor($opcao) {
	return explode('=',$opcao);
}

function isMail($email)
{
	$er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
	if(preg_match($er,$email))
		return true;
	else
		return false;
}

try{
	$ve = $_POST['ve'];
	$arr = array("#" => "/", "&" => "\\");
	
	$parametros = separar_parametros(gzinflate(base64_decode(strtr($ve,$arr))));
	foreach($parametros as $opcao) {
		$valor[] = separar_valor($opcao);
	}
	
	$email = $id = '';
	if(isset($valor[0][1])){
		$email = $valor[0][1];
	}
	if(isset($valor[1][1])){
		$id = $valor[1][1];
	}
	
	if(isMail($email)){
		
		session_start();
		
		$token = hash('sha256', uniqid(mt_rand() . $_SERVER['HTTP_USER_AGENT'], true));
		
		$_SESSION['id'] = 1;
		$_SESSION['email'] = $email;
		$_SESSION['name'] = 'Cliente Snack4me';
		$_SESSION['type'] = 1;
		$_SESSION['XSRF'] = $token;

		$user['id'] = 1;
		$user['email'] = $email;
		$user['name'] = 'Cliente Snack4me';
		$user['type'] = 1;
		$user['checkout'] = $id;
		$user['XSRF'] = $token;
		
		$resposta["error"] = false;
		$resposta["response"] = $user;
	} else{
		$resposta["error"] = true;
		$resposta["message"] = 'Erro na validação da compra sem cadastro.';
	}
	
} catch(PDOException $e){
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);