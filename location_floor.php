<?php

$resposta = array();
$floors = array();

try{
	include_once('conexao.php');
	include('class/EventFloor.php');
	
	$id_event = $_REQUEST['id_event'];
	
	$query = $con->prepare('SELECT * FROM event_floor WHERE event_id = ?');
	$query->execute(array($id_event));
	$query->setFetchMode(PDO::FETCH_CLASS, 'EventFloor');
	
	while($row = $query->fetch()){
		$floor['id_floor'] = $row->getEventFloorId();
		$floor['name_floor'] = $row->getEventFloor();
		$floors[] = $floor;
	}
	
	$resposta["floors"] = $floors;
	$resposta["error"] = false;	

} catch (Exception $e){
	
	$resposta["error"] = true;
	$resposta["message"] = $e->getMessage();
}
echo json_encode($resposta);
